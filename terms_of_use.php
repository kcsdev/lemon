<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en"><head>
	<title>Terms &amp; Conditions</title>
<style type="text/css"></style></head>
<body>
<div id="header">
	Site header
</div>

<div id="small-print">
    <h3>Terms &amp; Conditions</h3>
	To make the Lemonjuice.com website ("LemonJuice") available to you, we at LemonJuice Ltd. require you to agree to and abide by the requirements set out below ("Terms"). If you don't agree to these Terms, then you can't use LemonJuice.
    <br>
<h3>The Basics</h3>
<ul>
	<li> You must be 18 years old or older to use LemonJuice or have a legal guardian agree that you can use LemonJuice.</li>
	<li>We also allow businesses to use LemonJuice and make purchases. If you are using LemonJuice on behalf of a business, then you must have the authority, and your use of LemonJuice is a warranty that you have the authority to bind that business to these Terms.</li>
	<li>Be smart! You will be responsible for all activity that occurs under your username and password. We therefore recommend that you keep your password secure and don't allow others to use your username. The only way we have to verify your identity, is through your username and password, so keep them to yourself.</li>
	<li>Be nice! We are trying to create a community at LemonJuice, so we won't tolerate users that threaten, impersonate, abuse or harass other LemonJuice users or act in any way that we think is outside of these Terms or outside the spirit of LemonJuice.</li>
	<li>Because the internet is available pretty much everywhere, we can't possibly keep up with all legal requirements that may apply to use of LemonJuice. We therefore put the onus on you to ensure that your use of LemonJuice complies with all laws, rules and regulations that are applicable to you. These Terms are void and use of LemonJuice is revoked where use is prohibited.</li>
	<li>We reserve the right to change these Terms. We'll do our best to let you know when we change them but you should review them periodically to make sure that you understand the agreement between you and us. If you continue to use LemonJuice after we post new Terms, then you acknowledge that you agree to those new Terms.</li>
	<li>We have other policies, guidelines, FAQs and requirements that we post elsewhere on LemonJuice from time to time. We expect that you will, and you agree to, abide by all requirements posted on LemonJuice. All of those policies, guidelines, FAQs and other requirements are incorporated into these Terms.</li>
	<li>If you don't like something posted on LemonJuice, you may navigate away from that page. We can't be responsible for the impact anything posted on LemonJuice may have on you.</li>
</ul>
<h3>Do's and Don't's</h3>
When using LemonJuice...
<h3>Do:</h3>
<ul>
	<li>Re-post images from LemonJuice on other websites! (That's right, this is a Do, not a Don't.) We support this practice and think that it benefits our artists. As long as you include a link back to LemonJuice and the page on which that image or content appears (and otherwise comply with these Terms) we're fine with your re-posting of content, thumbnails, links to LemonJuice, etc., in your blogs, social-network postings, listings, etc.</li>
	<li>Be authentic. All the registration information that you submit to us, and all other information posted or submitted to LemonJuice must be accurate, truthful and up-to-date. We rely on this information, so please let us know if any of your information changes.</li>
	<li>Be responsible. You are solely responsible for your conduct on LemonJuice, including for any data, text, information, usernames, graphics, profiles, audio and video ("Content") that you post, submit or display on LemonJuice.</li>
	<li>Be careful. If you use any community features on LemonJuice, such as commenting or social networking, be aware that any personal information (such as your name, phone number, address, email address) that you elect to share, will be shown to other users of LemonJuice. So don't post it unless you want the general public to see it.</li>
</ul>
<h3>Don't:</h3>
<ul>
	<li>Infringe on our artists' rights. This is their livelihood! If you reproduce or sell images of their artwork, or post their artwork on other sites without a link back to LemonJuice, you'll be infringing their copyright and other rights to their art, which is illegal. As mentioned above under Do's, we do give you the limited right to repost images from LemonJuice.</li>
	<li>Infringe on other people's rights. Don't infringe upon any third party's copyright, patent, trademark, trade secret or other proprietary or intellectual property rights or rights of publicity or privacy.</li>
	<li>Abuse our site. You may not use LemonJuice for hosting graphic elements of web page designs, icons, smilies, buddy icons, forum avatars, badges and other non-photographic elements on external websites. Nor may you modify, adapt, disassemble or otherwise change or interfere with the use of LemonJuice. You are also not allowed to interfere with or disrupt LemonJuice or violate the security, servers or networks of LemonJuice.</li>
	<li>Lie, cheat or steal. Don't do anything on LemonJuice that is fraudulent or involves the sale of illegal, counterfeit, stolen items. This includes artists misrepresenting work as their own and buyers attempting to reproduce an artist's artwork.</li>
	<li>Lie, cheat or steal, the Sequel. Don't threaten, impersonate, abuse, defame, libel, or harass other LemonJuice users or act in any way that we think is outside of these Terms or the spirit of LemonJuice.</li>
	<li>Lie, cheat or steal, the 2nd Sequel. Don't post, submit or display anything that is false, inaccurate, misleading, defamatory, trade libelous, unlawfully threatening, unlawfully harassing, obscene, profane, vulgar, offensive, or otherwise not appropriate, as determined by us.</li>
	<li>Break the law. Don't violate any law, statute, ordinance or regulation (including, but not limited to, those governing export control, consumer protection, unfair competition, anti-discrimination or false advertising).</li>
	<li>Hack. Don't even think about uploading, posting or displaying any viruses, Trojan horses, worms, time bombs, cancel bots, Easter eggs or other computer programming routines that may damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or personal information.</li>
	<li>Ruin it for the rest of us. Don't create liability for us or cause us to lose (in whole or in part) the services of our ISPs or other suppliers.</li>
	<li>Spam us (or anyone else!). Create or submit through or otherwise use LemonJuice to engage in any unwanted email solicitations, spam, or other business that is unrelated to LemonJuice.</li>
	<li>Spread rumors. Link to LemonJuice in a manner that would create an association between LemonJuice and another site, or indicate that LemonJuice approves, endorses or is otherwise related to another site.</li>
</ul>
<h3>Artists' Rights</h3>
<ul>
	<li>Our artists retain the copyright to their artwork at all times. Simply buying a piece of art does not transfer any of the copyright or other rights to you. You therefore may not reproduce images of artwork purchased on LemonJuice without the artist's prior written consent.</li>
	<li>Artists cannot post their materials directly to LemonJuice. If you want to submit work to LemonJuice for consideration, please follow our <a href="http://www.c14.co.il/lemon/artist-submission-guidelines/">Artist Submission Guidelines</a>.</li>
	<li>If you are an artist, photographer or other copyright owner, and you think that something on LemonJuice violates your rights, please refer to our <a href="http://www.c14.co.il/lemon/copyright-policy/">Copyright Policy</a>.</li>
</ul>
<h3>Our Rights and Responsibilities</h3>
<ul>
	<li>We try really, really hard to make LemonJuice available to you at all times. That said, stuff happens. LemonJuice may not be continuously available, your content might be removed and we might need to make updates to LemonJuice, our software, hardware or infrastructure. Rest assured that we will get LemonJuice back up and running as soon as we can.</li>
	<li>We reserve the right to terminate your LemonJuice account and username if you violate these Terms, the spirit of these Terms, or for any reason or no reason at all. We have the right to choose our customers and may refuse service to anyone for any reason at any time.</li>
	<li>We may need to change or discontinue certain aspects of LemonJuice. We may do so with or without prior notice to you.</li>
	<li>We don't have the capacity to review everything that gets posted to LemonJuice. We may, but have no obligation to, remove content ("Content" includes, for example, items, item descriptions, and information submitted in the forums) and accounts containing Content that we determine in our sole discretion is unlawful, offensive, threatening, libelous, defamatory, obscene or otherwise objectionable or violates any party's intellectual property, these Terms, the <a href="http://www.c14.co.il/lemon/privacy-policy/">Privacy Policy</a>, or other policy documents and community guidelines as posted on LemonJuice.</li>
	<li>You can remove your profile at any time by deleting your account. This will also remove any private information you have stored in the system. We do not have the obligation to remove any comments or other postings that you have made on LemonJuice.</li>
	<li>By uploading Content to LemonJuice, you agree to allow other LemonJuice users to view and comment on the Content and you agree to allow LemonJuice to display the Content on LemonJuice, and store, modify, reformat, distribute and otherwise use and exploit the Content forever as we see fit.</li>
	<li>LemonJuice, and all Content, trademarks, logos, designs, graphics, images, audio, video and software are owned by us and our licensors. LemonJuice is protected by International copyright laws, and all rights are reserved by us and our licensors.</li>
	<li>If we feel that it is within our best interest, we may cooperate with law enforcement, court, third party or government investigations if we are requested or directed to comply.</li>
</ul>
<h3>Third Party Services</h3>
LemonJuice may enable you to use third party services, such as Twitter, Facebook, blogging sites, emailing content, etc. LemonJuice may also contain links to websites outside of LemonJuice. When you use these third party services or link to another website, you are no longer dealing directly with LemonJuice. Before using any of these features, you should review the Terms of Use and Privacy Policies of any such third party provider.
Checkout, Shipping, Returns, Gift Certificates
We're experts in art, not processing payment transactions. We use a third party to process payments for purchases that you make on LemonJuice. They have separate Terms and Conditions that you will need to agree to as part of the checkout process. We are not responsible for their Terms and Conditions or for the security and privacy of any credit card or other personal information that you submit through them.
We will ship orders in accordance with your requests at checkout. All orders will be subject to our <a href="http://www.c14.co.il/lemon/shipping-policy/">Shipping Policy</a>.
Returns and refunds are governed by our <a href="http://www.c14.co.il/lemon/return-policy/">Return Policy</a>.
You may purchase and redeem Gift Certificates as set out on our <a href="http://www.c14.co.il/lemon/shop_giftcard/gift-card/">Gift Certificate page</a>.
<h3>Privacy</h3>
We take your privacy seriously. Please review our <a href="http://www.c14.co.il/lemon/privacy-policy/">Privacy Policy</a>, which describes how we handle information about you. Remember, though: we don't actually do the payment processing, so you should refer to the Terms and Conditions of our provider for how they will handle your credit card and other important information.
<h3>When Will It End?</h3>
We're almost done!

The next two paragraphs are in all caps because our lawyers said they have to be. We're really not trying to yell at you, promise.
<ul>
	<li>
<h4>Disclaimer of warranties.</h4>
WE PROVIDE LEMONJUICE "AS IS, AS AVAILABLE", AND YOUR ACCESS OF LEMONJUICE IS AT YOUR OWN RISK. WE DO NOT WARRANT THAT LEMONJUICE OR OUR SERVICES WILL MEET YOUR REQUIREMENTS OR RESULT IN ANY PARTICULAR OUTCOME, OR THAT THE OPERATION WILL BE UNINTERRUPTED OR ERROR-FREE. OUR SERVICES AND LEMONJUICE ARE "AS IS" AND WITHOUT ANY WARRANTY OR CONDITION OF ANY KIND (EXPRESS, IMPLIED OR STATUTORY). TO THE FULLEST EXTENT ALLOWED BY LAW, WE SPECIFICALLY DISCLAIM ANY IMPLIED WARRANTIES OF TITLE, MERCHANTABILITY, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. SOME STATES OR COUNTRIES DO NOT ALLOW THE DISCLAIMER OF IMPLIED WARRANTIES, SO THE FOREGOING DISCLAIMER MAY NOT APPLY TO YOU.</li>
	<li>
<h4>Liability limitation.</h4>
WE SHALL NOT BE LIABLE TO YOU FOR ANY SPECIAL, INCIDENTAL, INDIRECT, PUNITIVE OR CONSEQUENTIAL DAMAGES (EVEN IF YOU HAVE BEEN ADVISED US OF THE POSSIBILITY OF SUCH DAMAGES). OUR CUMULATIVE LIABILITY RELATING TO YOUR USE OF LEMONJUICE (REGARDLESS OF ITS BASIS), SHALL NOT EXCEED THE GREATER OF (A) AMOUNTS THAT YOU PAY IN CONNECTION WITH A PARTICULAR PURCHASE YOU MAKE FROM LEMONJUICE AND (B) $100. SOME STATES DO NOT ALLOW THE EXCLUSION OR LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THE ABOVE LIMITATION OR EXCLUSION MAY NOT APPLY TO YOU.</li>
</ul>
<h3>Disputes with us</h3>
Our goal is to make you happy. In the event a dispute arises between you and us, please email us at <a href="mailto: contact@lemonjuice.com">contact@lemonjuice.com </a>and we will work quickly toward a solution. If we can't come to a mutual resolution and need to get a neutral third party involved, rather than having to go to court, we require that any dispute related to these Terms or your use of LemonJuice be submitted to arbitration by a single arbitrator in New York, under the Commercial Arbitration Rules of the American Arbitration Association in effect at that time. The arbitrator's award shall be binding and judgment on the award shall be entered in any court with proper jurisdiction.
<h3>Disputes with others</h3>
If you have a dispute with one or more other users, you release us, LemonJuice (and our officers, directors, agents, subsidiaries, joint ventures and employees) from any and all claims, demands and damages (actual and consequential) of every kind and nature, known and unknown, suspected and unsuspected, disclosed and undisclosed, arising out of or in any way connected with such disputes.
<h3>Keep in touch</h3>
You may contact us using the various methods listed on our <a href="http://www.c14.co.il/lemon/contact/">Contact page</a>. If we need to contact you, we will use the contact information that you provided to us as part of the registration process. Easy tiger. You and LemonJuice are independent contractors, and no agency, partnership, joint venture, employee-employer or franchisor-franchisee relationship is intended or created by these Terms.
<h3>Assignments, etc</h3>
You may not assign, transfer, delegate or sub-license any of your rights under these Terms. We may assign, transfer, delegate or sublicense any of our rights under these Terms. If you don't like whom we transfer our responsibilities to, then you should stop using LemonJuice.
<h3>Severability</h3>
If any provision of these Terms is illegal, void or unenforceable, that provision shall be limited or eliminated to the minimum extent necessary for the rest of the agreement to remain enforceable.
<h3>No waiver</h3>
Our failure to enforce any part of these Terms shall not constitute a waiver of our right to later enforce that or any other part of these Terms. Just because we might waive compliance once, does not mean we will do so again. For any waiver of compliance with these Terms to be binding on us, one of our authorized representatives must provide you with written notice of that waiver.
<h3>Entire agreement almost</h3>
These Terms and any policies, guidelines, <a href="http://www.c14.co.il/lemon/pages/faq/">FAQs </a>and requirements that we post on LemonJuice state the complete understanding between you and us. No modification of these Terms will be enforced against us unless in writing and signed by an authorized representative.
<h3>The headings don't count</h3>
The section and paragraph headings in these Terms are for your reading pleasure only and shall not affect the interpretation of these Terms.
<p style="text-align: center;">It's been real. And...that's a wrap. Thanks for reading and please,</p>

<h3 style="text-align: center;"><strong>Collect art—it's a very good investment!</strong></h3>
</div>
<div id="footer">
	Site footer
</div>

</body></html>