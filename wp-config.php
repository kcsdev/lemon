<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */


define('DB_NAME', 'c14_wp29');

/** MySQL database username */
define('DB_USER', 'c14_wp29');

/** MySQL database password */
define('DB_PASSWORD', 'C[rejYn9ed15&@7');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'r6dJjGSMnrjveHaSCWzqbJYLo27KLkTEWYUeDG6xfHsLwHqLxsX2kNRLFp91G7kw');
define('SECURE_AUTH_KEY',  'vyO7k2VULMdM5afZgpQbi4ZVltgrX4xb04lAMeJ0UuHy7k2ZVzTDDwLQOxeBlP8n');
define('LOGGED_IN_KEY',    'wBRKOX39bVgr7erpFI4aZWwSe3Wh2FH0TZ95eeBtyBwTTVAjMJGv0oKwOxJsbCWr');
define('NONCE_KEY',        'AmrsOi3GIm89TJzUop5Aj9V0xxzs8SimqRjPcMRWnAp6EXCxPwg3x5NOfBCE10yF');
define('AUTH_SALT',        'P9JrNA5yHXcIbVLUNUlh8jESJkCWxGmWW3ziEAoXFn39e1H2Svowgyy9DHtX7MFW');
define('SECURE_AUTH_SALT', 'GTDmxm15NjQ4tRuvaixYwNumQmQKfg2hjCNzouBdMj4jh8lHNOIM7RyuTZR8AqGI');
define('LOGGED_IN_SALT',   'fc48TyluASsEnmNfWWitrC7CDV3XAiICyqrgAgG0Dw3AdZJVDfKbjQqNutBiu0RQ');
define('NONCE_SALT',       'NxFzkDRR42WHEiG6BHtSBOhjZHkvorJc7S15YEEgqNUGrqD61t4Ivqj6dBNot1x5');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
