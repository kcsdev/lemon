<?php
/******************************************************************************/
/***************************** Theme Options *********************************/
/******************************************************************************/

if ( !class_exists( 'ReduxFramework' ) && file_exists( dirname( __FILE__ ) . '/settings/redux/ReduxCore/framework.php' ) ) {
    require_once( dirname( __FILE__ ) . '/settings/redux/ReduxCore/framework.php' );
}
if ( !isset( $redux_demo ) && file_exists( dirname( __FILE__ ) . '/settings/mrtailor.config.php' ) ) {
    require_once( dirname( __FILE__ ) . '/settings/mrtailor.config.php' );
}

/******************************************************************************/
/******************************** Includes ************************************/
/******************************************************************************/

include_once('inc/custom-fonts.php'); // Load Custom Fonts
include_once('inc/custom-styles/custom-styles.php'); // Load Custom Styles
include_once('inc/templates/post-meta.php'); // Load Post meta template
include_once('inc/templates/template-tags.php'); // Load Template Tags
require_once('functions/widget-areas.php'); // Creating my widget area

//Include metaboxes
define('_TEMPLATEURL', WP_CONTENT_URL . '/themes/' . basename(TEMPLATEPATH));

include_once 'inc/wpalchemy/MetaBox-mod.php';
include_once 'inc/wpalchemy/MediaAccess-mod.php';

add_action( 'init', 'mr_tailor_metabox_styles' ); 
function mr_tailor_metabox_styles()
{
    if ( is_admin() ) 
    { 
        wp_enqueue_style( 'wpalchemy-metabox', _TEMPLATEURL . '/css/wp-admin-metabox.css' );
    }
}

$wpalchemy_media_access = new WPAlchemy_MediaAccess();

//Include metaboxes
include_once 'inc/metaboxes/slider-spec.php';
include_once 'inc/metaboxes/map-spec.php';

//Include shortcodes
//include_once('inc/shortcodes/wishlist.php');
include_once('inc/shortcodes/product-categories.php');
include_once('inc/shortcodes/socials.php');
include_once('inc/shortcodes/from-the-blog.php');
include_once('inc/shortcodes/separator.php');
include_once('inc/shortcodes/spacing.php');
include_once('inc/shortcodes/banner.php');
include_once('inc/shortcodes/title.php');

//Mixed shortcodes
include_once('inc/shortcodes/mixed/recent-products-mixed.php');
include_once('inc/shortcodes/mixed/featured-products-mixed.php');
include_once('inc/shortcodes/mixed/sale-products-mixed.php');
include_once('inc/shortcodes/mixed/best-selling-products-mixed.php');
include_once('inc/shortcodes/mixed/top-rated-products-mixed.php');
include_once('inc/shortcodes/mixed/product-category-mixed.php');
include_once('inc/shortcodes/mixed/products-mixed.php');
include_once('inc/shortcodes/mixed/products-by-attribute-mixed.php');

//Sliders shortcodes
include_once('inc/shortcodes/sliders/recent-products-slider.php');
include_once('inc/shortcodes/sliders/featured-products-slider.php');
include_once('inc/shortcodes/sliders/sale-products-slider.php');
include_once('inc/shortcodes/sliders/best-selling-products-slider.php');
include_once('inc/shortcodes/sliders/top-rated-products-slider.php');
include_once('inc/shortcodes/sliders/product-category-slider.php');
include_once('inc/shortcodes/sliders/products-slider.php');
include_once('inc/shortcodes/sliders/products-by-attribute-slider.php');


/******************************************************************************/
/************************ Plugin recommendations ******************************/
/******************************************************************************/

require_once dirname( __FILE__ ) . '/inc/tgm/class-tgm-plugin-activation.php';
require_once dirname( __FILE__ ) . '/inc/tgm/plugins.php';




/******************************************************************************/
/*************************** Visual Composer **********************************/
/******************************************************************************/

if (class_exists('WPBakeryVisualComposerAbstract')) {
	
	add_action( 'init', 'visual_composer_stuff' );
	function visual_composer_stuff() {
	
		if(function_exists('vc_set_as_theme')) vc_set_as_theme(true);
		vc_disable_frontend();	
		
		// Modify and remove existing shortcodes from VC
		include_once('inc/shortcodes/visual-composer/custom_vc.php');
		
		// VC Templates
		$vc_templates_dir = get_template_directory() . '/inc/shortcodes/visual-composer/vc_templates/';
		vc_set_template_dir($vc_templates_dir);
		
		// Add new shortcodes to VC
		include_once('inc/shortcodes/visual-composer/from-the-blog.php');
		include_once('inc/shortcodes/visual-composer/social-media-profiles.php');
		include_once('inc/shortcodes/visual-composer/banner.php');
		include_once('inc/shortcodes/visual-composer/title.php');

        // Add new Shop shortcodes to VC
		if (class_exists('WooCommerce')) {
			include_once('inc/shortcodes/visual-composer/wc-recent-products.php');
			include_once('inc/shortcodes/visual-composer/wc-featured-products.php');
			include_once('inc/shortcodes/visual-composer/wc-products-by-category.php');
			include_once('inc/shortcodes/visual-composer/wc-products-by-attribute.php');
			include_once('inc/shortcodes/visual-composer/wc-product-by-id-sku.php');
			include_once('inc/shortcodes/visual-composer/wc-products-by-ids-skus.php');
			include_once('inc/shortcodes/visual-composer/wc-sale-products.php');
			include_once('inc/shortcodes/visual-composer/wc-top-rated-products.php');
			include_once('inc/shortcodes/visual-composer/wc-best-selling-products.php');
			include_once('inc/shortcodes/visual-composer/wc-add-to-cart-button.php');
			include_once('inc/shortcodes/visual-composer/wc-product-categories.php');
			include_once('inc/shortcodes/visual-composer/wc-product-categories-grid.php');
		}
		
		// Add new Options
		function add_vc_text_separator_no_border() {
			$param = WPBMap::getParam('vc_text_separator', 'style');
			$param['value'][__('No Border', 'mr_tailor')] = 'no_border';
			WPBMap::mutateParam('vc_text_separator', $param);
		}
		add_action('init', 'add_vc_text_separator_no_border');
		
		// Remove vc_teaser
		if (is_admin()) :
			function remove_vc_teaser() {
				remove_meta_box('vc_teaser', '' , 'side');
			}
			add_action( 'admin_head', 'remove_vc_teaser' );
		endif;
	
	}

}


/******************************************************************************/
/****************************** Ajax url **************************************/
/******************************************************************************/

add_action('wp_head','mrtailor_ajaxurl');
function mrtailor_ajaxurl() {
?>
    <script type="text/javascript">
        var mrtailor_ajaxurl = '<?php echo admin_url('admin-ajax.php', 'relative'); ?>';
    </script>
<?php
}

/******************************************************************************/
/************************ Customize WooCommerece Tabs ******************************************/
/******************************************************************************/
add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );

function woo_remove_product_tabs( $tabs ) {

   	// Remove the reviews tab
    unset( $tabs['additional_information'] );  	// Remove the additional information tab

    return $tabs;

}

/**
 * Custom Tabs for Custom Specifications Display. Compatible with WooCommerce 2.0+ only!
 * This version uses the code editor.
 *
 * Outputs an extra tab to the default set of info tabs on the single product page.
 * This file needs to be called via your functions.php file.
 */
function custom_tab_options_tab_spec() {
?>
	<li class="custom_tab3"><a href="#custom_tab_data3"><?php _e('Additional Information', 'woothemes'); ?></a></li>
<?php
}
add_action('woocommerce_product_write_panel_tabs', 'custom_tab_options_tab_spec'); 

/**
 * Custom Tab Options
 *
 * Provides the input fields and add/remove buttons for custom tabs on the single product page.
 */
function custom_tab_options_spec() {
	global $post;

	$custom_tab_options_spec = array(
		'titlec' => get_post_meta($post->ID, 'custom_tab_title_spec', true),
		'media' => get_post_meta($post->ID, 'custom_tab_media_spec', true),
		'surface' => get_post_meta($post->ID, 'custom_tab_surface_spec', true),
		'material' => get_post_meta($post->ID, 'custom_tab_material_spec', true),
        'date' => get_post_meta($post->ID, 'custom_tab_date_spec', true),

	);

?>
	<div id="custom_tab_data3" class="panel woocommerce_options_panel">
		<div class="options_group">
			<p class="form-field">
				<?php woocommerce_wp_checkbox( array( 'id' => 'custom_tab_enabled_spec', 'label' => __('Enable Tab?', 'woothemes'), 'description' => __('Enable this option to enable the tab on the frontend.', 'woothemes') ) ); ?>
			</p>
		</div>

		<div class="options_group custom_tab_options">
			<p class="form-field">
				<label><?php _e('Tab Title:', 'woothemes'); ?></label>
				<input type="text" size="5" name="custom_tab_title_spec" value="Additional Information" placeholder="<?php _e('Enter your tab title', 'woothemes'); ?>" />
			</p>

			<p class="form-field">
				<label><?php _e('Media:', 'woothemes'); ?></label>
				<input type="text" size="5" name="custom_tab_media_spec" value="<?php echo @$custom_tab_options_spec['media']; ?>" placeholder="<?php _e('Enter your Media type here', 'woothemes'); ?>" />
			</p>
            
            <p class="form-field">
				<label><?php _e('Surface:', 'woothemes'); ?></label>
				<input type="text" size="5" name="custom_tab_surface_spec" value="<?php echo @$custom_tab_options_spec['surface']; ?>" placeholder="<?php _e('Enter your Surface type here', 'woothemes'); ?>" />
			</p>
            
            <p class="form-field">
				<label><?php _e('Material:', 'woothemes'); ?></label>
				<input type="text" size="5" name="custom_tab_material_spec" value="<?php echo @$custom_tab_options_spec['material']; ?>" placeholder="<?php _e('Enter your Material type here', 'woothemes'); ?>" />
			</p>
            
            <p class="form-field">
				<label><?php _e('Date of Creation:', 'woothemes'); ?></label>
				<input type="text" size="5" name="custom_tab_date_spec" value="<?php echo @$custom_tab_options_spec['date']; ?>" placeholder="<?php _e('Enter your Date type here', 'woothemes'); ?>" />
			</p>

        </div>
	</div>
<?php
}
add_action('woocommerce_product_write_panels', 'custom_tab_options_spec');

/**
 * Process meta
 *
 * Processes the custom tab options when a post is saved
 */
function process_product_meta_custom_tab_spec( $post_id ) {
	update_post_meta( $post_id, 'custom_tab_enabled_spec', ( isset($_POST['custom_tab_enabled_spec']) && $_POST['custom_tab_enabled_spec'] ) ? 'yes' : 'no' );
	update_post_meta( $post_id, 'custom_tab_title_spec', $_POST['custom_tab_title_spec']);
    update_post_meta( $post_id, 'custom_tab_media_spec', $_POST['custom_tab_media_spec']);
	update_post_meta( $post_id, 'custom_tab_surface_spec', $_POST['custom_tab_surface_spec']);
    update_post_meta( $post_id, 'custom_tab_material_spec', $_POST['custom_tab_material_spec']);
    update_post_meta( $post_id, 'custom_tab_date_spec', $_POST['custom_tab_date_spec']);
    update_post_meta( $post_id, 'custom_tab_weight_spec', $_POST['_weight']);
    if($_POST['_length'] != '' && $_POST['_width'] != ''){$_POST['_length'] = $_POST['_length']. ' x ';}
    if($_POST['_height'] != ''){$_POST['_height'] = ' x '.$_POST['_height'];}
   
    update_post_meta( $post_id, 'custom_tab_dimensions_spec', $_POST['_length'].$_POST['_width']. $_POST['_height']);
}
add_action('woocommerce_process_product_meta', 'process_product_meta_custom_tab_spec', 10, 2);

/**
 * Display Tab
 *
 * Display Custom Tab on Frontend of Website for WooCommerce 2.0
 */

add_filter( 'woocommerce_product_tabs', 'woocommerce_product_custom_tab_spec' );

	function woocommerce_product_custom_tab_spec( $tabs ) {
		global $post, $product;

		$custom_tab_options_spec = array(
			'enabled' => get_post_meta($post->ID, 'custom_tab_enabled_spec', true),
			'titlec' => get_post_meta($post->ID, 'custom_tab_title_spec', true),
			'media' => get_post_meta($post->ID, 'custom_tab_media_spec', true),
            'surface' => get_post_meta($post->ID, 'custom_tab_surface_spec', true),
            'material' => get_post_meta($post->ID, 'custom_tab_material_spec', true),
            'date' => get_post_meta($post->ID, 'custom_tab_date_spec', true),
            'weight' => get_post_meta($post->ID, 'custom_tab_weight_spec', true),
            'dimensions' => get_post_meta($post->ID, 'custom_tab_dimensions_spec', true),
		);

			if ( $custom_tab_options_spec['enabled'] != 'no' ){
				$tabs['custom-tab-second'] = array(
					'title'    => $custom_tab_options_spec['titlec'],
					'priority' => 30,
					'callback' => 'custom_product_tabs_panel_content_spec',
					'media'  => $custom_tab_options_spec['media'],
					'surface'  => $custom_tab_options_spec['surface'],
					'material'  => $custom_tab_options_spec['material'],
					'date'  => $custom_tab_options_spec['date'],
					'weight'  => $custom_tab_options_spec['weight'],
					'dimensions'  => $custom_tab_options_spec['dimensions']
				);
			}
		return $tabs;
	}

	/**
	 * Render the custom product tab panel content for the callback 'custom_product_tabs_panel_content_spec'
	 */

   function custom_product_tabs_panel_content_spec( $key, $custom_tab_options_spec ) {

		global $post, $product;

		$custom_tab_options_spec = array(
			'enabled' => get_post_meta($post->ID, 'custom_tab_enabled_spec', true),
			'titlec' => get_post_meta($post->ID, 'custom_tab_title_spec', true),
		);
        $custom_tab_options_content = array(
            'media' => get_post_meta($post->ID, 'custom_tab_media_spec', true),
			'surface' => get_post_meta($post->ID, 'custom_tab_surface_spec', true),
			'material' => get_post_meta($post->ID, 'custom_tab_material_spec', true),
			'date' => get_post_meta($post->ID, 'custom_tab_date_spec', true),
            'weight' => get_post_meta($post->ID, 'custom_tab_weight_spec', true) . ' kg',
            'dimensions' => get_post_meta($post->ID, 'custom_tab_dimensions_spec', true). ' cm',
        );
       
		//echo '<h2>' . $custom_tab_options_spec['titlec'] . '</h2>';
       if($custom_tab_options_spec['enabled']){
        echo '<table class="shop_attributes"><tbody>';
        foreach($custom_tab_options_content as $name => $value){
            if($name == 'dimensions'){
                $value = tab_conv_size($value);
            }
            if($name == 'weight'){
                $value = conv_weight($value);
            }
            if($value != '' && $value != ' kg' && $value != ' cm' && $value != '0.00 inch'){
            echo '			<tr>';
            echo '				<th>'. $name .'</th>';
            echo '				<td>'. $value .'</td>';
            echo '			</tr>';
            } 
        }
        echo '			</tbody></table>';
       }
	}


/******************************************************************************/
/************************ Ajax calls ******************************************/
/******************************************************************************/

//ajax on shopping bag items number
/*if (class_exists('WooCommerce')) {
	function refresh_shopping_bag_items_number() {
		global $woocommerce;
		echo $woocommerce->cart->cart_contents_count;
		die();
	}
	add_action( 'wp_ajax_refresh_shopping_bag_items_number', 'refresh_shopping_bag_items_number' );
	add_action( 'wp_ajax_nopriv_refresh_shopping_bag_items_number', 'refresh_shopping_bag_items_number' );
}*/

/*if (class_exists('YITH_WCWL')) {
	function wishlist_shortcode_offcanvas() {
		echo do_shortcode('[mr_tailor_yith_wcwl_wishlist]');
		die;
	}
	add_action('wp_ajax_wishlist_shortcode', 'wishlist_shortcode_offcanvas');
	add_action('wp_ajax_nopriv_wishlist_shortcode', 'wishlist_shortcode_offcanvas');
}*/

// ajax refresh_dynamic_contents
//if (class_exists('YITH_WCWL')) {
	function refresh_dynamic_contents() {
		global $woocommerce, $yith_wcwl;
		$data = array(
	        'cart_count_products' => class_exists('WooCommerce') ? $woocommerce->cart->cart_contents_count : 0,
	        'wishlist_count_products' => class_exists('YITH_WCWL') ? yith_wcwl_count_products() : 0,
	    );
		wp_send_json($data);
	}
	add_action( 'wp_ajax_refresh_dynamic_contents', 'refresh_dynamic_contents' );
	add_action( 'wp_ajax_nopriv_refresh_dynamic_contents', 'refresh_dynamic_contents' );
//}



/******************************************************************************/
/*********************** mr_tailor setup **************************************/
/******************************************************************************/


if ( ! function_exists( 'mr_tailor_setup' ) ) :
function mr_tailor_setup() {
	
	global $mr_tailor_theme_options;
	
	/** Theme support **/
	add_theme_support( 'menus' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ) );
	add_theme_support( 'woocommerce');
	function custom_header_custom_bg() {
		add_theme_support( 'custom-header' );
		add_theme_support( 'custom-background' );
	}
	
	/** Add Image Sizes **/
	$shop_catalog_image_size = get_option( 'shop_catalog_image_size' );
	$shop_single_image_size = get_option( 'shop_single_image_size' );
    add_image_size('product_small_thumbnail', (int)$shop_catalog_image_size['width']/3, (int)$shop_catalog_image_size['height']/3, isset($shop_catalog_image_size['crop']) ? true : false); // made from shop_catalog_image_size
	add_image_size('shop_single_small_thumbnail', (int)$shop_single_image_size['width']/3, (int)$shop_single_image_size['height']/3, isset($shop_catalog_image_size['crop']) ? true : false); // made from shop_single_image_size
	
	
	//add_image_size('default_gallery_img', 300, 300, true);
	//add_image_size('product_small_thumbnail', 100, 100, true);
	
	/** Register menus **/ 
	register_nav_menus( array(
		'top-bar-navigation' => __( 'Top Bar Navigation', 'mr_tailor' ),
		'main-navigation' => __( 'Main Navigation', 'mr_tailor' ),
	) );

	/** Theme textdomain **/
	load_theme_textdomain( 'mr_tailor', get_template_directory() . '/languages' );
	
	/** WooCommerce Number of products displayed per page **/
	if ( (isset($mr_tailor_theme_options['products_per_page'])) ) {
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return ' . $mr_tailor_theme_options['products_per_page'] . ';' ), 20 );
	}

}
endif; // mr_tailor_setup
add_action( 'after_setup_theme', 'mr_tailor_setup' );




/******************************************************************************/
/*********************** Enable excerpts **************************************/
/******************************************************************************/

add_action('init', 'mr_tailor_post_type_support');
function mr_tailor_post_type_support() {
	add_post_type_support( 'page', 'excerpt' );
}






/******************************************************************************/
/**************************** Enqueue styles **********************************/
/******************************************************************************/

// frontend
function mr_tailor_styles() {
	
	global $mr_tailor_theme_options;
	
	wp_enqueue_style('mr_tailor-app', get_template_directory_uri() . '/css/app.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-easyzoom', get_template_directory_uri() . '/css/easyzoom.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-woocommerce-overwrite', get_template_directory_uri() . '/css/woocommerce-overwrite.css', array(), '1.0', 'all' );	
	
	wp_enqueue_style('mr_tailor-animate', get_template_directory_uri() . '/css/animate.min.css', array(), '1.0', 'all' );		
	wp_enqueue_style('mr_tailor-animations-products-grid', get_template_directory_uri() . '/css/animations-products-grid.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-fresco', get_template_directory_uri() . '/css/fresco/fresco.css', array(), '1.3.0', 'all' );
	wp_enqueue_style('mr_tailor-swiper', get_template_directory_uri() . '/css/idangerous.swiper.css', array(), '2.5.1', 'all' );
	wp_enqueue_style('mr_tailor-owl', get_template_directory_uri() . '/css/owl.carousel.css', array(), '1.3.1', 'all' );
	wp_enqueue_style('mr_tailor-owl-theme', get_template_directory_uri() . '/css/owl.theme.css', array(), '1.3.1', 'all' );
	wp_enqueue_style('mr_tailor-offcanvas', get_template_directory_uri() . '/css/offcanvas.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-nanoscroller', get_template_directory_uri() . '/css/nanoscroller.css', array(), '0.7.6', 'all' );
	wp_enqueue_style('mr_tailor-select2', get_template_directory_uri() . '/css/select2.css', array(), '3.5.1', 'all' );
	
	wp_enqueue_style('mr_tailor-defaults', get_template_directory_uri() . '/css/defaults.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-styles', get_template_directory_uri() . '/css/styles.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-responsive', get_template_directory_uri() . '/css/responsive.css', array(), '1.0', 'all' );
	
	wp_enqueue_style('mr_tailor-fonts', get_template_directory_uri() . '/inc/fonts/getbowtied-fonts/style.css', array(), '1.0', 'all' );
	wp_enqueue_style('mr_tailor-font-awesome', get_template_directory_uri() . '/inc/fonts/font-awesome/css/font-awesome.min.css', array(), '1.0', 'all' );

	wp_enqueue_style('mr_tailor-default-style', get_stylesheet_uri());		
	
	if (file_exists(dirname( __FILE__ ) . '/_theme-explorer/css/theme-explorer.css')) {
		wp_enqueue_style('mr_tailor-theme-explorer', get_template_directory_uri() . '/_theme-explorer/css/theme-explorer.css', array(), '1.0', 'all' );
	}

}
add_action( 'wp_enqueue_scripts', 'mr_tailor_styles', 99 );


// admin area
function mr_tailor_admin_styles() {
    if ( is_admin() ) {
        
		wp_enqueue_style("mr_tailor_admin_styles", get_template_directory_uri() . "/css/wp-admin-custom.css", false, "1.0", "all");
		
		if (class_exists('WPBakeryVisualComposerAbstract')) { 
			wp_enqueue_style('mr_tailor_visual_composer', get_template_directory_uri() .'/css/visual-composer.css', false, "1.0", 'all');
		}
    }
}
add_action( 'admin_enqueue_scripts', 'mr_tailor_admin_styles' );


/******************************************************************************/
/*************************** Enqueue scripts **********************************/
/******************************************************************************/

// frontend
function mr_tailor_scripts() {
	
	global $mr_tailor_theme_options;
	
	/** In Header **/
	wp_enqueue_script('mr_tailor-modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', '', '2.6.3', FALSE);
	
	if ($mr_tailor_theme_options['main_font_source'] == "2") {
		if ( (isset($mr_tailor_theme_options['main_font_typekit_kit_id'])) && ($mr_tailor_theme_options['main_font_typekit_kit_id'] != "") ) {
			wp_enqueue_script('mr_tailor-main_font_typekit', '//use.typekit.net/'.$mr_tailor_theme_options['main_font_typekit_kit_id'].'.js', array(), NULL, FALSE );
		}
	}
	
	if ($mr_tailor_theme_options['secondary_font_source'] == "2") {
		if ( (isset($mr_tailor_theme_options['secondary_font_typekit_kit_id'])) && ($mr_tailor_theme_options['secondary_font_typekit_kit_id'] != "") ) {
			wp_enqueue_script('mr_tailor-secondary_font_typekit', '//use.typekit.net/'.$mr_tailor_theme_options['secondary_font_typekit_kit_id'].'.js', array(), NULL, FALSE );
		}
	}
	
	if ( ($mr_tailor_theme_options['main_font_source'] == "2") || ($mr_tailor_theme_options['secondary_font_source'] == "2") ) {
		if ( ((isset($mr_tailor_theme_options['main_font_typekit_kit_id'])) && ($mr_tailor_theme_options['main_font_typekit_kit_id'] != "")) || ((isset($mr_tailor_theme_options['secondary_font_typekit_kit_id'])) && ($mr_tailor_theme_options['secondary_font_typekit_kit_id'] != "")) ) {
			function mr_tailor_typekit_exec() {
				echo '<script type="text/javascript">try{Typekit.load();}catch(e){}</script>';
			}
			add_action('wp_head', 'mr_tailor_typekit_exec');
		}
	}
	
	if ( ($mr_tailor_theme_options['main_font_source'] == "2") && ($mr_tailor_theme_options['secondary_font_source'] == "2") ) {
		if ( ((isset($mr_tailor_theme_options['main_font_typekit_kit_id'])) && ($mr_tailor_theme_options['main_font_typekit_kit_id'] != "")) || ((isset($mr_tailor_theme_options['secondary_font_typekit_kit_id'])) && ($mr_tailor_theme_options['secondary_font_typekit_kit_id'] != "")) ) {
			if ( $mr_tailor_theme_options['main_font_typekit_kit_id'] == $mr_tailor_theme_options['secondary_font_typekit_kit_id'] ) {
				wp_dequeue_script('mr_tailor-secondary_font_typekit');
			}
		}
	}
	
	/** In Footer **/
	
	if (file_exists(dirname( __FILE__ ) . '/js/_combined.min.js')) {
		
		wp_enqueue_script('mr_tailor-combined-scripts', get_template_directory_uri() . '/js/_combined.min.js', array('jquery'), '1.0', TRUE);
		
	} else {
		
		wp_enqueue_script('mr_tailor-foundation', get_template_directory_uri() . '/js/foundation.min.js', array('jquery'), '5.2.0', TRUE);
		wp_enqueue_script('mr_tailor-foundation-interchange', get_template_directory_uri() . '/js/foundation.interchange.js', array('jquery'), '5.2.0', TRUE);
		wp_enqueue_script('mr_tailor-isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), 'v2.0.0', TRUE);
		wp_enqueue_script('mr_tailor-imagesloaded', get_template_directory_uri() . '/js/imagesloaded.js', array('jquery'), 'v3.1.4', TRUE);
		wp_enqueue_script('mr_tailor-touchswipe', get_template_directory_uri() . '/js/jquery.touchSwipe.min.js', array('jquery'), '1.6.5', TRUE);
		wp_enqueue_script('mr_tailor-fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '1.0.3', TRUE);
		wp_enqueue_script('mr_tailor-idangerous-swiper', get_template_directory_uri() . '/js/idangerous.swiper.min.js', array('jquery'), '2.6.1', TRUE);
		wp_enqueue_script('mr_tailor-owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.1', TRUE);
		wp_enqueue_script('mr_tailor-fresco', get_template_directory_uri() . '/js/fresco.js', array('jquery'), '1.3.0', TRUE);
		wp_enqueue_script('mr_tailor-nanoscroller', get_template_directory_uri() . '/js/jquery.nanoscroller.min.js', array('jquery'), '0.7.6', TRUE);
		wp_enqueue_script('mr_tailor-select2', get_template_directory_uri() . '/js/select2.min.js', array('jquery'), '3.5.1', TRUE);
		wp_enqueue_script('mr_tailor-scroll_to', get_template_directory_uri() . '/js/jquery.scroll_to.js', array('jquery'), '1.4.5', TRUE);
		wp_enqueue_script('mr_tailor-stellar', get_template_directory_uri() . '/js/jquery.stellar.min.js', array('jquery'), '0.6.2', TRUE);
		wp_enqueue_script('mr_tailor-snapscroll', get_template_directory_uri() . '/js/jquery.snapscroll.min.js', array('jquery'), '1.6.1', TRUE);
		wp_enqueue_script('mr_tailor-easyzoom', get_template_directory_uri() . '/js/easyzoom.js', array('jquery'), '1.0', TRUE);
		wp_enqueue_script('mr_tailor-scripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0', TRUE);
		
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'mr_tailor_scripts', 99 );


function my_enqueue($hook) {
    if (( 'post-new.php' == $hook) || ( 'post.php' == $hook)) {
       // return;
		 wp_enqueue_script( 'my_admin_script', get_template_directory_uri() .'/js/admin_functions.js' );
    }
	return;

   
}
add_action( 'admin_enqueue_scripts', 'my_enqueue' );



/*********************************************************************************************/
/******************************** Fix empty title on homepage  *******************************/
/*********************************************************************************************/

/*add_filter( 'wp_title', 'mr_tailor_hack_wp_title_for_home' );
function mr_tailor_hack_wp_title_for_home( $title )
{
	if( empty( $title ) && ( is_home() || is_front_page() ) ) {
		return __( 'Home', 'mr_tailor' ) . ' | ' . get_bloginfo( 'description' );
	}
	return $title;
}*/




add_filter( 'wp_title', 'mr_tailor_wp_title', 10, 2 );
function mr_tailor_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'mr_tailor' ), max( $paged, $page ) );
	}

	return $title;
}



/******************************************************************************/
/******************** Revolution Slider set as Theme **************************/
/******************************************************************************/

if(function_exists( 'set_revslider_as_theme' )){
	add_action( 'init', 'mr_tailor_set_revslider_as_theme' );
	function mr_tailor_set_revslider_as_theme() {
		set_revslider_as_theme();
	}
}





/******************************************************************************/
/****** Register widgetized area and update sidebar with default widgets ******/
/******************************************************************************/

function mr_tailor_widgets_init() {
	
	$sidebars_widgets = wp_get_sidebars_widgets();	
	$footer_area_widgets_counter = "0";	
	if (isset($sidebars_widgets['footer-widget-area'])) $footer_area_widgets_counter  = count($sidebars_widgets['footer-widget-area']);
	
	switch ($footer_area_widgets_counter) {
		case 0:
			$footer_area_widgets_columns ='large-12';
			break;
		case 1:
			$footer_area_widgets_columns ='large-12 medium-12 small-12';
			break;
		case 2:
			$footer_area_widgets_columns ='large-6 medium-6 small-12';
			break;
		case 3:
			$footer_area_widgets_columns ='large-4 medium-6 small-12';
			break;
		case 4:
			$footer_area_widgets_columns ='large-3 medium-6 small-12';
			break;
		case 5:
			$footer_area_widgets_columns ='footer-5-columns large-2 medium-6 small-12';
			break;
		case 6:
			$footer_area_widgets_columns ='large-2 medium-6 small-12';
			break;
		default:
			$footer_area_widgets_columns ='large-2 medium-6 small-12';
	}
	
	//default sidebar
	register_sidebar(array(
		'name'          => __( 'Sidebar', 'mr_tailor' ),
		'id'            => 'default-sidebar',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	));
	
	//footer widget area
	register_sidebar( array(
		'name'          => __( 'Footer Widget Area', 'mr_tailor' ),
		'id'            => 'footer-widget-area',
		'before_widget' => '<div class="' . $footer_area_widgets_columns . ' columns"><aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside></div>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	//catalog widget area
	register_sidebar( array(
		'name'          => __( 'Shop Sidebar', 'mr_tailor' ),
		'id'            => 'catalog-widget-area',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'mr_tailor_widgets_init' );



// Remove Woocommerce prettyPhoto
add_action( 'wp_enqueue_scripts', 'mr_tailor_remove_woo_lightbox', 99 );
function mr_tailor_remove_woo_lightbox() {
    wp_dequeue_script('prettyPhoto-init');
}





/*********************************************************************************************/
/*********** Remove Admin Bar - Only display to administrators *******************************/
/*********************************************************************************************/

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
		show_admin_bar(false);
	}
}





/*********************************************************************************************/
/************************** WooCommerce Custom Out of Stock **********************************/
/*********************************************************************************************/


add_action( 'init', 'out_of_stock_stuff' );
function out_of_stock_stuff() {

	global $mr_tailor_theme_options;
	
	if (isset($mr_tailor_theme_options['out_of_stock_text'])) {		
		add_filter( 'woocommerce_get_availability', 'custom_get_availability', 1, 2);
		function custom_get_availability( $availability, $_product ) {
			global $mr_tailor_theme_options;
			if ( !$_product->is_in_stock() ) $availability['availability'] = __($mr_tailor_theme_options['out_of_stock_text'], 'mr_tailor');
			return $availability;
		}		
	}

}





/*********************************************************************************************/
/****************************** WooCommerce Custom Sale **************************************/
/*********************************************************************************************/


add_action( 'init', 'sale_stuff' );
function sale_stuff() {

	global $mr_tailor_theme_options;

	if (isset($mr_tailor_theme_options['sale_text'])) {
		add_filter('woocommerce_sale_flash', 'custom_sale_flash', 10, 3);
		function custom_sale_flash($text, $post, $_product) {
			global $mr_tailor_theme_options;
			return '<span class="onsale">'.__($mr_tailor_theme_options['sale_text'], 'mr_tailor').'</span>';  
		}
	}

}


/******************************************************************************/
/****** Add Fresco to Galleries ***********************************************/
/******************************************************************************/

add_filter( 'wp_get_attachment_link', 'sant_prettyadd', 10, 6);
function sant_prettyadd ($content, $id, $size, $permalink, $icon, $text) {
    if ($permalink) {
    	return $content;    
    }
    $content = preg_replace("/<a/","<span class=\"fresco\" data-fresco-group=\"\"", $content, 1);
    return $content;
}




//Adds gallery shortcode defaults of size="medium" and columns="2"
/*
function custom_gallery_atts( $out, $pairs, $atts ) {
   
    $atts = shortcode_atts( array(
        'size' => 'default_gallery_img',
    ), $atts );

    $out['size'] = $atts['size'];

    return $out;

}
add_filter( 'shortcode_atts_gallery', 'custom_gallery_atts', 10, 3 );
*/



/******************************************************************************/
/****** Add Font Awesome to Redux *********************************************/
/******************************************************************************/

function newIconFont() {

    wp_register_style(
        'redux-font-awesome',
        get_template_directory_uri() . '/inc/fonts/font-awesome/css/font-awesome.min.css',
        array(),
        time(),
        'all'
    );  
    wp_enqueue_style( 'redux-font-awesome' );
}
// This example assumes the opt_name is set to mr_tailor_theme_options.
add_action( 'redux/page/mr_tailor_theme_options/enqueue', 'newIconFont' );





/******************************************************************************/
/* WooCommerce Update Number of Items in the cart *****************************/
/******************************************************************************/

add_action('woocommerce_ajax_added_to_cart', 'mr_tailor_ajax_added_to_cart');
function mr_tailor_ajax_added_to_cart() {

	add_filter('add_to_cart_fragments', 'mr_tailor_shopping_bag_items_number');
	function mr_tailor_shopping_bag_items_number( $fragments ) 
	{
		global $woocommerce;
		ob_start(); ?>

		<script>
		(function($){
			$('.shopping-bag-button').trigger('click');
		})(jQuery);
		</script>
        
        <span class="shopping_bag_items_number"><?php echo $woocommerce->cart->cart_contents_count; ?></span>

		<?php
		$fragments['.shopping_bag_items_number'] = ob_get_clean();
		return $fragments;
	}

}



/******************************************************************************/
/* WooCommerce Number of Related Products *************************************/
/******************************************************************************/

function woocommerce_output_related_products() {
	$atts = array(
		'posts_per_page' => '12',
		'orderby'        => 'rand'
	);
	woocommerce_related_products($atts);
}



/******************************************************************************/
/* WooCommerce Add data-src & lazyOwl to Thumbnails ***************************/
/******************************************************************************/
function woocommerce_get_product_thumbnail( $size = 'product_small_thumbnail', $placeholder_width = 0, $placeholder_height = 0  ) {
	global $post;

	if ( has_post_thumbnail() ) {
		$image_src = wp_get_attachment_image_src( get_post_thumbnail_id(), 'shop_catalog' );
		return get_the_post_thumbnail( $post->ID, $size, array('data-src' => $image_src[0], 'class' => 'lazyOwl') );
	} elseif ( wc_placeholder_img_src() ) {
		return wc_placeholder_img( $size );
	}
}

function woocommerce_subcategory_thumbnail( $category ) {
	$small_thumbnail_size  	= apply_filters( 'single_product_small_thumbnail_size', 'product_small_thumbnail' );
	$thumbnail_size  		= apply_filters( 'single_product_small_thumbnail_size', 'shop_catalog' );
	$dimensions    			= wc_get_image_size( $small_thumbnail_size );
	$thumbnail_id  			= get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true  );

	if ( $thumbnail_id ) {
		$image_small = wp_get_attachment_image_src( $thumbnail_id, $small_thumbnail_size  );
		$image_small = $image_small[0];
		$image = wp_get_attachment_image_src( $thumbnail_id, $thumbnail_size  );
		$image = $image[0];
	} else {
		$image = $image_small = wc_placeholder_img_src();
		
	}

	if ( $image_small )
		echo '<img data-src="' . esc_url( $image ) . '" class="lazyOwl" src="' . esc_url( $image_small ) . '" alt="' . esc_attr( $category->name ) . '" width="' . esc_attr( $dimensions['width'] ) . '" height="' . esc_url( $dimensions['height'] ) . '" />';
}





/******************************************************************************/
/* WooCommerce Wrap Oembed Stuff **********************************************/
/******************************************************************************/
add_filter('embed_oembed_html', 'mr_tailor_embed_oembed_html', 99, 4);
function mr_tailor_embed_oembed_html($html, $url, $attr, $post_id) {
	return '<div class="video-container">' . $html . '</div>';
}




/******************************************************************************/
/****** Overwrite WooCommerce Widgets *****************************************/
/******************************************************************************/
 

function overwride_woocommerce_widgets() { 
	if ( class_exists( 'WC_Widget_Cart' ) ) {
		include_once( 'inc/widgets/woocommerce-cart.php' ); 
		register_widget( 'mr_tailor_WC_Widget_Cart' );
	}
}
add_action( 'widgets_init', 'overwride_woocommerce_widgets', 15 );




/******************************************************************************/
/****** Set woocommerce images sizes ******************************************/
/******************************************************************************/

/**
 * Hook in on activation
 */
global $pagenow;
if ( is_admin() && isset( $_GET['activated'] ) && $pagenow == 'themes.php' ) add_action( 'init', 'mr_tailor_woocommerce_image_dimensions', 1 );

/**
 * Define image sizes
 */
function mr_tailor_woocommerce_image_dimensions() {
  	$catalog = array(
		'width' 	=> '350',	// px
		'height'	=> '435',	// px
		'crop'		=> 1 		// true
	);

	$single = array(
		'width' 	=> '570',	// px
		'height'	=> '708',	// px
		'crop'		=> 1 		// true
	);

	$thumbnail = array(
		'width' 	=> '70',	// px
		'height'	=> '87',	// px
		'crop'		=> 1 		// false
	);

	// Image sizes
	update_option( 'shop_catalog_image_size', $catalog ); 		// Product category thumbs
	update_option( 'shop_single_image_size', $single ); 		// Single product image
	update_option( 'shop_thumbnail_image_size', $thumbnail ); 	// Image gallery thumbs
}

/******************************************************************************/
/****** Change user role *********************************************************/
/******************************************************************************/


add_action( 'user_register', 'myplugin_registration_save' );

function myplugin_registration_save( $user_id ) {

    if ( isset( $_POST['artist'] )) {
        $u = new WP_User( $user_id );
        $u->remove_role( 'customer' );
        $u->add_role( 'multi_vendor' );
    } 
}

/******************************************************************************/
/****** Change Item Price *********************************************************/
/******************************************************************************/

function getDisplayName($name){
    $display = str_replace('pa_', '', $name);
    $newName = str_replace('_', ' ', $display);
    return $newName;
}

function getTermNameFromId($id,$taxonomy){
    $term = get_term( $id, $taxonomy );
    return $term->name;
}


function getDisAttName($product){
    $attributes = $product->get_attributes();
    return $attributes;
}


function woo_add_cart_fee() {
    global $woocommerce;
    $items = $woocommerce->cart->get_cart();
    $array = array();
    
    foreach($items as $item => $values) { 
        $array[$values['product_id']] = $values['quantity'];
    } 
    
    $total = 0;
    if(isset($_SESSION['custom_attributes'])){
        foreach ($_SESSION['custom_attributes'] as $custom){
            //var_dump((int)$custom['Extra']);
            if(isset($custom['Extra'])){
            $a = (int)$custom['Extra'];
            $b = (int)$custom['ID'];
            $c = (int)$array[$b];
            $total = $total + $a*$c;
            }
        }
    }

    $woocommerce->cart->add_fee( __('Custom', 'woocommerce'), $total );
}

add_action( 'woocommerce_cart_calculate_fees', 'woo_add_cart_fee' );/*a
function scotty_woocommerce_add_cart_item( $cart_item ) {
    $cart_item['data']->adjust_price( $_SESSION['custom_price'] );
    return $cart_item;
}
add_filter( 'woocommerce_add_cart_item', 'scotty_woocommerce_add_cart_item', 5, 1 );

function scotty_get_cart_item_from_session( $cart_item, $values ) {
    $cart_item = scotty_woocommerce_add_cart_item( $cart_item );
    return $cart_item;
}
add_filter( 'woocommerce_get_cart_item_from_session', 'scotty_get_cart_item_from_session', 5, 2 );
*/
/*
add_action('wp_ajax_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');
add_action('wp_ajax_nopriv_wdm_add_user_custom_data_options', 'wdm_add_user_custom_data_options_callback');

function wdm_add_user_custom_data_options_callback()
{
      //Custom data - Sent Via AJAX post method
      $product_id = $_POST['id']; //This is product ID
         session_start();
      $user_custom_data_values = $_SESSION['current_data']; //This is User custom value sent via AJAX
      $_SESSION['wdm_user_custom_data'] = $user_custom_data_values;
      die();
}

add_filter('woocommerce_add_cart_item_data','wdm_add_item_data',1,2);
 
if(!function_exists('wdm_add_item_data'))
{
    function wdm_add_item_data($cart_item_data,$product_id)
    {
        //Here, We are adding item in WooCommerce session with, wdm_user_custom_data_value name//
        global $woocommerce;
        session_start();    
        if (isset($_SESSION['wdm_user_custom_data'])) {
            $option = $_SESSION['wdm_user_custom_data'];       
            $new_value = array('wdm_user_custom_data_value' => $option);
        }
        if(empty($option))
            return $cart_item_data;
        else
        {    
            if(empty($cart_item_data))
                return $new_value;
            else
                return array_merge($cart_item_data,$new_value);
        }
        unset($_SESSION['wdm_user_custom_data']); 
        //Unset our custom session variable, as it is no longer needed.
    }
}

add_filter('woocommerce_get_cart_item_from_session', 'wdm_get_cart_items_from_session', 1, 3 );
if(!function_exists('wdm_get_cart_items_from_session'))
{
    function wdm_get_cart_items_from_session($item,$values,$key)
    {
        if (array_key_exists( 'wdm_user_custom_data_value', $values ) )
        {
        $item['wdm_user_custom_data_value'] = $values['wdm_user_custom_data_value'];
        }       
        return $item;
    }
}

add_filter('woocommerce_checkout_cart_item_quantity','wdm_add_user_custom_option_from_session_into_cart',1,3);  
add_filter('woocommerce_cart_item_price','wdm_add_user_custom_option_from_session_into_cart',1,3);
if(!function_exists('wdm_add_user_custom_option_from_session_into_cart'))
{
 function wdm_add_user_custom_option_from_session_into_cart($product_name, $values, $cart_item_key )
    {
        //code to add custom data on Cart & checkout Page//    
        if(count($values['wdm_user_custom_data_value']) > 0)
        {
            $return_string = $product_name . "</a><dl class='variation'>";
            $return_string .= "<table class='wdm_options_table' id='" . $values['product_id'] . "'>";
            $return_string .= "<tr><td>" . $values['wdm_user_custom_data_value'] . "</td></tr>";
            $return_string .= "</table></dl>"; 
            return $return_string;
        }
        else
        {
            return $product_name;
        }
    }
}

add_action('woocommerce_add_order_item_meta','wdm_add_values_to_order_item_meta',1,2);
if(!function_exists('wdm_add_values_to_order_item_meta'))
{
  function wdm_add_values_to_order_item_meta($item_id, $values)
  {
        global $woocommerce,$wpdb;
        $user_custom_values = $values['wdm_user_custom_data_value'];
        if(!empty($user_custom_values))
        {
            wc_add_order_item_meta($item_id,'wdm_user_custom_data',$user_custom_values);  
        }
  }
}

add_action('woocommerce_before_cart_item_quantity_zero','wdm_remove_user_custom_data_options_from_cart',1,1);
if(!function_exists('wdm_remove_user_custom_data_options_from_cart'))
{
    function wdm_remove_user_custom_data_options_from_cart($cart_item_key)
    {
        global $woocommerce;
        // Get cart
        $cart = $woocommerce->cart->get_cart();
        // For each item in cart, if item is upsell of deleted product, delete it
        foreach( $cart as $key => $values)
        {
        if ( $values['wdm_user_custom_data_value'] == $cart_item_key )
            unset( $woocommerce->cart->cart_contents[ $key ] );
        }
    }
}*/
/******************************************************************************/
/****** Share Product *********************************************************/
/******************************************************************************/

function getbowtied_single_share_product() {
    global $post, $product, $mr_tailor_theme_options;
    if ( (isset($mr_tailor_theme_options['sharing_options'])) && ($mr_tailor_theme_options['sharing_options'] == "1" ) ) :
?>

    <div class="box-share-master-container">
        <div class="box-share-container product-share-container">

            <a class="trigger-share-list" href="#"><i class="fa fa-share-alt"></i><?php _e( 'Share this product', 'mr_tailor' )?></a>
            <div class="box-share-list">

                <?php
                    //Get the Thumbnail URL
                    $src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), false, '' );
                ?>

                <div class="box-share-list-inner">
                    <a href="//www.facebook.com/sharer.php?u=<?php the_permalink(); ?>" class="box-share-link" target="_blank"><i class="fa fa-facebook"></i><span>Facebook</span></a>
                    <a href="//twitter.com/share?url=<?php the_permalink(); ?>" class="box-share-link" target="_blank"><i class="fa fa-twitter"></i><span>Twitter</span></a>
                    <a href="//plus.google.com/share?url=<?php the_permalink(); ?>" class="box-share-link" target="_blank"><i class="fa fa-google-plus"></i><span>Google</span></a>
                    <a href="//pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&amp;media=<?php echo esc_url($src[0]) ?>&amp;description=<?php echo urlencode(get_the_title()); ?>" class="box-share-link" target="_blank"><i class="fa fa-pinterest"></i><span>Pinterest</span></a>
                </div><!--.box-share-list-inner-->

            </div><!--.box-share-list-->
        </div>
    </div><!--.box-share-master-container-->

<?php
    endif;
}
add_filter( 'woocommerce_single_product_summary', 'getbowtied_single_share_product', 50 );


/**
 * Add Photographer Name and URL fields to media uploader
 *
 * @param $form_fields array, fields to include in attachment form
 * @param $post object, attachment record in database
 * @return $form_fields, modified form fields
 */
 
function be_attachment_field_credit( $form_fields, $post ) {
    if(get_post_meta( $post->ID, 'be_dpi_value', true ) != ''){
        $val = get_post_meta( $post->ID, 'be_dpi_value', true );
    }else{
        $val = 180;
    }

	$form_fields['be_dpi_value'] = array(
		'label' => 'DPI Value',
		'input' => 'text',
		'value' => $val,
		'helps' => 'If provided, photo sizes will be displayed',
	);

	$options = array( '1' => 'Yes', '0' => 'No' );
	
	// Get currently selected value
	$selected = get_post_meta( $post->ID, 'be_dpi_select', true );
	
	// If no selected value, default to 'No'
	if( !isset( $selected ) ) 
		$selected = '0';
	
	// Display each option	
    $html = "<div class='rotator-include-option' id='sizes_warper'><ul style='display:none;' name='attachments[$post->ID][be-rotator-include]' id='select_sizes'>";
	foreach ( $options as $value => $label ) {	
		$html .= "<li id='li_{$css_id}'>$label</li>";
	} 
    $html .= "</ul></div>
    <button onclick='refresh_sizes()'>Generate Sizes</button>";
    $out[] = $html;

    $form_fields['be_dpi_select'] = array(
		'label' => 'Suggested Sizes',
		'input' => 'html',
		'html'  => join("\n", $out),
	);
	
	return $form_fields;
}
add_filter( 'attachment_fields_to_edit', 'be_attachment_field_credit', 10, 2 );
/**
 * Save values of Photographer Name and URL in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */
function be_attachment_field_credit_save( $post, $attachment ) {
	if( isset( $attachment['be_dpi_value'] ) )
		update_post_meta( $post['ID'], 'be_dpi_value', $attachment['be_dpi_value'] );

    if( isset( $attachment['be_dpi_select'] ) )
		update_post_meta( $post['ID'], 'be_dpi_select', $attachment['be_dpi_select'] );

	return $post;
}
add_filter( 'attachment_fields_to_save', 'be_attachment_field_credit_save', 10, 2 );

/********************************************************************************/

function prefix_ajax_add_list() {
    $post_id = $_POST['post_id'];
    $json = stripslashes($_POST['list']);
    $list = (json_decode($json));
    $endList = array();
    foreach($list as $att){
        $endList[$att[0]] = $att[1];
    }
    // After inserting post
    //wcproduct_set_attributes($post_id, $endList);
   
}

add_action( 'wp_ajax_add_list', 'prefix_ajax_add_list' );
add_action( 'wp_ajax_nopriv_add_list', 'prefix_ajax_add_list' );




if ( ! isset( $content_width ) ) $content_width = 640; /* pixels */



function to_inch($value,$type){
    if($type == 'number'){
        $new_val = ((float)$value / 2.54);
        return number_format((float)$new_val, 2, '.', '');
    }
    if($type == 'text'){
        $new_value = str_replace(' cm','',$value);
        $values = explode(' x ',$new_value);
        $result_str ='';
        $i = 0;
        $len = count($values);
        foreach ($values as $val){
                $new_val = ((float)$val / 2.54);
                $result_str.= number_format((float)$new_val, 2, '.', '');
                if ($i != $len - 1) {
                    $result_str.= ' x ';
                }else{
                    $result_str.= ' inch';    
                }
            $i++;            
        }
        return ($result_str);
    }
}



// Display User IP in WordPress


function get_the_user_ip() {
if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
//check ip from share internet
$ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
//to check ip is pass from proxy
$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
$ip = $_SERVER['REMOTE_ADDR'];
}
return apply_filters( 'wpb_get_ip', $ip );
}

add_shortcode('show_ip', 'get_the_user_ip');

// Currency Converter
function _cc($price){
    global $WOOCS;
    $cur = $WOOCS->current_currency;
    $currencies = $WOOCS->get_currencies();

    if($currencies[$WOOCS->current_currency]['position'] == 'right'){
    return apply_filters('woocs_exchange_value', $price).$currencies[$WOOCS->current_currency]['symbol'];
    }
    else{
        //var_dump(apply_filters('woocs_exchange_value', $price));
    return $currencies[$WOOCS->current_currency]['symbol'].apply_filters('woocs_exchange_value', $price);
    }
}



//add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
function wpa83367_price_html( $price, $product ){
	$currency=get_woocommerce_currency_symbol();	
	if($currency=='&#36;') { 
		$price=str_replace( '&#36;</span>', '</span>', $price );
		$price=str_replace( '<span class="amount">', '<span class="amount">'.$currency, $price );
		return $price;
	}
	else{
		return $price;
	}
}

// Get country from vendor id
function get_country($vendor_id){
    // 133
    $sql = "SELECT * FROM wp_options WHERE option_name  = 'multi_vendor_".$vendor_id."_v_country'";
    global $wpdb;
    $country = $wpdb->get_results( $sql , OBJECT )[0]->option_value;
    
    return $country;
}

// On order complete - send PDF
include_once('page-pdf-certificate.php');
function mysite_woocommerce_order_status_completed( $order_id ) {
    
    $order = wc_get_order( $order_id );
    $i = 1;
    foreach( $order->get_items() as $item_id => $item ) {
        
        // Get the order info
        global $wpdb;
        $order_item_id = $wpdb->get_results( 'SELECT order_item_id FROM wp_woocommerce_order_items WHERE order_item_type = "line_item" AND order_id = '.$order_id." AND order_item_name = '".$item['name']."'", OBJECT )[0]->order_item_id ;
        $results = $wpdb->get_results( 'SELECT meta_value FROM wp_woocommerce_order_itemmeta WHERE meta_key = "wdm_user_custom_data" AND order_item_id = '.$order_item_id, OBJECT );
        
        // Get the attributes info
        $attrs = json_decode($results[0]->meta_value);
        $artist_info = $wpdb->get_results( 'SELECT * FROM wp_multi_vendor WHERE orderid = '.$order_id, OBJECT )[0];
 
        // Get the artist info
        $term = $wpdb->get_results( 'SELECT * FROM wp_woocommerce_termmeta WHERE meta_key = "fp_mv_vendor_admins" AND woocommerce_term_id = '.$artist_info->vendorid, OBJECT )[0];
        $user_id = unserialize(unserialize($term->meta_value))[0];
        $user_info = get_userdata($user_id); 
        $user_post_id = get_user_meta($user_id,'signature')[0];
        $signature_img = $wpdb->get_results( 'SELECT * FROM wp_postmeta WHERE meta_key = "_wp_attached_file" AND post_id = '.$user_post_id, OBJECT )[0];

        // Declaration of the artist info
        $artist = new stdClass();      
        $artist->name = $artist_info->vendorname;
        $artist->lname = "";
        $artist->id = $artist_info->id;
        $artist->signture_url = "http://www.c14.co.il/lemon/wp-content/uploads/".$signature_img->meta_value;
         
        // Declaration of the arwork info
        $artwork = new stdClass();
        $artwork->order_id = $order_id;
        $artwork->idx = $i;
        $artwork->title = $item['name'];
        $artwork->completion = date( 'd.m.Y', current_time( 'timestamp', 0 ) );
        $artwork->material = get_post_meta($attrs->ID, 'custom_tab_material_spec', true);
        $artwork->size = get_post_meta($attrs->ID, 'custom_tab_dimensions_spec', true)." cm";
        $artwork->weight = get_post_meta($attrs->ID, 'custom_tab_weight_spec', true)." kg";
        $artwork->number = $attrs->ID;//5;

        // Destination Email
        $to = 'michael@lemonjuice.com';//'tomers@kcsnet.net'; //$user_info->user_email /* Artist Email */;
        
        // Execute the creation & send to mail
        create_pdf($artist,$artwork,$to);
        $i++;
    }
}
add_action( 'woocommerce_order_status_completed',
'mysite_woocommerce_order_status_completed' );

// Add the product custom attributes selections to db
add_action('woocommerce_add_order_item_meta','wdm_add_values_to_order_item_meta',1,2);

          function wdm_add_values_to_order_item_meta($item_id) 
          {
              
               if(isset($_SESSION['custom_attributes'])){ 
                      foreach($_SESSION['custom_attributes'] as $attrs)
                      {
                          //$item_id = $attrs['ID'];
                          $value = json_encode($attrs);
                            global $woocommerce,$wpdb;
                            $user_custom_values = $value;
                            if(!empty($user_custom_values))
                            {
                               // wc_add_order_item_meta($item_id,'wdm_user_custom_data',$user_custom_values);  
                                //var_dump($item_id);
                                
                                $wpdb->insert( 
                                    'wp_woocommerce_order_itemmeta', 
                                    array( 
                                        'order_item_id' => $item_id, 
                                        'meta_key' => 'wdm_user_custom_data',
                                        'meta_value' => $user_custom_values
                                    )
                                );

                            }
                      }
                }       
          }
        //wdm_add_values_to_order_item_meta();


// Add custom css to admin panel
add_action('admin_head', 'my_custom_fonts');
function my_custom_fonts() {
  echo "<style>
   label[for='fp_mv_commissions_rate'], input[name='fp_mv_commissions_rate'] {
    display: none;
}
.user-aim-wrap,.user-yim-wrap,.user-jabber-wrap{
        display:none;
    }
    .taxonomy-multi_vendor form h3,.taxonomy-multi_vendor .wpseo-taxonomy-form,.taxonomy-multi_vendor #acf-price-value, .taxonomy-multi_vendor #acf-size-value, .taxonomy-multi_vendor #acf-frame_texture {
    display: none;
}
ul.country-selector-list li:nth-of-type(2) {
    display: none;
}
.type_box {
    display: none !important;
}
  </style>";
}

// Custom Settings Page
function theme_settings_page()
{
    ?>
	    <div class="wrap">
	    <h1>Website Options</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}

// Custom Setting Menu Tab
function add_theme_menu_item()
{
	add_menu_page("Website Options", "Website Options", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

// Inputs at the Custom Settings
function display_63_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="63_text" id="63_text" value="<?php echo get_option('63_text'); ?>" />
    <?php }
function display_63_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="63_url" id="63_url" value="<?php echo get_option('63_url'); ?>" />
    <?php }
function display_65_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="65_text" id="65_text" value="<?php echo get_option('65_text'); ?>" />
    <?php }
function display_65_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="65_url" id="65_url" value="<?php echo get_option('65_url'); ?>" />
    <?php }
function display_249_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="249_text" id="249_text" value="<?php echo get_option('249_text'); ?>" />
    <?php }
function display_249_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="249_url" id="249_url" value="<?php echo get_option('249_url'); ?>" />
    <?php }
function display_64_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="64_text" id="64_text" value="<?php echo get_option('64_text'); ?>" />
    <?php }
function display_64_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="64_url" id="64_url" value="<?php echo get_option('64_url'); ?>" />
<?php  }
function display_67_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="67_text" id="67_text" value="<?php echo get_option('67_text'); ?>" />
    <?php }
function display_67_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="67_url" id="67_url" value="<?php echo get_option('67_url'); ?>" />
<?php  }
function display_250_text_element()
{ ?>
    	<input type="text" style='width:400px;' name="250_text" id="250_text" value="<?php echo get_option('250_text'); ?>" />
    <?php }
function display_250_url_element()
{ ?>
    	<input type="text" style='width:400px;' name="250_url" id="250_url" value="<?php echo get_option('250_url'); ?>" />
<?php  }
function display_prespex_element()
{ ?>
    	<input type="text" style='width:400px;' name="prespex" id="prespex" value="<?php echo get_option('prespex'); ?>" />
<?php  }
function display_kappa_element()
{ ?>
    	<input type="text" style='width:400px;' name="kappa" id="kappa" value="<?php echo get_option('kappa'); ?>" />
<?php  }
function display_package1_element()
{ ?>
    	<input type="text" style='width:400px;' name="package1" id="package1" value="<?php echo get_option('package1'); ?>" />
<?php  }
function display_package2_element()
{ ?>
    	<input type="text" style='width:400px;' name="package2" id="package2" value="<?php echo get_option('package2'); ?>" />
<?php  }
function display_package3_element()
{ ?>
    	<input type="text" style='width:400px;' name="package3" id="package3" value="<?php echo get_option('package3'); ?>" />
<?php  }
function display_profit1_element()
{ ?>
    	<input type="text" style='width:400px;' name="profit1" id="profit1" value="<?php echo get_option('profit1'); ?>" />
<?php  }
function display_profit2_element()
{ ?>
    	<input type="text" style='width:400px;' name="profit2" id="profit2" value="<?php echo get_option('profit2'); ?>" />
<?php  }
function display_clearing_element()
{ ?>
    	<input type="text" style='width:400px;' name="clearing" id="clearing" value="<?php echo get_option('clearing'); ?>" />
<?php  }
function display_shipping1_element()
{ ?>
    	<input type="text" style='width:400px;' name="shipping1" id="shipping1" value="<?php echo get_option('shipping1'); ?>" />
<?php  }
function display_shipping2_element()
{ ?>
    	<input type="text" style='width:400px;' name="shipping2" id="shipping2" value="<?php echo get_option('shipping2'); ?>" />
<?php  }
function display_shipping3_element()
{ ?>
    	<input type="text" style='width:400px;' name="shipping3" id="shipping3" value="<?php echo get_option('shipping3'); ?>" />
<?php  }

// Register the Custom Inputs
function display_theme_panel_fields()
{
	add_settings_section("section", "Question Marks", null, "theme-options");
    
	add_settings_field("63_text", "Photo Paper Text", "display_63_text_element", "theme-options", "section");
    add_settings_field("63_url", "Photo Paper Link", "display_63_url_element", "theme-options", "section");
    add_settings_field("65_text", "Acrylic Text", "display_65_text_element", "theme-options", "section");
    add_settings_field("65_url", "Acrylic Link", "display_65_url_element", "theme-options", "section");
    add_settings_field("249_text", "Acrylic Block Text", "display_249_text_element", "theme-options", "section");
    add_settings_field("249_url", "Acrylic Block Link", "display_249_url_element", "theme-options", "section");
    add_settings_field("64_text", "Canvas Text", "display_64_text_element", "theme-options", "section");
    add_settings_field("64_url", "Canvas Link", "display_64_url_element", "theme-options", "section");
    add_settings_field("67_text", "MDF Text", "display_67_text_element", "theme-options", "section");
    add_settings_field("67_url", "MDF Link", "display_67_url_element", "theme-options", "section");
    add_settings_field("250_text", "Mini Canvas Text", "display_250_text_element", "theme-options", "section");
    add_settings_field("250_url", "Mini Canvas Link", "display_250_url_element", "theme-options", "section");
    
    
    add_settings_field("prespex", "Perspex Value", "display_prespex_element", "theme-options", "section");  
    add_settings_field("kappa", "Kappa Value", "display_kappa_element", "theme-options", "section");
    add_settings_field("package1", "Package1 Value", "display_package1_element", "theme-options", "section");
    add_settings_field("package2", "Package2 Value", "display_package2_element", "theme-options", "section");
    add_settings_field("package3", "Package3 Value", "display_package3_element", "theme-options", "section");
    add_settings_field("profit1", "Profit Picture Value", "display_profit1_element", "theme-options", "section");
    add_settings_field("profit2", "Profit Print Value", "display_profit2_element", "theme-options", "section");
    add_settings_field("clearing", "Clearing Fees Value", "display_clearing_element", "theme-options", "section");
    add_settings_field("shipping1", "Shipping1 Value", "display_shipping1_element", "theme-options", "section");
    add_settings_field("shipping2", "Shipping2 Value", "display_shipping2_element", "theme-options", "section");
    add_settings_field("shipping3", "Shipping3 Value", "display_shipping3_element", "theme-options", "section");
    
    register_setting("section", "63_text");
    register_setting("section", "63_url");
    register_setting("section", "65_text");
    register_setting("section", "65_url");
    register_setting("section", "249_text");
    register_setting("section", "249_url");
    register_setting("section", "64_text");
    register_setting("section", "64_url");
    register_setting("section", "67_text");
    register_setting("section", "67_url");
    register_setting("section", "250_text");
    register_setting("section", "250_url");
    
    register_setting("section", "prespex");
    register_setting("section", "kappa");
    register_setting("section", "package1");
    register_setting("section", "package2");
    register_setting("section", "package3");
    register_setting("section", "profit1");
    register_setting("section", "profit2");
    register_setting("section", "clearing");
    register_setting("section", "shipping1");
    register_setting("section", "shipping2");
    register_setting("section", "shipping3");
}
add_action("admin_init", "display_theme_panel_fields");

// Custom Pricing Option Page
function theme_pricing_page()
{
    ?>
	    <div class="wrap">
	    <h1>Pricing Options</h1>
	    <form method="post" action="options.php">
	        <?php
	            settings_fields("section");
	            do_settings_sections("theme-options");      
	            submit_button(); 
	        ?>          
	    </form>
		</div>
	<?php
}

// convert sizes
function conv_size($size){
    if($_SESSION['dim'] == 'CM'){
        $first = number_format((float)explode('" × ',$size)[0]*2.54, 2, '.', '');
        $second = number_format((float)str_replace('"','',(explode('" × ',$size)[1]))*2.54, 2, '.', '');
        return $first." cm × ".$second." cm";
    }return $size;
}
// convert tab sizes
function tab_conv_size($size){
    if($_SESSION['dim'] != 'CM'){
        $first = number_format((float)explode(' x ',$size)[0]/2.54, 2, '.', '');
        $second = number_format((float)str_replace('cm','',(explode(' x ',$size)[1]))/2.54, 2, '.', '');
        return $first." × ".$second." inch";
    }return $size;
}

// convert weights
function conv_weight($weight){
    if($_SESSION['weight'] == 'POUNDS'){
        $val = number_format(((float)str_replace(' kg','',$weight))*2.20462262, 2, '.', '')." pounds";
        return $val;
    }
    return $weight;
}

// Top lvl category
function get_top_parent_category($cat_ID)
{
    $cat = get_category( $cat_ID );
    $new_cat_id = $cat->category_parent;

    if($new_cat_id != "0")
    {
        return (get_top_parent_category($new_cat_id));
    }
    return $cat_ID;
}

// Change tabs order
add_filter( 'woocommerce_product_tabs', 'woo_reorder_tabs', 98 );
function woo_reorder_tabs( $tabs ) {

	$tabs['reviews']['priority'] = 55;			// Reviews first
	$tabs['description']['priority'] = 30;			// Description second
	$tabs['additional_information']['priority'] = 25;	// Additional information third
    $tabs['custom-tab-second']['priority'] = 20;	// Artist information fourth
    
	return $tabs;
}

// Email Notifications
function get_user_role() {
	global $current_user;

	$user_roles = $current_user->roles;
	$user_role = array_shift($user_roles);

	return $user_role;
}

add_action( 'save_post', 'my_project_updated_send_email' ); 
 function my_project_updated_send_email( $post_id ) {
    //verify post is not a revision 
    $role = get_user_role();
    if($role == 'multi_vendor'){
        
        if ( !wp_is_post_revision( $post_id ) ) {

             $post_title = get_the_title(); 
            //$post_url = get_permalink( $post_id ); 



             $subject = 'A post has been updated'; 
             $message = "A post has been updated on your website:\n\n";
             $message .= "Name: " .$post_title. "\nID: ".$post_id."\n\n"; 
             //send email to admin 
             wp_mail( 'tomers@kcsnet.net'/*get_option( 'admin_email' )*/, $subject, $message ); 
       } 
    }
     
} 
function user_profile_update($userid) {
   $role = get_user_role();
   if($role == 'multi_vendor'){
        $user = get_user_by( 'id', $userid );
        $subject = 'A user has been updated'; 
        $message = "A user has been updated on your website:\n\n";
        $message .= "Username: " .$user->user_login. "\nID: ".$userid."\n\n"; 
        wp_mail('tomers@kcsnet.net'/*get_option( 'admin_email' )*/, $subject, $message);
   }
}
add_action('profile_update','user_profile_update');


