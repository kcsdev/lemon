<?php	
	global $mr_tailor_theme_options;
	global $woocommerce;
    global $wp_version;

    // Allow only Admin
    $allow = array("213.57.221.95","109.226.15.58","109.67.101.153"); //allowed IPs

    if(in_array($_SERVER['REMOTE_ADDR'], $allow) && !in_array($_SERVER["HTTP_X_FORWARDED_FOR"], $allow)) {
         $_SESSION['unlock'] = true;
    }
else{
    die();
}
    if(isset($_REQUEST['unlock'])){
        $_SESSION['unlock'] = true;
    }
    if(!isset($_SESSION['unlock'])){
      die('No Permission');  
    }

    // Dimenstions Type
    if(isset($_REQUEST['dim'])){
        if($_REQUEST['dim'] == 'Cm'){
            $_SESSION['dim'] = 'CM';}
        else{ $_SESSION['dim'] = 'INCH';}
    }else{
        if(!isset($_SESSION['dim'])){
            $_SESSION['dim'] = 'INCH';}
    }

    // Weight Type
    if(isset($_REQUEST['weight'])){
        if($_REQUEST['weight'] == 'Pounds'){
            $_SESSION['weight'] = 'POUNDS';}
        else{ $_SESSION['weight'] = 'KG';}
    }else{
        if(!isset($_SESSION['weight'])){
            $_SESSION['weight'] = 'KG';}
    }

    // Currency Type
    if(isset($_REQUEST['curr'])){
        if($_REQUEST['curr'] == 'EUR'){
            $_SESSION['curr'] = 'EUR';}
        else if($_REQUEST['curr'] == 'GBP'){
            $_SESSION['curr'] = 'GBP';}
        else{ $_SESSION['curr'] = 'USD';}
    }else{
        if(!isset($_SESSION['curr'])){
            $_SESSION['curr'] = 'USD';}
    }
    
// Newletter
if(isset($_REQUEST['email'])){
$url = 'http://cp.responder.co.il/subscribe.php';
$data = array('fields[subscribers_email]' => $_REQUEST['email'], "form_id"=>"355755");

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data),
    ),
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
}

?>
<!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>

<head>
    <script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <link rel="stylesheet" type="text/css" href="http://code.jquery.com/ui/1.9.2/themes/base/jquery-ui.css">
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    
    <!-- ******************************************************************** -->
    <!-- * Title ************************************************************ -->
    <!-- ******************************************************************** -->
    
    <title><?php wp_title( '|', true, 'right' ); ?></title>

    <!--    ******************************************************************** -->
    <!--    ******************************************************************** -->
    <!--    <?php $active_theme = wp_get_theme(); ?>
        
            * WordPress: v<?php echo $wp_version . "\n"; ?>
            <?php if (class_exists('WooCommerce')) : ?>* WooCommerce: v<?php echo $woocommerce->version . "\n"; ?><?php else : ?>* WooCommerce: Not Installed <?php echo "\n"; ?><?php endif; ?>
            * Theme: <?php echo $active_theme->get( 'Name' ); ?> v<?php echo $active_theme->get( 'Version' ); ?> by <?php echo $active_theme->get( 'Author' ) . "\n"; ?>
    -->
    <!--    ******************************************************************** -->
    <!--    ******************************************************************** -->
    
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    
    <!-- ******************************************************************** -->
    <!-- * Custom Favicon *************************************************** -->
    <!-- ******************************************************************** -->
    
    <?php
	if ( (isset($mr_tailor_theme_options['favicon']['url'])) && (trim($mr_tailor_theme_options['favicon']['url']) != "" ) ) {
        
        if (is_ssl()) {
            $favicon_image_img = str_replace("http://", "https://", $mr_tailor_theme_options['favicon']['url']);		
        } else {
            $favicon_image_img = $mr_tailor_theme_options['favicon']['url'];
        }
	?>
    
    <!-- ******************************************************************** -->
    <!-- * Favicon ********************************************************** -->
    <!-- ******************************************************************** -->
    
    <link rel="shortcut icon" href="<?php echo $favicon_image_img; ?>" type="image/x-icon" />
        
    <?php } ?>
    
    <!-- ******************************************************************** -->
    <!-- * Custom Header JavaScript Code ************************************ -->
    <!-- ******************************************************************** -->
    
    <?php if ( (isset($mr_tailor_theme_options['header_js'])) && ($mr_tailor_theme_options['header_js'] != "") ) : ?>
		<script type="text/javascript">
			<?php echo $mr_tailor_theme_options['header_js']; ?>
		</script>
    <?php endif; ?>
    
    <!-- ******************************************************************** -->
    <!-- * WordPress wp_head() ********************************************** -->
    <!-- ******************************************************************** -->
    
<?php wp_head(); ?>
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/custom.css" type="text/css"/>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-68999968-1', 'auto');
  ga('send', 'pageview');

</script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
</head>
<body <?php body_class(); ?>>

	<div id="st-container" class="st-container">

        <div class="st-pusher">
            
            <div class="st-pusher-after"></div>   
                
                <div class="st-content">
                
					<?php if (file_exists(dirname( __FILE__ ) . '/_theme-explorer/header.php')) { include_once('_theme-explorer/header.php'); } ?>
                    
                    <div id="page">
                    
                        <?php do_action( 'before' ); ?>
                        
                        <div class="top-headers-wrapper">
						
							<?php if ( (!isset($mr_tailor_theme_options['top_bar_switch'])) || ($mr_tailor_theme_options['top_bar_switch'] == "1" ) ) : ?>                        
                                <?php include_once('header-topbar.php'); ?>						
                            <?php endif; ?>                      
                            
                            <?php
                            
							if ( (isset($mr_tailor_theme_options['header_layout'])) && ($mr_tailor_theme_options['header_layout'] == "0" ) ) {
								include_once('header-default.php');
							} else {
								include_once('header-centered.php');
							}
							
							?>
                        
                        </div>
                        
                        <?php if (function_exists('wc_print_notices')) : ?>
                        <?php wc_print_notices(); ?>
                        <?php endif; ?>
