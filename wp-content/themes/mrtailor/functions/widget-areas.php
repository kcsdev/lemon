<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */

function blog_sidebar_area() {

	register_sidebar( array(
		'name'          => 'Blog_sidbar',
		'id'            => 'blog_sidbar',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'blog_sidebar_area' );
?>