<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */

    header('X-Frame-Options: GOFORIT'); 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $mr_tailor_theme_options;

$category_header_src = "";

if (function_exists('woocommerce_get_header_image_url')) $category_header_src = woocommerce_get_header_image_url();

get_header('shop');

    $shop_page_has_sidebar = false;
    
    if ( (isset($mr_tailor_theme_options['shop_layout'])) && ($mr_tailor_theme_options['shop_layout'] == "0" ) ) {
        
        $shop_page_has_sidebar = false; 
    
    } else {
    
        if ( is_active_sidebar( 'catalog-widget-area' ) ){
            $shop_page_has_sidebar = true;
        }
    
    }

// Get the current tag id
    $cur_term = get_term_by( 'slug', get_query_var('term'), get_query_var('taxonomy') );
    $tag_id = $cur_term->term_id;

// Check if it is an artist page
if (get_query_var( 'taxonomy' ) == 'multi_vendor'){ $is_artist = true; }else{ $is_artist=false; };

?>
    <div id="primary" class="content-area catalog-page <?php echo $shop_page_has_sidebar ? 'with-sidebar' : 'without-sidebar'; ?>">
        
        <div class="category_header <?php if ($category_header_src != "") : ?>with_featured_img<?php endif; ?>" <?php if ($category_header_src != "") : ?>style="background-image:url(<?php echo $category_header_src ; ?>)<?php endif; ?>">
        
            <div class="category_header_overlay"></div>
            
            <div class="row">
                <div class="large-8 large-centered columns">

                    <?php do_action('woocommerce_before_main_content'); ?>

                    <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

                        <h1 class="page-title shop_page_title"><?php woocommerce_page_title(); ?></h1>

                    <?php endif; ?>

                    <?php do_action( 'woocommerce_archive_description' ); ?>
                    
                </div>
            </div>
            
        </div>
        <div id="content" class="site-content" role="main">
            <div class="row">

                <div class="large-12 columns">  
            <?php if(!$is_artist): ?>
                    
                

                        <?php
                        // Find the category + category parent, if applicable
                        $term           = get_queried_object();
                        $parent_id      = empty( $term->term_id ) ? 0 : $term->term_id;
                        $categories     = get_terms('product_cat', array('hide_empty' => 0, 'parent' => $parent_id));
                        ?>
                    
                        <?php
                    
                        $show_categories = FALSE;

                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == '') ) $show_categories = FALSE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'products') ) $show_categories = FALSE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ) $show_categories = TRUE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'both') ) $show_categories = TRUE;
                        
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == '') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'products') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ) $show_categories = TRUE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'both') ) $show_categories = TRUE;

                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'products') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories' ) ) $show_categories = TRUE;
                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'both') ) $show_categories = TRUE;
                        
                        if ( isset($_GET["s"]) && $_GET["s"] != '' ) $show_categories = FALSE;

                        // echo "Shop Page Display: " . get_option('woocommerce_shop_page_display') . "<br />";                        
                        // echo "Default Category Display: " . get_option('woocommerce_category_archive_display') . "<br />";
                        // echo "Display type (edit product category): " . get_woocommerce_term_meta($term->term_id, 'display_type', true) . "<br />";
                    
                        ?>
                    
                        <?php if (!is_paged()) : //show categories only on first page ?>
                        
                            <?php if ($show_categories == TRUE) : ?>
    
                                <?php if ($categories) : ?>
                                
                                <div class="row">
                                    <div class="categories_grid">
                                        
                                        <?php $cat_counter = 0; ?>
                                        
                                        <?php $cat_number = count($categories); ?>
                                        
                                        <?php foreach($categories as $category) : ?>
                                            
                                            <?php                        
                                                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                                                $image = wp_get_attachment_url( $thumbnail_id );
                                                $cat_class = "";
                                            ?>
                                        
                                            <?php
                                        
                                                if (is_shop() && get_option('woocommerce_shop_page_display') == 'both') $cat_class = "original_grid";
                                                if (is_product_category() && get_option('woocommerce_category_archive_display') == 'both') $cat_class = "original_grid";
                                                
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'products') $cat_class = "";
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories') $cat_class = "";
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'both') $cat_class = "original_grid";
    
                                            ?>
                                            
                                            <?php 
                                                if($cat_class != "original_grid") 
                                                {
                                                    $cat_counter++;                                        
    
                                                    switch ($cat_number) {
                                                        case 1:
                                                            $cat_class = "one_cat_" . $cat_counter;
                                                            break;
                                                        case 2:
                                                            $cat_class = "two_cat_" . $cat_counter;
                                                            break;
                                                        case 3:
                                                            $cat_class = "three_cat_" . $cat_counter;
                                                            break;
                                                        case 4:
                                                            $cat_class = "four_cat_" . $cat_counter;
                                                            break;
                                                        case 5:
                                                            $cat_class = "five_cat_" . $cat_counter;
                                                            break;
                                                        default:
                                                            if ($cat_counter < 7) {
                                                                $cat_class = $cat_counter;
                                                            } else {
                                                                $cat_class = "more_than_6";
                                                            }
                                                    }
                                                    
                                                }
                                            ?>
                                            
                                            <div class="category_<?php echo $cat_class; ?>">
                                                <div class="category_grid_box">
                                                    <span class="category_item_bkg" style="background-image:url(<?php echo $image; ?>)"></span> 
                                                    <a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>" class="category_item">
                                                        <span class="category_name"><?php echo $category->name; ?></span>
                                                    </a>
                                                </div>
                                                
                                            </div>
                                        
                                        <?php endforeach; ?>
                                        
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                </div>
                                
                                <?php endif; ?>
                            
                            <?php endif; ?>
                        
                        <?php endif; ?>
                                    
                        <?php
                    
                        $show_products = TRUE;

                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ) $show_products = FALSE;

                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ) $show_products = FALSE;

                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories' ) ) $show_products = FALSE;
                        
                        if ( isset($_GET["s"]) && $_GET["s"] != '' ) $show_products = TRUE;
                    
                        ?>
                        
                        <?php if ($show_products == TRUE) : ?>
                
                            <?php if ( have_posts() ) : ?>
                            
                                <style>
                                    .categories_grid { margin-bottom: 0; }
                                </style>
                                
                                <div class="catalog_top row">
                                    <?php do_action( 'woocommerce_before_shop_loop' );  ?>
                                </div>
                                
                                <div class="row">
                                    <div class="large-12 columns">
                                        <hr class="catalog_top_sep" />
                                    </div><!-- .columns -->
                                </div>
                    
                                <div class="row">
                                    
                                    <?php if ($shop_page_has_sidebar) : ?>
                                        
                                    <div class="large-3 columns show-for-large-up">
                                        <div class="shop_sidebar wpb_widgetised_column">
                                            <?php dynamic_sidebar('catalog-widget-area'); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="large-9 columns">
                                            
                                    <?php else :  ?>
                                        
                                    <div class="large-12 columns">
                                    
                                    <?php endif; ?>
                        
                                        <div class="active_filters_ontop"><?php the_widget( 'WC_Widget_Layered_Nav_Filters', 'title=' ); ?></div>
                        
                                        <?php woocommerce_product_loop_start(); ?>            
                                            <?php while ( have_posts() ) : the_post(); ?>                            
                                                <?php woocommerce_get_template_part( 'content', 'product' ); ?>                            
                                            <?php endwhile; // end of the loop. ?>                            
                                        <?php woocommerce_product_loop_end(); ?>
                                        
                                        <div class="woocommerce-after-shop-loop-wrapper">
                                            <?php do_action( 'woocommerce_after_shop_loop' ); ?>
                                        </div>
                                        
                                    </div><!-- .columns -->
                                </div><!--.row-->
                                
                            <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                    
                                 <?php if (!is_paged()) : //show categories only on first page ?>
                                    <?php if ($show_categories == TRUE) : ?>
                                        <?php if ($categories) : ?>
                                            <style>

                                                .no-products-info { margin-top: -34px; }
                                                
                                                /* min-width 641px, medium screens */
                                                @media only screen and (min-width: 40.063em) {
                                                    .no-products-info { margin-top: -156px; }
                                                }

                                            </style>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                                <?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>
                    
                            <?php endif; ?>
                        
                        <?php endif; ?>
                
                    <?php do_action('woocommerce_after_main_content'); ?>

                    
    
                </div><!-- #content -->
                <?php else: ?><?php if (get_field("youtube_link", "multi_vendor_" .$tag_id) != '') { ?>
                  <script type="text/javascript" src="../../html5lightbox/jquery.js"></script>
<script type="text/javascript" src="../../html5lightbox/html5lightbox.js"></script>
                    
                    <div id="mydiv" style="display:none;">
  <div class="lightboxcontainer">
	
	<div class="lightboxright">
	 <?php 

echo preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"400\" height=\"200\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",get_field("youtube_link", "multi_vendor_" .$tag_id));
        ?> 
	</div>
	<div style="clear:both;"></div>
</div></div>
                    
                    <a href="#mydiv" class="html5lightbox" data-width=800 data-height=400 title="Inline Div">Get to know the artist</a>
                    <style type="text/css">div#html5-watermark {
    display: none !important;
}
                        div#html5-text {
    display: none !important;
}
                        a.html5lightbox {
    text-align: center;
    display: block;
}
.lightboxcontainer {
  width:100%;
  text-align:left;
}
.lightboxleft {
  width: 40%;
  float:left;
}
.lightboxright {
  width: 100%;
  float:left;
}
.lightboxright iframe {
  min-height: 390px;
}
.divtext {
  margin: 36px;
}
@media (max-width: 800px) {
  .lightboxleft {
    width: 100%;
  }
  .lightboxright {
    width: 100%;
  }
  .divtext {
    margin: 12px;
  }
}
</style>
 
                    <?php } ?>
                    <div class="tabs">
                        <ul class="tab-links">
                            <li class="active"><a href="#tab1">About</a></li>
                            <li><a href="#tab2">Products</a></li>
                        </ul>
                     
                        <div class="tab-content">
                            <div id="tab1" class="tab active">
                                
                                <?php $error = 'There is no information about the artist yet.<br><br>'; 
                                $txt = get_field("tell_us_about_yourself", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Tell us about yourself: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("education:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Education: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("years_of_practice:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Years of practice: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("information_about_the_work_style:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Information about the work style: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("details_about:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Details about the materials and techniques you use: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("why_you_create_this_type_of_art:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Why you create this type of art: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("where_you_find_inspiration:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Where you find inspiration: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("which_other_artists:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Which other artists or movements influence you: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("future_plans:_", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Future plans: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                
                                <?php $txt = get_field("exhibitions", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Exhibitions: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; } ?>
                                <?php $txt = get_field("biography", "multi_vendor_" .$tag_id);
                                    if ($txt != ''){ ?>
                                <h5>Biography: </h5>
                                <?php echo '<p>'.$txt.'</p>'; $error = ''; }
                                echo $error;?>
                                
                            </div>
                            <div id="tab2" class="tab">
                                
                                <?php 
                        // Find the category + category parent, if applicable
                        $term           = get_queried_object();
                        $parent_id      = empty( $term->term_id ) ? 0 : $term->term_id;
                        $categories     = get_terms('product_cat', array('hide_empty' => 0, 'parent' => $parent_id));
                        ?>
                    
                        <?php
                    
                        $show_categories = FALSE;

                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == '') ) $show_categories = FALSE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'products') ) $show_categories = FALSE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ) $show_categories = TRUE;
                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'both') ) $show_categories = TRUE;
                        
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == '') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'products') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ) $show_categories = TRUE;
                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'both') ) $show_categories = TRUE;

                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'products') ) $show_categories = FALSE;
                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories' ) ) $show_categories = TRUE;
                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'both') ) $show_categories = TRUE;
                        
                        if ( isset($_GET["s"]) && $_GET["s"] != '' ) $show_categories = FALSE;

                        // echo "Shop Page Display: " . get_option('woocommerce_shop_page_display') . "<br />";                        
                        // echo "Default Category Display: " . get_option('woocommerce_category_archive_display') . "<br />";
                        // echo "Display type (edit product category): " . get_woocommerce_term_meta($term->term_id, 'display_type', true) . "<br />";
                    
                        ?>
                    
                        <?php if (!is_paged()) : //show categories only on first page ?>
                        
                            <?php if ($show_categories == TRUE) : ?>
    
                                <?php if ($categories) : ?>
                                
                                <div class="row">
                                    <div class="categories_grid">
                                        
                                        <?php $cat_counter = 0; ?>
                                        
                                        <?php $cat_number = count($categories); ?>
                                        
                                        <?php foreach($categories as $category) : ?>
                                            
                                            <?php                        
                                                $thumbnail_id = get_woocommerce_term_meta( $category->term_id, 'thumbnail_id', true );
                                                $image = wp_get_attachment_url( $thumbnail_id );
                                                $cat_class = "";
                                            ?>
                                        
                                            <?php
                                        
                                                if (is_shop() && get_option('woocommerce_shop_page_display') == 'both') $cat_class = "original_grid";
                                                if (is_product_category() && get_option('woocommerce_category_archive_display') == 'both') $cat_class = "original_grid";
                                                
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'products') $cat_class = "";
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories') $cat_class = "";
                                                if (is_product_category() && get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'both') $cat_class = "original_grid";
    
                                            ?>
                                            
                                            <?php 
                                                if($cat_class != "original_grid") 
                                                {
                                                    $cat_counter++;                                        
    
                                                    switch ($cat_number) {
                                                        case 1:
                                                            $cat_class = "one_cat_" . $cat_counter;
                                                            break;
                                                        case 2:
                                                            $cat_class = "two_cat_" . $cat_counter;
                                                            break;
                                                        case 3:
                                                            $cat_class = "three_cat_" . $cat_counter;
                                                            break;
                                                        case 4:
                                                            $cat_class = "four_cat_" . $cat_counter;
                                                            break;
                                                        case 5:
                                                            $cat_class = "five_cat_" . $cat_counter;
                                                            break;
                                                        default:
                                                            if ($cat_counter < 7) {
                                                                $cat_class = $cat_counter;
                                                            } else {
                                                                $cat_class = "more_than_6";
                                                            }
                                                    }
                                                    
                                                }
                                            ?>
                                            
                                            <div class="category_<?php echo $cat_class; ?>">
                                                <div class="category_grid_box">
                                                    <span class="category_item_bkg" style="background-image:url(<?php echo $image; ?>)"></span> 
                                                    <a href="<?php echo get_term_link( $category->slug, 'product_cat' ); ?>" class="category_item">
                                                        <span class="category_name"><?php echo $category->name; ?></span>
                                                    </a>
                                                </div>
                                            </div>
                                        
                                        <?php endforeach; ?>
                                        
                                        <div class="clearfix"></div>
                                        
                                    </div>
                                </div>
                                
                                <?php endif; ?>
                            
                            <?php endif; ?>
                        
                        <?php endif; ?>
                                    
                        <?php
                    
                        $show_products = TRUE;

                        if ( is_shop() && (get_option('woocommerce_shop_page_display') == 'subcategories') ) $show_products = FALSE;

                        if ( is_product_category() && (get_option('woocommerce_category_archive_display') == 'subcategories') ) $show_products = FALSE;

                        if ( is_product_category() && (get_woocommerce_term_meta($term->term_id, 'display_type', true) == 'subcategories' ) ) $show_products = FALSE;
                        
                        if ( isset($_GET["s"]) && $_GET["s"] != '' ) $show_products = TRUE;
                    
                        ?>
                        
                        <?php if ($show_products == TRUE) : ?>
                
                            <?php if ( have_posts() ) : ?>
                            
                                <style>
                                    .categories_grid { margin-bottom: 0; }
                                </style>
                                
                                <div class="catalog_top row">
                                    <?php do_action( 'woocommerce_before_shop_loop' );  ?>
                                </div>
                                
                                <div class="row">
                                    <div class="large-12 columns">
                                        <hr class="catalog_top_sep" />
                                    </div><!-- .columns -->
                                </div>
                    
                                <div class="row">
                                    
                                    <?php if ($shop_page_has_sidebar) : ?>
                                        
                                    <div class="large-3 columns show-for-large-up">
                                        <div class="shop_sidebar wpb_widgetised_column">
                                            <?php dynamic_sidebar('catalog-widget-area'); ?>
                                        </div>
                                    </div>
                                    
                                    <div class="large-9 columns">
                                            
                                    <?php else :  ?>
                                        
                                    <div class="large-12 columns">
                                    
                                    <?php endif; ?>
                        
                                        <div class="active_filters_ontop"><?php the_widget( 'WC_Widget_Layered_Nav_Filters', 'title=' ); ?></div>
                        
                                        <?php woocommerce_product_loop_start(); ?>            
                                            <?php while ( have_posts() ) : the_post(); ?>                            
                                                <?php woocommerce_get_template_part( 'content', 'product' ); ?>                            
                                            <?php endwhile; // end of the loop. ?>                            
                                        <?php woocommerce_product_loop_end(); ?>
                                        
                                        <div class="woocommerce-after-shop-loop-wrapper">
                                            <?php do_action( 'woocommerce_after_shop_loop' ); ?>
                                        </div>
                                        
                                    </div><!-- .columns -->
                                </div><!--.row-->
                                
                            <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>
                    
                                 <?php if (!is_paged()) : //show categories only on first page ?>
                                    <?php if ($show_categories == TRUE) : ?>
                                        <?php if ($categories) : ?>
                                            <style>

                                                .no-products-info { margin-top: -34px; }
                                                
                                                /* min-width 641px, medium screens */
                                                @media only screen and (min-width: 40.063em) {
                                                    .no-products-info { margin-top: -156px; }
                                                }

                                            </style>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                
                                <?php woocommerce_get_template( 'loop/no-products-found.php' ); ?>
                    
                            <?php endif; ?>
                        
                        <?php endif; ?>
                
                    <?php do_action('woocommerce_after_main_content'); ?>
                            </div>

                        </div>
                    </div>
                <?php endif; ?>           
            </div><!-- .columns -->        
        </div><!-- .row -->
                <?php if($is_artist){ ?>
                   <div id="comments" class="comments-area">
                
                <?php if ( have_comments() ) : ?>
                    <h2 class="comments-title">
                        <?php
                            printf( _nx( 'One reply on &ldquo;%2$s&rdquo;', '%1$s replies on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', 'mr_tailor' ),
                                number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );
                        ?>
                    </h2>
            
                    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
                    <nav id="comment-nav-above" class="comment-navigation" role="navigation">
                        <h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'mr_tailor' ); ?></h1>
                        <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'mr_tailor' ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'mr_tailor' ) ); ?></div>
                    </nav><!-- #comment-nav-above -->
                    <?php endif; // check for comment navigation ?>
            
                    <ul class="comment-list">
                        <?php
                            /* Loop through and list the comments. Tell wp_list_comments()
                             * to use mr_tailor_comment() to format the comments.
                             * If you want to override this in a child theme, then you can
                             * define mr_tailor_comment() and that will be used instead.
                             * See mr_tailor_comment() in inc/template-tags.php for more.
                             */
                            wp_list_comments( array( 'callback' => 'mr_tailor_comment' ) );
                        ?>
                    </ul><!-- .comment-list -->
            
                    <?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // are there comments to navigate through ?>
                    <nav id="comment-nav-below" class="comment-navigation" role="navigation">
                        <h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'mr_tailor' ); ?></h1>
                        <div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'mr_tailor' ) ); ?></div>
                        <div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'mr_tailor' ) ); ?></div>
                    </nav><!-- #comment-nav-below -->
                    <?php endif; // check for comment navigation ?>
            
                <?php endif; // have_comments() ?>
            
                <?php
                    // If comments are closed and there are comments, let's leave a little note, shall we?
                    if ( ! comments_open() && '0' != get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
                ?>
                    <p class="no-comments"><?php _e( 'Comments are closed.', 'mr_tailor' ); ?></p>
                <?php endif; ?>
                
                
                
                
                
                
                
                
                
                
                
                <?php 
                
                $commenter 	= wp_get_current_commenter();
                $req 		= get_option( 'require_name_email' );
                $aria_req 	= ( $req ? " aria-required='true'" : '' );
                
                $getbowtied_comment_args = array(		
                
                    'title_reply' => __( 'Leave a Reply', 'mr_tailor' ),
                
                    'fields' => apply_filters( 'comment_form_default_fields', array(
                    
                        'author' 	=> 	'<div class="row"><div class="large-6 columns"><p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'mr_tailor' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
                                        '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30"' . $aria_req . ' /></p></div>',
                        'email'  	=> 	'<div class="large-6 columns"><p class="comment-form-email"><label for="email">' . __( 'Email Address', 'mr_tailor' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label> ' .
                                        '<input id="email" name="email" type="email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" size="30"' . $aria_req . ' /></p></div></div>',
                        'url'   	=> 	'<div class="row"><div class="large-12 columns"><p class="comment-form-url"><label for="url">' . __( 'Website', 'mr_tailor' ) . '</label> ' .
                                        '<input id="url" name="url" type="url" value="' . esc_attr( $commenter['comment_author_url'] ) . '" size="30" /></p></div></div>'
                    
                    )),
                    
                    'comment_field' =>	'<div class="row"><div class="large-12 columns"><p>' .	
                                        '<label for="comment">' . __( 'Message', 'mr_tailor' ) . ( $req ? ' <span class="required">*</span>' : '' ) . '</label>' .
                                        '<textarea id="comment" name="comment" cols="45" rows="8" ' . $aria_req . '></textarea>' .	
                                        '</p></div></div>',
                
                    //'comment_notes_after'  => '<p class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s', 'mr_tailor' ), ' <code>' . allowed_tags() . '</code>' ) . '</p>',
                    'comment_notes_after'  => '',
            
                );
                
                echo '<div class="row"><div class="large-12 columns">';
                    
                    //comment_form($getbowtied_comment_args);
                    
                    ob_start();
                    comment_form($getbowtied_comment_args);
                    $form = ob_get_clean(); 
                    echo str_replace('id="submit"','id="submit" class="button"', $form);
                    
                echo '</div></div>';
                
                ?>
            
            </div><!-- #comments -->
    
<?php } ?>
    </div><!-- #primary -->

<?php get_footer('shop'); ?>

<script type="text/javascript">
   jQuery(document).ready(function() {
        jQuery('.tabs .tab-links a').on('click', function(e)  {
            var currentAttrValue = jQuery(this).attr('href');
 
        // Show/Hide Tabs
        jQuery('.tabs ' + currentAttrValue).show().siblings().hide();
 
        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');
 
        e.preventDefault();
        });
    });
</script>