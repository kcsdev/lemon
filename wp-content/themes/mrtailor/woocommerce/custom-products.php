 <?php
    // Get all attributes
    global $post;
    $attributes = $product->get_attributes();
    $terms = get_the_terms( $post->ID, 'product_cat' );
$product_cat_id = 0;

// Linked Products
    $linked_div ='';
     $linked_product = get_field('linked_product',$product->id);                      
    if($linked_product){
                                                $cats = get_the_terms( $linked_product->ID, 'product_cat' );
                                                foreach ($cats as $cat) {
                                                   
                                                    $product_cat_id = $cat->term_id;        
                                                    $product_cat_name = $cat->name;
                                                    $parent = $cat;
                                                    $max = 0;
                                                    
                                                    while($product_cat_id != 33 && $product_cat_id != 31 && $max<10){
                                                        $parent = get_term_by ('id', $parent->parent ,'product_cat' );
                                                        $product_cat_name = $parent->name;
                                                        $product_cat_id = $parent->term_id;
                                                        $max++;
                                                    }
                                                    
                                                    break;
                                                }
                                                
                                                $linked_div = '<div class="linked_product" style="width:150px;"><a style="text-align:center; display:block;" href='.$linked_product->guid.'>Also in '.$product_cat_name.'</a></div>';
    
                                     }
if($product->id != 620 && $product->id != 1203){
    foreach ($terms as $term) {
        $product_cat_id = $term->term_id;
        break;
    }
}
if($product_cat_id == 31 || $product->id == 620){
    ?> <style>
.type-product .owl-wrapper{
    width:auto !important;
}
</style> <?php 
    
    function get_cAttr($aName){
        $terms = get_the_terms( $product->id, $aName);
        return $terms;
    }

    // Change Product Price
    add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

    function add_custom_price( $cart_object , $custom_price ) {  
        foreach ( $cart_object->cart_contents as $key => $value ) {
            $value['data']->price = $value['data']->price + $custom_price;
        }
    }
    
    
                                             
    // Display All Attributes
    echo "<div id='wraper_header'><h3>Photo Paper</h3></div><div id='select_area'><ul class='select_wraper'>";
    $lists = '';

    // Array sort
    $found = $others = array();
    foreach($attributes as $key => $value) {
        
        if ($key == 'pa_print_type') {
            $found[$key] = $value;
        }else{
            $others[$key] = $value;
        }
    }
  
    $attributes = array_merge($found,$others);

    foreach ($attributes as $aName => $attr){
        
        $display = getDisplayName($aName);
        $style = '';
        if($aName === 'pa_frame_color'){ 
            $style = 'style="display:none;"';
        }
        if($aName === 'pa_frame_style'){ 
            $style = 'style="display:none;"';
        }
        if($aName === 'pa_mat_color'){
            $style = 'style="display:none;"';
        }
        if($aName === 'pa_hanging'){
            $style = 'style="display:none;"';
        }
        if($aName === 'pa_paper_type'){
            $style = 'style="display:none;"';
        }
        if($aName === 'pa_frame_type'){
            $style = 'style="display:none;"';
        }
        
        $lists .= "<li ".$style."><a tab='".$aName."'>".$display."</a></li>";
        
        unset($style);
    } 
    
    echo $lists."</ul><div class='selection_tab'>";

    // Display Selections
    foreach ($attributes as $aName => $attr){
        $cAttr = get_cAttr($aName);
        
        // Array sort
        $found = $others = array();
        if($cAttr){
        foreach($cAttr as $key => $value) {
            if ($value->name == 'None' || $value->name == 'Photo Paper' || $value->name == 'Roll' || $value->name == 'Glossy') {
                $found[] = $value;
            }else{
                $others[] = $value;
            }
        }
        }
        $cAttr = array_merge($found,$others);
        
        echo "<div class='radio_opts' id='".$aName."'><br>";
        
        $is_first = true;
        $is_range = false;
        
        foreach ($cAttr as $innerAttr){
                //var_dump($innerAttr);
                $price_val = get_field(  'price-value' , $aName.'_'.$innerAttr->term_id);
                if($price_val == ''){
                    $price_val = 0;
                }
                $selected = '';
                if(isset($_SESSION['custom_attributes'][$product->id])){
                    
                    if($_SESSION['custom_attributes'][$product->id]['Attributes'][$aName] == $innerAttr->term_id)
                    {
                        $selected = "checked";
                    }
                }
                else if($innerAttr->name == "None" || $innerAttr->name == "Photo Paper"  || $innerAttr->name == "Roll" || $innerAttr->name == "Glossy"){
                    $selected = "checked";
                }else if($is_first){
                    $selected = "checked";    
                }
                $is_first = false;
               
                // Convert Sizes
                if($aName == 'pa_size'){
                   $innerAttr->name = conv_size($innerAttr->name); 
                }
            $color = $innerAttr->name;
                if($color == 'Dark Brown'){ $color = '#4C2B00'; }
                if($color == 'Honey Brown'){ $color = '#FBA32F'; }
                
                // Visual Color Selects
                if($aName == 'pa_frame_color'){
                    echo "<input type='radio' style='display:none' name='".$aName."' ".$selected." data-id='".$innerAttr->term_id."' value ='".$innerAttr->name."'><div class='color_picker' style='background:".$color.";'></div>";
                 
                } // Create Size
               elseif( $product->id == 620 && $aName == 'pa_size'){
                   if(!$is_range){
                    echo "<input id='iRange' type='range' min='5' max='15' step='0.1' name='".$aName."' value ='10' oninput='changeISize(this.value)' onchange='changeISize(this.value)'> ";
                    echo "<span id='isize_txt_span'><span>10 Inch</span></span><br>";
                       $is_range = true;
                   }
               }
                // Other Case
                else{
                    echo "<input type='radio' name='".$aName."' ".$selected." data-id='".$innerAttr->term_id."' value ='".$innerAttr->name."'>".$innerAttr->name; 
                    if($aName == 'pa_print_type'){
                         $help_url = get_option($innerAttr->term_id.'_url');
                         $help_text = get_option($innerAttr->term_id.'_text');
                        echo "<div class='help_div' style='display:inline-block;margin-left: 5px;'><span class='question'>?</span><input class='help_url' type='hidden' value='".$help_url."'/><input class='help_text' type='hidden' value='".$help_text."'/></div>";
                    }
                    echo "<br>";
                }
               
            } 
         echo "</div>";
    } 
    echo "</div></div>";
?>

<!-- Attributes Execution -->
<script>
    jQuery( document ).ready(function() {
        
        // Declaration of variables
        var site_url = '<?php bloginfo('url'); ?>';
        window.ajaxstatus = true;
        
        jQuery('.product_infos .price').append('<img id="price_loader" style="display:none; width:40px; height:40px;" src="'+site_url+'/wp-content/themes/mrtailor/images/loader.gif"></img>');
        
        // Tab effects
        jQuery('.select_wraper li:eq(0)').addClass('active_tab');
        window.att_tab = jQuery('.select_wraper li:eq(0)').find('a').attr('tab');
        jQuery('.select_wraper li').each(function(){
                 
                 jQuery(this).click(function(){
                    jQuery('.select_wraper li').each(function(){
                        jQuery(this).removeClass('active_tab');
                    });
                    jQuery(this).addClass('active_tab');
                    window.att_tab = jQuery(this).find('a').attr('tab');
                    jQuery('.radio_opts').each(function(){
                          jQuery(this).hide();
                    });
                    jQuery('#'+att_tab).show();
                    show_color_select();
                 });
                    
        });

        // toggle radio buttons on\off
        function toggle_buttons(type){
            if (type == 'on'){
                jQuery("#select_area .radio_opts input").attr('disabled',false);
                /*jQuery('#select_area .radio_opts').each(function(){
                    jQuery(this).
                }*/
            }else if(type == 'off'){
                jQuery("#select_area .radio_opts input").attr('disabled',true);
            }
        }
        // show/hide extra selections
        function show_color_select(){
            // Set wraper header text
            jQuery("#wraper_header h3").text(jQuery('#pa_print_type input[type="radio"]:checked').val());
            
            // Frame Color Selection
            if( window.att_tab != 'pa_frame' || jQuery('#pa_frame input[type="radio"]:checked').val() != "Color"){
                jQuery('#pa_frame_color').hide();
            }else{
                jQuery('#pa_frame_color').show();
            }
            
            // Frame Style Selection
            if( window.att_tab != 'pa_frame' || jQuery('#pa_frame input[type="radio"]:checked').val() != "Classic"){
                jQuery('#pa_frame_style').hide();
                jQuery('#select_area').css({'min-height':'310px'});
            }else{
                jQuery('#pa_frame_style').show();
                jQuery('#select_area').css({'min-height':'470px'});
            }

            // Frame Type Selection
            if( window.att_tab != 'pa_frame' || jQuery('#pa_frame input[type="radio"]:checked').val() == "None"){
                jQuery('#pa_frame_type').hide();
            }else{
                jQuery('#pa_frame_type').show();
            }
            
            // Mat Color Selection
            if( window.att_tab != 'pa_mat' || jQuery('#pa_mat input[type="radio"]:checked').val() == "None"){
                jQuery('#pa_mat_color').hide();
            }else{
                jQuery('#pa_mat_color').show();
            }
            
            // Acrylic Hangings Selection
            if(jQuery('.select_wraper li').find("a[tab='pa_hanging']").length > 0){
                if( jQuery('#pa_print_type input[type="radio"]:checked').val() != "Acrylic"){
                    jQuery('.select_wraper li').find("a[tab='pa_hanging']").parent().hide();
                    jQuery('#pa_hanging input[value="None"]').prop("checked", true);
                }else{
                    jQuery('.select_wraper li').find("a[tab='pa_hanging']").parent().show();
                }
            }
            
     
            
            // Print & Canvas Options Selection
            if(jQuery('.select_wraper li').find("a[tab='pa_frame']").length > 0){
                
                // On Print
                if( jQuery('#pa_print_type input[type="radio"]:checked').val() == "Photo Paper"){
                    jQuery('.select_wraper li').find("a[tab='pa_paper_type']").parent().show();
                    jQuery('.select_wraper li').find("a[tab='pa_frame']").parent().show();
                    jQuery('.select_wraper li').find("a[tab='pa_mat']").parent().show();
                    
                    jQuery('.select_wraper li').find("a[tab='pa_wrap']").parent().hide();
                    jQuery('#pa_wrap input[value="Roll"]').prop("checked", true); 
                }else if( jQuery('#pa_print_type input[type="radio"]:checked').val() == "Canvas"){
                // On Canvas
                    jQuery('.select_wraper li').find("a[tab='pa_paper_type']").parent().show();
                    jQuery('.select_wraper li').find("a[tab='pa_frame']").parent().show();
                    jQuery('.select_wraper li').find("a[tab='pa_wrap']").parent().show();
                    
                    jQuery('.select_wraper li').find("a[tab='pa_mat']").parent().hide();
                    jQuery('#pa_mat input[value="None"]').prop("checked", true);
                }else if( jQuery('#pa_print_type input[type="radio"]:checked').val() == "MDF"){
                // On Canvas
                    jQuery('.select_wraper li').find("a[tab='pa_paper_type']").parent().hide();
                    jQuery('.select_wraper li').find("a[tab='pa_frame']").parent().show();
                    jQuery('.select_wraper li').find("a[tab='pa_wrap']").parent().hide();
                    
                    jQuery('.select_wraper li').find("a[tab='pa_mat']").parent().hide();
                    jQuery('#pa_mat input[value="None"]').prop("checked", true);
                    jQuery('#pa_wrap input[value="Roll"]').prop("checked", true);
                }else{
                // On Other
                    jQuery('.select_wraper li').find("a[tab='pa_paper_type']").parent().hide();
                    jQuery('#pa_paper_type input[value="Glossy"]').prop("checked", true);
                    jQuery('.select_wraper li').find("a[tab='pa_frame']").parent().hide();
                    jQuery('#pa_frame input[value="None"]').prop("checked", true);
                    jQuery('.select_wraper li').find("a[tab='pa_mat']").parent().hide();
                    jQuery('#pa_mat input[value="None"]').prop("checked", true);
                    jQuery('.select_wraper li').find("a[tab='pa_wrap']").parent().hide();
                    jQuery('#pa_wrap input[value="Roll"]').prop("checked", true);    
                }
            }
            
            // Mini Canvas Options Selection
            if(jQuery('.select_wraper li').find("a[tab='pa_wrap']").length > 0){
               
            }
        }
        show_color_select();
        
        // Update function
        function update_pval(){
            
            // Display Loader
            jQuery('.product_infos .price .amount').text('');
            jQuery('#price_loader').show();

            

                // Get All Selected Params
                var printType = jQuery('#pa_print_type');
                window.params = {};
                jQuery('#select_area .radio_opts').each(function(){
                    var name = jQuery(this).attr("id");
                    var selected = parseInt(jQuery(this).find('input[type="radio"]:checked').attr('data-id'));
                    window.params[name] = selected;
                      
                });
                  
            
                // Change Visual Frame Depends On Type
                var printTypeVal = window.params['pa_print_type'];
            
                    // Reset styles
                                jQuery('.product_images .owl-item').removeClass('frame_style_1');
                                jQuery('.product_images .owl-item').removeClass('frame_style_2');
                                jQuery('.product_images .owl-item').removeClass('frame_style_3');
                                jQuery('.product_images .owl-item').removeClass('frame_style_4');
                                jQuery('.product_images .owl-item').removeClass('frame_style_5');
                                jQuery('.product_images .owl-item').removeClass('frame_style_6');
                                jQuery('.product_images .owl-item').removeClass('frame_style_7');
                                jQuery('.product_images .owl-item').removeClass('frame_style_8');
                                jQuery('.product_images .owl-item').removeClass('frame_style_9');
                                jQuery('.product_images .owl-item').removeClass('frame_style_10');
                                jQuery('.product_images .owl-item').removeClass('frame_style_11');
                                jQuery('.product_images .owl-item').removeClass('mini_canvas');
            
                                jQuery('.product_images .owl-item').css('border', 'none');
                                jQuery('.product_images .owl-wrapper').css('border', 'none');
                                jQuery('.product_images .attachment-shop_single').css('border', 'none !important');
                                jQuery('.type-product .owl-wrapper-outer').css('box-shadow', 'none');
            
                    // Change Canvas
                    if(printType.length > 0 && (printTypeVal == 63 || printTypeVal == 64 || printTypeVal == 67)){
                        var picS = parseInt(jQuery('#pa_size input[type="radio"]:checked').val());
                            // Case Creat
                            <?php if( $product->id == 620){ ?> 
                            var picS = parseInt(jQuery('#pa_size input[type="range"]').val());
                            <?php } ?>
                        
                        var frameCheck = jQuery('#pa_frame');
                      
                            // Frame Change
                            if(frameCheck.length > 0){
                                var frameS = jQuery('#pa_frame input[type="radio"]:checked').val();
                                var frameC = jQuery('#pa_frame_color input[type="radio"]:checked').val();
                                var frameStyle = jQuery('#pa_frame_style input[type="radio"]:checked').val();
                                
                                if(frameC == 'Dark Brown'){ frameC = '#4C2B00'; }
                                if(frameC == 'Honey Brown'){ frameC = '#FBA32F'; }
 
                                // Add new styles
                                if(frameS == "Color"){
                                    jQuery('.product_images .owl-item').css('border', '20px solid '+frameC);  
                                    jQuery('.product_images .owl-wrapper').css('border', '1px outset rgb(178, 178, 178)');
                                }else if(frameS == "Classic"){
                                    if(frameStyle == "Style 01"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_1');
                                    }
                                    else if(frameStyle == "Style 02"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_2');
                                    }
                                    else if(frameStyle == "Style 03"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_3');
                                    }
                                    else if(frameStyle == "Style 04"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_4');
                                    }
                                    else if(frameStyle == "Style 05"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_5');
                                    }
                                    else if(frameStyle == "Style 06"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_6');
                                    }
                                    else if(frameStyle == "Style 07"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_7');
                                    }
                                    else if(frameStyle == "Style 08"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_8');
                                    }
                                    else if(frameStyle == "Style 09"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_9');
                                    }
                                    else if(frameStyle == "Style 10"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_10');
                                    }
                                    else if(frameStyle == "Style 11"){
                                        jQuery('.product_images .owl-item').addClass('frame_style_11');
                                    }
                                }
                            }

                            // Prints Only - Mat Change
                            if(printTypeVal == 63){
                                var matCheck = jQuery('#pa_mat');
                                if(matCheck.length > 0 && matCheck != "None"){

                                    var matS = jQuery('#pa_mat input[type="radio"]:checked').val();
                                    var matC = jQuery('#pa_mat_color input[type="radio"]:checked').val();
                                    var px = (matS[0]*10+5 - picS);
                                  
                                    if(matS != "None"){

                                        jQuery('.product_images .attachment-shop_single').css({'border': px+'px solid '+matC ,  'padding': '1px','background': 'rgb(136, 136, 136)'});  
                                        jQuery('.product_images .easyzoom').css('border', '1px inset rgb(178, 178, 178)' );
                                    }else{
                                         jQuery('.product_images .attachment-shop_single').css({'border': 'none' ,  'padding': '0px','background': 'none'});
                                    }
                                }
                            }
                            
                            // Canvas Only - Clear Pins
                            if(printTypeVal == 64){
                                if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                            }
                    }
            
                    // Acrylic Visual Effects
                    else if(printType.length > 0 && printTypeVal == 65){
                        jQuery('.product_images .attachment-shop_single').css({'border-right': 'none','border-                                 bottom': 'none', 'border': '0px solid grey)' ,  'padding': '0px','background': 'rgba(243, 253, 253, 0.5)'});  
                        jQuery('.product_images .easyzoom').css('border', '0 px outset rgba(250, 255, 255,0.2)' );
                        jQuery('.type-product .owl-wrapper-outer').css('box-shadow', 'rgba(0, 0, 0, 0.75) 5px 5px 10px');
                        // Change Hanging
                        if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                        if(window.params['pa_hanging'] == 49 ){
                            jQuery('.product_images .attachment-shop_single').after('<div id="pins-on"><div class="pin_n1"></div>                                      <div class="pin_n2"></div></div>');
                        }else if(window.params['pa_hanging'] == 69 ){
                            jQuery('.product_images .attachment-shop_single').after('<div id="pins-on"><div class="pin_n1"></div>                                      <div class="pin_n2"></div><div class="pin_n3"></div><div class="pin_n4"></div></div>');
                        }else{
                        
                        } 
                    }
            
                    // Metal Visual Effects
                    else if(printType.length > 0 && printTypeVal == 66){
                        jQuery('.product_images .attachment-shop_single').css({'border': 'none','border-right': '5px solid rgb(105, 105,105)','border-bottom': '5px solid rgb(105, 105,105)',padding:'none','background':'rgb(105, 105, 105)'});  
                        jQuery('.product_images .easyzoom').css('border','1px inset rgb(145, 145, 145)');
                         if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                    }
            
                    // Canvas Visual Effects
                    /** Disabled **
                    else if(printType.length > 0 && printTypeVal == 64){
                        jQuery('.product_images .attachment-shop_single').css({'border-right': 'none','border-                                 bottom': 'none', 'border': '10px solid rgb(224, 223, 153)' ,  'padding':'2px','background': 'rgb(255, 255, 255)','box-shadow': '0px 0px 1px 2px rgb(254, 255, 227)'});  
                        jQuery('.product_images .easyzoom').css('border','1px inset rgb(145, 145, 145)');
                         if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                    }*/
                    // Mini-Canvas Visual Effects
                    else if(printType.length > 0 && printTypeVal == 250){
                        jQuery('.product_images .attachment-shop_single').css({'width':'50%','margin':'0 auto','border-right': 'none','border-                                 bottom': 'none', 'border': '0px solid rgb(224, 223, 153)' ,  'padding':'0px','background': 'rgb(255, 255, 255)'});  
                        jQuery('.product_images .easyzoom').css('border','0px inset rgb(145, 145, 145)');
                           if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                        jQuery('.product_images .owl-item').addClass('mini_canvas');
                        
                    }
                    // Matt Visual Effects
                    else if(printType.length > 0 && printTypeVal == 248){
                        jQuery('.product_images .attachment-shop_single').css({'border': 'none','border-right': '5px solid rgb(105, 105,105)','border-bottom': '5px solid rgb(105, 105,105)',padding:'none','background':'rgb(105, 105, 105)'});  
                        jQuery('.product_images .easyzoom').css('border','1px inset rgb(145, 145, 145)');
                         if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                    }
            
                    // MDF Visual Effects
                    else if(printType.length > 0 && printTypeVal == 67){
                        jQuery('.product_images .attachment-shop_single').css({'border-right': 'none','border-bottom': 'none', 'border': '8px double rgba(0, 0, 0, 0.82)' ,  'padding':'0px','background': 'rgb(62, 62, 62)','box-shadow': '0px 0px 1px 2px rgb(254, 255, 227)'});  
                        jQuery('.product_images .easyzoom').css('border','1px inset rgb(145, 145, 145)');
                         if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                    }
            
                    // Print Visual Effects
                    else{
                        jQuery('.product_images .attachment-shop_single').css({'border': 'none','border-right': 'none','border-bottom': 'none' , 'background': 'none', padding:'none'});  
                        jQuery('.product_images .easyzoom').css('border', 'none');
                         if(jQuery('#pins-on').length > 0){jQuery('#pins-on').remove()};
                    }
            
                    //Reset Frame
                    if(printType.length > 0 && printTypeVal != 64){
                        jQuery('.product_images .attachment-shop_single').css({'box-shadow': 'none'}); }
                    if(printType.length > 0 && printTypeVal != 63){
                        jQuery('.product_images .owl-item').css('border', 'none'); }
                    if(printType.length > 0 && printTypeVal != 250){
                        jQuery('.product_images .attachment-shop_single').css({'width':'100%'}); }
                    
            
                    var picS = parseInt(jQuery('#pa_size input[type="radio"]:checked').val());
                            // Case Create
                            <?php if( $product->id == 620){ ?> 
                            var picS = parseInt(jQuery('#pa_size input[type="range"]').val());
                            <?php } ?>
            
            if(picS > 0){
                changeSize(picS*40);
            }
            
                // Ajax Call 
                var data = {
                  "action": "getPrice",
                  "params" : window.params,
                  "product" : <?php echo $product->id; ?>
                };
                 
                data = jQuery(this).serialize() + "&" + jQuery.param(data);
                        jQuery.ajax({
                  type: "POST",
                  dataType: "json",
                  url: site_url+"/wp-content/themes/mrtailor/woocommerce/pricing.php",
                  data: data,
                  success: function(data) {
                        
                        var result = jQuery.parseJSON(data["json"]);
                        console.log(result);
                    
                        // On Success Change Price
                        jQuery('meta[itemprop="price"]').attr('content',result.Converted_Price);
                        jQuery('.product_infos .price .amount').html(result.Converted_Price);
                        jQuery('#price_loader').hide();
                        toggle_buttons('on');
                        window.ajaxstatus = true;
                        jQuery('#pa_frame_color').css({'opacity': '1'});
                  }
                });
            
        }
        
        // Call to update on change
        jQuery('#select_area .radio_opts').each(function(){
                //cval = cval + parseInt(jQuery(this).find(":selected").attr('data-price-value'));
            jQuery(this).change(function(){
                toggle_buttons('off');
                show_color_select();
                if(window.ajaxstatus == true){
                    window.ajaxstatus = false;
                    setTimeout(function(){ update_pval(); }, 300);
                }
            });
        });
    
       jQuery('.color_picker').each(function(){
                        var color_picked = jQuery(this);
                       var prev =  jQuery(this).prev();
                        color_picked.click(function(){
                            if(window.ajaxstatus == true){
                                console.log(prev);
                                prev.prop('checked', true);
                                jQuery('.color_picker').css({'border':'1px solid #B9B9B9'});
                                color_picked.css({'border':'2px solid red'});
                                jQuery('#pa_frame_color').css({'opacity': '0.2'});
                                toggle_buttons('off');
                                show_color_select();

                                window.ajaxstatus = false;
                                setTimeout(function(){ update_pval();  }, 300);
                            }
                        });
                    });
        
        setTimeout(function(){ update_pval(); }, 300);
        
        // Tooltip script
          jQuery("span.question").each(function(){
              jQuery(this).click(function() {
                        if(jQuery(this).parent().find('.tooltip').length > 0){
                            jQuery("div.tooltip").remove(); 
                        }else{
                     jQuery("div.tooltip").remove(); 
                     var help_text = jQuery(this).parent().find('input.help_text').val();
                                    var help_url = jQuery(this).parent().find('input.help_url').val();
                                    jQuery(this).append('<div class="tooltip"><p>'+help_text+'</p><a style="color: white;text-decoration: underline;" href="'+help_url+'">Read More</a></div>');
                     
                  
                  
                        }
                }); 
          });

    
    
    });
</script>
<?php $res_w = get_field(picture_res_w,$product->id);
$res_h = get_field(picture_res_h,$product->id);
if ($res_w && $res_h){
    echo '<p>Resolution: '.$res_w.'x'.$res_h.'</p>';
}
}
// Case original
else
{ ?>
    <script> 
            $(document).ready(function() {
                var picS = <?php echo explode(" ",$product->get_dimensions())[0]; ?> ;
                changeSize(picS*40);
            });
    </script>
<?php }
echo $linked_div;?>

<br>



<script type="text/javascript" src="../../html5lightbox/html5lightbox.js"></script>

                    
                    <div id="mydiv" style="display:none;">
  <div class="lightboxcontainer room">
	<div class="buttonHolder">
  <a href="#" class="mbutton tick" onclick='changeRoom(1)'></a>
  <a href="#" class="mbutton cross" onclick='changeRoom(2)'></a>
  <a href="#" class="mbutton heart" onclick='changeRoom(3)'></a>
  <a href="#" class="mbutton flower" onclick='changeRoom(4)'></a>
  <a href="#" class="mbutton tick2" onclick='changeRoom(5)'></a>
  <a href="#" class="mbutton cross2" onclick='changeRoom(6)'></a>
  <a href="#" class="mbutton heart2" onclick='changeRoom(7)'></a>
  <a href="#" class="mbutton flower2" onclick='changeRoom(8)'></a>
     
</div>
	<div class="lightbox"><div class='dragable' style="display:inline-block; z-index:200;">
	<?php
$feat_image = get_the_post_thumbnail( $post->ID, 'full' );
echo $feat_image;
?>
   </div>
        
<script type="text/javascript">

$(document).ready(function() {
      $('.dragable').draggable();
   
        
});
    
     function changeRoom(room) {
         
        var background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg";
        if(room == 1){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg";
        }else if(room == 2){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-2.jpg";
        }else if(room == 3){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-3.jpg";
        }else if(room == 4){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg";
        }else if(room == 5){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg";
        }else if(room == 6){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-2.jpg";
        }else if(room == 7){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-3.jpg";
        }else if(room == 8){
            background = "http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg";
        }
        
        jQuery('.room').css("background-image","url("+background+")");
        
    }
    function changeSize(size) {

        jQuery('.dragable img').css({
            width: size
        });
        var new_span = size/40 + ' Inch';
        
        jQuery('#size_txt_span span').text(new_span);
      
        console.log(size);
    }
    function changeISize(size) {

        
        var new_span = size + ' Inch';
        
        jQuery('#isize_txt_span span').text(new_span);
      
        console.log(size);
    }
   
    
</script>
      </div><span id='size_txt_span'><span>7.5 Inch</span></span><input id='range1' type="range" min="30" max="450" value="245" step="5" oninput="changeSize(this.value)" onchange="changeSize(this.value)"  style="z-index:999;"/></div>
	</div>
	<div style="clear:both;"></div>


                    
                    <a href="#mydiv" class="html5lightbox" data-width=1000 data-height=700 title="Inline Div">Virtual Room</a>
                    <style type="text/css">
div#html5-watermark {
    display: none !important;
}
                        div#html5-text {
    display: none !important;
}
                        a.html5lightbox {
    text-align: center;
    display: block;
}
.room{
    background-image:url('http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg');
}
span#size_txt_span {
    position: absolute;
    bottom: 40px;
    left: 35%;
    width: 30%;
    color: #81FF00;
    text-shadow: 1px 1px 1px black;
}
.lightboxcontainer {
  width:100%;
  height: 100%;
    background-size: cover;
}

.lightbox {
    width: 600px;
    position: absolute;
    left: 19%;
    height: 0;
    top: 95px;
}
.lightbox img{
    width: 300px;
    cursor: move;
}
 input#range1 {
    position: absolute;
    bottom: 20px;
    left: 35%;
    width: 30%;
}                     
.lightbox iframe {
  min-height: 390px;
}
.divtext {
  margin: 36px;
}
                        .mydiv{
  		background-image: -webkit-linear-gradient(top, #edecec, #cecbc9);
			background-image: linear-gradient(top, #edecec, #cecbc9);
}

.buttonHolder{
      width: 800px;
    position: relative;
    display: inline-block;
}


		.mbutton{
			background-image: -webkit-linear-gradient(top, #f4f1ee, #fff);
			background-image: linear-gradient(top, #f4f1ee, #fff);
            background-size: cover;
			border-radius: 5px;
			box-shadow: 0px 4px 10px 0px rgba(0, 0, 0, .3), inset 0px 2px 1px 1px white, inset 0px -3px 1px 1px rgba(204,198,197,.5);
			float:left;
			height: 70px;
			margin: 0 30px 30px 0;
			position: relative;
			width: 70px;			
			-webkit-transition: all .1s linear;
			transition: all .1s linear;
		}

		.mbutton:after{
			color:#e9e6e4;
			content: "";
			display: block;
			font-size: 30px;
			height: 30px;
			text-decoration: none;
			text-shadow: 0px -1px 1px #bdb5b4, 1px 1px 1px white;
			position: absolute;
			width: 30px;
			left:24px;
			top:19px;
		}

        
        .tick{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg");
        }.cross{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-2.jpg");
        }.heart{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-3.jpg");
        }.flower{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg");
        } .tick2{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg");
        }.cross2{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-2.jpg");
        }.heart2{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-3.jpg");
        }.flower2{
			background-image: url("http://www.firstartgallery.com.au/image/data/FirstArtGallery-Virtual-Room-1.jpg");
        }
        

		

		.mbutton:hover{
			color:#0088cc;
		}

		.heart:hover:after,.heart2:hover:after{
			color:#f94e66;
			text-shadow:0px 0px 6px #f94e66;
		}

		.flower:hover:after,.flower2:hover:after{
			color:#f99e4e;
			text-shadow:0px 0px 6px #f99e4e;
		}

		.tick:hover:after,.tick2:hover:after{
			color:#83d244;
			text-shadow:0px 0px 6px #83d244;
		}

		.cross:hover:after,.cross2:hover:after{
			color:#eb2f2f;
			text-shadow:0px 0px 6px #eb2f2f;
		}

		

		.mbutton:active{
			box-shadow: 0 3px 5px 0 rgba(0,0,0,.4), inset 0px -3px 1px 1px rgba(204,198,197,.5);
		}

		.mbutton:active:after{
			color:#dbd2d2;
			text-shadow: 0px -1px 1px #bdb5b4, 0px 1px 1px white;
		}
@media (max-width: 800px) {
  .lightbox {
    width: 100%;
  }
  .divtext {
    margin: 12px;
  }
}
</style>

