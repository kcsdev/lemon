<?php
require_once("../../../../wp-load.php");

// Reset Session
session_start(); 

if (is_ajax()) {
  if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
    $action = $_POST["action"];
    switch($action) { //Switch case for value of action
      case "getPrice": getPrice(); break;
    }
  }
}

//Function to check if the request is an AJAX request
function is_ajax() {
  return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
}

// Function to get the attribute's price value
function getAttrPriceVal($aName,$term_id){ 
    $pval = get_field(  'price-value' , $aName.'_'.$term_id);
    return $pval;
}

// Function to get the attribute's size value
function getAttrSizeVal($aName,$term_id){
    
    $sval = get_field(  'size-value' , $aName.'_'.$term_id);
    return $sval;
}

// Function to get the return total price value
function getPrice(){
    
// Get Values from the settings page
$prespex = get_option('prespex');
$kappa = get_option('kappa');
$package1 = get_option('package1');
$package2 = get_option('package2');
$package3 = get_option('package3');
$profit1 = get_option('profit1');
$profit2 = get_option('profit2');
$clearing = get_option('clearing');
$shipping1 = get_option('shipping1');
$shipping2 = get_option('shipping2');
$shipping3 = get_option('shipping3');

$backend_values = array('prespex'=>$prespex,'kappa'=>$kappa,'package1'=>$package1,'package2'=>$package2,'package3'=>$package3,'profit1'=>$profit1,'profit2'=>$profit2,'clearing'=>$clearing,'shipping1'=>$shipping1,'shipping2'=>$shipping2,'shipping3'=>$shipping3);
    
  // Get parameters from Post
  $selectedParams = $_POST['params'];
  $selectedParamsVals = array();
  $product_id = $_POST['product'];
  
    
  // Get product regular price
  $basePrice = $price = get_post_meta( $product_id, '_regular_price', true); ;//get price from id
    
  // Declare all needed variables
  $pval_type = 0;
  $pic_size_h = 0;
  $pic_size_l = 0;
  $pic_size = 0;
  $pval_frame = 0;
  $pval_frame_type = 0;
  $pasparto_size = 0; 
  $pval_pasparto = 0; 
  $pval_wrap = 0;
  $pval_hanging = 0;
  $extra = 0;
  $additional= 0;
  $debug = 0;
    
   
 // Foreach attribute - add the extra price
  foreach($selectedParams as $aName => $term_id){
      if( $aName == 'pa_print_type'){
          $pval_type = (float)getAttrPriceVal($aName,$term_id);
      }elseif($aName == 'pa_size'){
          $slug = explode('-x-',get_term_by('id', $term_id, $aName)->slug);
          $pic_size_h = (float)str_replace('-','.',$slug[0]);
          $pic_size_l = (float)str_replace('-','.',$slug[1]);
          $pic_size = $pic_size_h*$pic_size_l;
      }elseif($aName == 'pa_frame'){
          $pval_frame = (float)getAttrPriceVal($aName,$term_id);
      }elseif($aName == 'pa_frame_type'){
          $pval_frame_type = (float)getAttrPriceVal($aName,$term_id);
      }elseif($aName == 'pa_mat'){
          $pval_pasparto = (float)getAttrPriceVal($aName,$term_id);
          $pasparto_size = (float)getAttrSizeVal($aName,$term_id);
      }elseif($aName == 'pa_wrap'){
          $pval_wrap = (float)getAttrPriceVal($aName,$term_id);
      }elseif($aName == 'pa_hanging'){
          $pval_hanging = (float)getAttrPriceVal($aName,$term_id);
      }
      
        // Defaul extra calculation - not in use
        $sval = (float)getAttrSizeVal($aName,$term_id);
        $pval = (float)getAttrPriceVal($aName,$term_id);
        $extra += $sval*$pval;
  }
        

    // Get print type
    $print_type = $selectedParams['pa_print_type'];
    
    // Split calculation depends on the type
    if($print_type == "63"){ // Photo Paper
        if($pval_frame ==0 && $pval_pasparto == 0) { $additional = $package1+$shipping1; }
            elseif ($pval_frame ==0 && $pval_pasparto != 0) { $additional = $package2+$shipping2; }
                else { $additional = $package3+$shipping3; }
        $extra = ($basePrice*(($profit1+100)/100) + (($pval_type*$pic_size + (2*$pasparto_size+$pic_size_h)*(2*$pasparto_size+$pic_size_l)*$pval_pasparto)  +  (((2*$pasparto_size+$pic_size_h)+((2*$pasparto_size+$pic_size_l)))*2*$pval_frame) + ((2*$pasparto_size+$pic_size_h)*(2*$pasparto_size+$pic_size_l)*($prespex+$kappa))) *(($profit2+100)/100) + $additional ) *(($clearing+100)/100);
        $debug = ($basePrice*(($profit1+100)/100)) ." + ".($pval_type*$pic_size + (2*$pasparto_size+$pic_size_h)*(2*$pasparto_size+$pic_size_l)*$pval_pasparto)." + ". (((2*$pasparto_size+$pic_size_h)+((2*$pasparto_size+$pic_size_l)))*2*$pval_frame) . " + " . ((2*$pasparto_size+$pic_size_h)*(2*$pasparto_size+$pic_size_l)*($prespex+$kappa)) . " * " . (($profit2+100)/100) . " + " . $additional . " * " . (($clearing+100)/100);
   
    }else if($print_type == "65"){ // Acrilc
 $extra = ($basePrice*(($profit1+100)/100) + (($pval_type*$pic_size + $pval_hanging)*(($profit2+100)/100))  + $package3+($shipping3*($pic_size+100)/100)) *(($clearing+100)/100);
    
    }else if($print_type == "249"){ // Acrilc Block
$extra = ($basePrice*(($profit1+100)/100) + (($pval_type*$pic_size)*(($profit2+100)/100)) + $package3 + $shipping1) *(($clearing+100)/100);
   
    }else if($print_type == "64"){ // Canvas
        if($pval_frame_type ==0 && $pval_wrap == 0) { $additional = $package1+$shipping1; }
            else { $additional = $package2+$shipping2; }
$extra = ($basePrice*(($profit1+100)/100) + (($pval_type + $pval_warp)*$pic_size + ((2*$pic_size_h)+((2*$pic_size_l))*$pval_frame) + ((2*$pic_size_h)*((2*$pic_size_l))*($prespex)) )*(($profit2+100)/100) + $additional) * (($clearing+100)/100);
  
    }else if($print_type == "67"){ // MDF
$extra = ($basePrice*(($profit1+100)/100)+ (($pval_type*$pic_size)*(($profit2+100)/100)) + $package3+($shipping3*($pic_size+100)/100)) *(($clearing+100)/100);
  
    }else if($print_type == "250"){ // MINICanvas
$extra = ($basePrice*(($profit1+100)/100) + (($pval_type*$pic_size)*(($profit2+100)/100)) + $package3 + $shipping1) *(($clearing+100)/100);
  
    }else{
        return('Error, print type illegal');
        die();
    }
    
  $New_Att = array('pval_type'=>$pval_type,'pic_size_h'=>$pic_size_h,'pic_size_l'=>$pic_size_l,'pval_frame'=>$pval_frame,'pval_frame_type'=>$pval_frame_type,'pasparto_size'=>$pasparto_size,'pval_pasparto'=>$pval_pasparto,'pval_wrap'=>$pval_wrap,'pval_hanging'=>$pval_hanging,'extra'=>$extra,'additional' => $additional); 

    
  $endPrice = $basePrice + $extra; // + $selectedParams
                           
  $convertedPrice = _cc($endPrice);
  $return = array("ID"=>$product_id,"Base_Price"=>$basePrice,"End_Price"=>$endPrice,"Extra"=>$extra,"Attributes"=>$selectedParams,"Converted_Price"=>$convertedPrice,"Backend_Values"=>$backend_values,"After_Split"=>$New_Att,$debug);
    
  // Set Sessions
  $_SESSION['custom_price'] = $extra;
  $_SESSION['custom_attributes'][$product_id] = $return;
   
  // Return the information
  $return["json"] = json_encode($return);
  echo json_encode($return);
}

?>