<?php
/*
Template Name: Page with Slider
*/
?>

<?php get_header(); ?>

<div class="page_with_slider">

	<?php
        global $slider_metabox;
        $slider_metabox->the_meta();
    ?>
    
    <?php
    
	$slider_style = $slider_metabox->get_the_value('slider_template');
	switch ($slider_style) {
		case "style_1":
			include_once('templates/slider/style_1.php');
			break;
		case "style_2":
			include_once('templates/slider/style_2.php');
			break;
		case "style_3":
			include_once('templates/slider/style_3.php');
			break;
		case "style_4":
			include_once('templates/slider/style_4.php');
			break;
		case "style_5":
			include_once('templates/slider/style_5.php');
			break;
		case "style_6":
			include_once('templates/slider/style_6.php');
			break;
		default:
			include_once('templates/slider/style_1.php');
	}
	
	?>
    
    <?php if ($post->post_content != "") : ?>
    
    <div id="primary" class="content-area">
       
        <div id="content" class="site-content" role="main">

            <?php while ( have_posts() ) : the_post(); ?>

            	<?php get_template_part( 'content', 'page' ); ?>

            <?php endwhile; // end of the loop. ?>
            
            <div class="row">        
                    <div class="wpb_wrapper">
                     
                        <a href='<?php echo get_site_url().'/multi_vendor/'; ?>' title='View all Artists'><h2 style="text-align: center;">Artists</h2></a>

                    </div>
                        <div id="content" class="site-content" role="main">
                            <?php
                            $args = array( 'hide_empty=0' );

                            $terms = get_terms( 'multi_vendor', $args );
                            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
                                $count = count( $terms ); 
                                $i = 0;
                                $term_list = '<div class="artist_box">';
                                foreach ( $terms as $term ) {
                                    //var_dump($term);
                                    $i++;
                                    $country = unserialize(get_country($term->term_id))[country_name];
                                    if($i < 7){
                                        $image = '';
                                        $thumbnail_id = absint(get_woocommerce_term_meta($term->term_id, 'thumbnail_id', true));

                                        if ($thumbnail_id)
                                            $image = wp_get_attachment_url($thumbnail_id);
                                        else
                                            $image = get_site_url().'/wp-content/uploads/2014/03/blog_quote-author.jpg';                                    

                                        if ($image != '') {
                                            $getimagefromurl = "<img src=" . $image . " >";
                                        } else {
                                            $getimagefromurl = '';
                                        }
                                        $img = '<div class="artist_img">' . $getimagefromurl . $description . '<p class="artist_country">'. $country . '</p></div>';

                                        $term_list .= '<a style="margin-right:10px; margin-left:10px; display:inline-block" href="' . get_term_link( $term ) . '" title="' . sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) . '">'.$img.'<p class="artist_name">' . $term->name . '</p><span class="artist_desc">' /*. $term->description */. '</span></a>';

                                        if ( $count != $i ) {
                                            $term_list .= '';
                                        }
                                        else {
                                            $term_list .= '</div>';
                                        }
                                    }
                                }
                                echo $term_list;
                            }  ?>

                        </div>
                    </div>

        </div><!-- #content -->           
        
    </div><!-- #primary -->
    
    <?php endif; ?>
    
</div>
    
<?php get_footer(); ?>
