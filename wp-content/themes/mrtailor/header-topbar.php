<div id="site-top-bar">
                            
    <div class="row">
        
        <div class="large-6 columns">
        	<?php if ( is_user_logged_in() ) { ?>
                    <span class='hello_usr'>Hello, <?php echo wp_get_current_user()->display_name; ?></span> <?php } ?>
					<?php /* <span>Your ip: <?php echo do_shortcode("[show_ip]");?></span> */ ?>
			<div class="language-and-currency">
				
				<?php if (function_exists('icl_get_languages')) { ?>
				
					<?php $additional_languages = icl_get_languages('skip_missing=N&orderby=KEY&order=DIR&link_empty_to=str'); ?>
					
					<select class="topbar-language-switcher">
						<option><?php echo ICL_LANGUAGE_NAME; ?></option>
						<?php
								
						if (count($additional_languages) > 1) {
							foreach($additional_languages as $additional_language){
							  if(!$additional_language['active']) $langs[] = '<option value="'.$additional_language['url'].'">'.$additional_language['native_name'].'</option>';
							}
							echo join(', ', $langs);
						}
						
						?>
					</select>
				
				<?php } ?>
				
				<?php if (class_exists('woocommerce_wpml')) { ?>
					<?php echo(do_shortcode('[currency_switcher]')); ?>
				<?php } ?>
				
			</div><!--.language-and-currency-->
            
            <div class="site-top-message22"><?php if ( isset($mr_tailor_theme_options['top_bar_text']) ) _e( $mr_tailor_theme_options['top_bar_text'], 'mr_tailor' ); ?></div> 
                           
        </div><!-- .large-6 .columns -->
        
        <div class="large-6 columns">
            
            <?php /* <div class="site-social-icons-wrapper">
                <div class="site-social-icons">
                    <ul class="//animated //flipY">
                        <?php if ( (isset($mr_tailor_theme_options['facebook_link'])) && (trim($mr_tailor_theme_options['facebook_link']) != "" ) ) { ?><li class="site-social-icons-facebook"><a target="_blank" href="<?php echo $mr_tailor_theme_options['facebook_link']; ?>"><i class="fa fa-facebook"></i><span>Facebook</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['twitter_link'])) && (trim($mr_tailor_theme_options['twitter_link']) != "" ) ) { ?><li class="site-social-icons-twitter"><a target="_blank" href="<?php echo $mr_tailor_theme_options['twitter_link']; ?>"><i class="fa fa-twitter"></i><span>Twitter</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['pinterest_link'])) && (trim($mr_tailor_theme_options['pinterest_link']) != "" ) ) { ?><li class="site-social-icons-pinterest"><a target="_blank" href="<?php echo $mr_tailor_theme_options['pinterest_link']; ?>"><i class="fa fa-pinterest"></i><span>Pinterest</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['linkedin_link'])) && (trim($mr_tailor_theme_options['linkedin_link']) != "" ) ) { ?><li class="site-social-icons-linkedin"><a target="_blank" href="<?php echo $mr_tailor_theme_options['linkedin_link']; ?>"><i class="fa fa-linkedin"></i><span>LinkedIn</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['googleplus_link'])) && (trim($mr_tailor_theme_options['googleplus_link']) != "" ) ) { ?><li class="site-social-icons-googleplus"><a target="_blank" href="<?php echo $mr_tailor_theme_options['googleplus_link']; ?>"><i class="fa fa-google-plus"></i><span>Google+</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['rss_link'])) && (trim($mr_tailor_theme_options['rss_link']) != "" ) ) { ?><li class="site-social-icons-rss"><a target="_blank" href="<?php echo $mr_tailor_theme_options['rss_link']; ?>"><i class="fa fa-rss"></i><span>RSS</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['tumblr_link'])) && (trim($mr_tailor_theme_options['tumblr_link']) != "" ) ) { ?><li class="site-social-icons-tumblr"><a target="_blank" href="<?php echo $mr_tailor_theme_options['tumblr_link']; ?>"><i class="fa fa-tumblr"></i><span>Tumblr</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['instagram_link'])) && (trim($mr_tailor_theme_options['instagram_link']) != "" ) ) { ?><li class="site-social-icons-instagram"><a target="_blank" href="<?php echo $mr_tailor_theme_options['instagram_link']; ?>"><i class="fa fa-instagram"></i><span>Instagram</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['youtube_link'])) && (trim($mr_tailor_theme_options['youtube_link']) != "" ) ) { ?><li class="site-social-icons-youtube"><a target="_blank" href="<?php echo $mr_tailor_theme_options['youtube_link']; ?>"><i class="fa fa-youtube-play"></i><span>Youtube</span></a></li><?php } ?>
                        <?php if ( (isset($mr_tailor_theme_options['vimeo_link'])) && (trim($mr_tailor_theme_options['vimeo_link']) != "" ) ) { ?><li class="site-social-icons-vimeo"><a target="_blank" href="<?php echo $mr_tailor_theme_options['vimeo_link']; ?>"><i class="fa fa-vimeo-square"></i><span>Vimeo</span></a></li><?php } ?>
                    </ul>
                </div>
            </div> */ ?>
            
            <nav id="site-navigation-top-bar" class="main-navigation" role="navigation">                    
				<?php /*
                    wp_nav_menu(array(
                        'theme_location'  => 'top-bar-navigation',
                        'fallback_cb'     => false,
                        'container'       => false,
                        'items_wrap'      => '<ul id="%1$s">%3$s</ul>',
                    ));
                */ ?>
                <ul>  <li style='white-space: initial;'><?php echo do_shortcode("[woocs show_flags=1 width='300px' flag_position='right' txt_type='desc']"); ?></li>
                            <li class="shopping-bag-button" class="right-off-canvas-toggle"><a><i class="getbowtied-icon-shop"></i><span class="shopping_bag_items_number">&nbsp;<?php //echo $woocommerce->cart->cart_contents_count; ?></span></a></li>

                    <li><select class="topbar-size-switcher">
                        <option <?php if($_SESSION['dim'] == 'INCH'){echo 'selected';} ?> >Inch</option>
                        <option <?php if($_SESSION['dim'] == 'CM'  ){echo 'selected';} ?> >Cm</option>
                    </select></li>
                    
                    <li><select class="topbar-weight-switcher">
                        <option <?php if($_SESSION['weight'] == 'KG'  ){echo 'selected';} ?> >Kg</option>
						<option <?php if($_SESSION['weight'] == 'POUNDS'){echo 'selected';} ?> >Pounds</option>
                    </select></li>
                   
                     <?php /* <li><select class="topbar-curr-switcher">
                        <option <?php if($_SESSION['curr'] == 'USD'  ){echo 'selected';} ?> >USD</option>
						<option <?php if($_SESSION['curr'] == 'EUR'){echo 'selected';} ?> >EUR</option>
                        <option <?php if($_SESSION['curr'] == 'GBP'){echo 'selected';} ?> >GBP</option>
                    </select></li> */ ?>

                    
                     <li><a href="<?php echo get_site_url(); ?>/about" class="logout_link"><?php _e('About us', 'mr_tailor'); ?></a></li>    
                     <li><a href="<?php echo get_site_url(); ?>/blog"  class="blog_link"><?php _e('Blog', 'mr_tailor'); ?></a></li>
                
                <?php if ( is_user_logged_in() ) { ?>
                   
                    <li><a href="<?php echo get_site_url(); ?>/?<?php echo get_option('woocommerce_logout_endpoint'); ?>=true" class="logout_link"><?php _e('Logout', 'mr_tailor'); ?></a></li>
                    <?php }else{
                ?>
                     <li><a href="<?php echo get_site_url(); ?>/my-account/?" class="logout_link"><?php _e('Login/Register', 'mr_tailor'); ?></a></li>
                   
                    <?php
                } ?></ul>    
            </nav><!-- #site-navigation -->
            
                           
        </div><!-- .large-8 .columns -->
                    
    </div><!-- .row -->
    
</div><!-- #site-top-bar -->