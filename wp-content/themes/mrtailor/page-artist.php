<?php
/*
Template Name: Artists
*/
?>

<?php get_header(); ?>
<script type="text/javascript" src="/lemon/simplePagination/jquery.simplePagination.js"></script>
<link type="text/css" rel="stylesheet" href="/lemon/simplePagination/simplePagination.css"/>
	<div class="full-width-page">
    
        <div id="primary" class="content-area">
           
            <div id="content" class="site-content" role="main">
                <br><?php woocommerce_breadcrumb(); ?>
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <div class="row art-wrap">
                    <div id="primary-sidebar" class="primary-sidebar widget-area" role="complementary">
                        <div><form role="search" method="get" class="search-form" action="">    
                            <div>
                                <label class="screen-reader-text" for="s">Search for:</label>
                                <input type="search" class="search-field" id="s" placeholder="Search..." value="" name="s">
                                <input type="submit" class="search-submit" value="Search">
                            </div>
                        </form></div>
                    </div
                        
                    <div class="large-12 columns">        

                        <div id="content" class="site-content  artists-index" role="main">
                            <?php 
                            $args = array( 'hide_empty=0' );

                            $terms = get_terms( 'multi_vendor', $args );
                            if ( ! empty( $terms ) && ! is_wp_error( $terms ) ) {
                                $count = count( $terms ); 
                                $i = 0;
                                $term_list = '';
                                foreach ( $terms as $term ) {
                                    
                                    $country = unserialize(get_country($term->term_id))[country_name];

                                    $term_list .='<div class="artist_box" style="visibilty:none">';
                                    $i++;

                                    $image = '';
                                    $thumbnail_id = absint(get_woocommerce_term_meta($term->term_id, 'thumbnail_id', true));
                                    
                                    if ($thumbnail_id)
                                        $image = wp_get_attachment_url($thumbnail_id);
                                    else
                                        $image = get_site_url().'/wp-content/uploads/2014/03/blog_quote-author.jpg';                                    

                                    if ($image != '') {
                                        $getimagefromurl = "<img src=" . $image . " >";
                                    } else {
                                        $getimagefromurl = '<div style="width:80px ;height:80px;"></div>';
                                    }
                                    
                                    $img = '<div class="artist_img">' . $getimagefromurl . $description . '';
                                    
                                    $term_list .= '<a href="' . get_term_link( $term ) . '" title="' . sprintf( __( 'View all post filed under %s', 'my_localization_domain' ), $term->name ) . '">'.$img.'<span class="artist_desc"><p class="artist_name">' . $term->name .'</p><p class="artist_country">'. $country . '</p></a></div>';
                                    
                                    

                                    $args = array( 'post_type' => 'product', 'posts_per_page' => 30, 'tax_query' => array(
		array(
			'taxonomy' => 'multi_vendor',
			'field'    => 'id',
			'terms'    => $term->term_id
		)
	), 'orderby' => 'rand' );

                                $loop = new WP_Query( $args );
                                $linked_products = array();
                               

                              $term_list .= '<div class="owl-gallery"><div class="owl-carousel">';
                                while ( $loop->have_posts() ) : $loop->the_post(); 
                                    
                                global $product; 
                                    if (!in_array($loop->post->ID, $linked_products, true)){
                                        $linked_div = '';
                                        
                                        $linked_product = get_field('linked_product',$loop->post->ID);                      
                                         if($linked_product){
                                                $cats = get_the_terms( $linked_product->ID, 'product_cat' );
                                                if($cats != ''){foreach ($cats as $cat) {
                                                   
                                                    $product_cat_id = $cat->term_id;        
                                                    $product_cat_name = $cat->name;
                                                    $parent = $cat;
                                                    $max = 0;
                                                    
                                                    while($product_cat_id != 33 && $product_cat_id != 31 && $max<10){
                                                        $parent = get_term_by ('id', $parent->parent ,'product_cat' );
                                                        $product_cat_name = $parent->name;
                                                        $product_cat_id = $parent->term_id;
                                                        $max++;
                                                    }
                                                    
                                                    break;
                                                }}
                                                $self_cats = get_the_terms( $loop->post->ID, 'product_cat' );
                                                foreach ($self_cats as $self_cat) {
                                                    
                                                    $self_product_cat_id = $self_cat->term_id;
                                                    $self_product_cat_name = $self_cat->name;
                                                    $parent = $self_cat;
                                                    $max = 0;
                                                    while($self_product_cat_id != 33 && $self_product_cat_id != 31 && $max<10){
                                                        $parent = get_term_by ('id', $parent->parent ,'product_cat' );
                                                        $self_product_cat_name = $parent->name;
                                                        $self_product_cat_id = $parent->term_id;
                                                        $max++;
                                                    }
                                                    
                                                    break;
                                                    
                                                }
                                                $linked_div = '<div class="self_product"><a href="'.get_permalink().'">'.$self_product_cat_name.'</a></div><div class="linked_product"><a href='.$linked_product->guid.'>'.$product_cat_name.'</a></div>';
                                                $term_list .= '<div class="item">'.get_the_post_thumbnail($loop->post->ID, 'thumbnail').$linked_div.'</div></a>';
                                                $linked_products[] = $linked_product->ID;
                                           
                                     }else{
                                         $cat = get_the_terms ($loop->post->ID,'product_cat' )[0]->name;
                                         $linked_div = '<div class="self_product solo_link"><a href="'.get_permalink().'">'.$cat.'</a></div>';    
                                             
                                         $term_list .= '<a href="'.get_permalink().'"><div class="item">'.get_the_post_thumbnail($loop->post->ID, 'thumbnail').$linked_div.'</div></a>';} 
                                    }
                                   
                                     
                                //echo '<li><a href="'.get_permalink().'">'.get_the_post_thumbnail($loop->post->ID, 'thumbnail').'</a></li>';

                                endwhile; 

                             $term_list .= '</div></div>';
                                   

                                wp_reset_query(); 

                                    
                                    
                                    

                                        $term_list .= '</div>';
                                    
                                }
                                echo $term_list;
                            }  ?>
                            
                            <script src='/lemon/wp-content/themes/mrtailor/js/owl.carousel.js'></script>
                            <style>
                                .owl-gallery {
                                    display: inline-block;
                                    width: 80%;
                                    
                                    padding: 5px;
                                    padding-bottom: 1px;
                                    margin-right: 3px;
                                    vertical-align: top;
                                }
                                .owl-carousel {
                                  display:inline-block;
                                  overflow: hidden;
                                }
                                .owl-carousel .item {
                                  padding: 0;
                                }
                                .owl-controls .owl-nav div{
    color: #FFF;
    display: inline-block;
    zoom: 1;
    *display: inline;/*IE7 life-saver */
    margin: 5px;
    padding: 3px 10px;
    font-size: 12px;
    -webkit-border-radius: 30px;
    -moz-border-radius: 30px;
    border-radius: 30px;
    background: black;
    filter: Alpha(Opacity=50);/*IE7 fix*/
    opacity: 0.7;
      position: absolute;
  top: 40%;
  z-index: 999999999;                                
}
.owl-controls .owl-nav .owl-prev{
    left: 0;
}
.owl-controls .owl-nav .owl-next{
    right: 0;
}
.owl-controls .owl-nav .owl-prev:hover,.owl-controls .owl-nav .owl-next:hover{
    opacity: 1;
    cursor:pointer;
}
                                .full-width-page .row {
    padding: 20px;
                                }
                                
                                
                            </style>
                            <script>
                                jQuery('.owl-carousel').owlCarousel({
                                    loop:true,
                                    margin:10,
                                    autoWidth:true,
                                    nav:true,
                                    navText: [
      "<",
      ">"
      ],
                                    lazyLoad : true,
mouseDrag: true,
    touchDrag: true,                                 
                                    responsive:{
                                        480:{
                                            items:1
                                        },
                                        767:{
                                            items:3
                                        },
                                        1000:{
                                            items:5
                                        }
                                    }
                                })
                                
        // Pagination
        jQuery(function() {
           var counter = 0;
           var items_per_page = 10;
           jQuery(".artist_box").each(function(){
                counter ++;
           });
            
            // Create the pagination
            jQuery('.pagination').pagination({
                items: counter,
                itemsOnPage: items_per_page,
                cssStyle: 'light-theme'
            });
            
            // On page click
            function add_events(){
                 jQuery('.page-link').click(function(){
                     var page = jQuery(this).text();
                     refresh_page(page);
                     add_events();
                 });
            }
            
            // refresh the artist boxes
            function refresh_page(page){
                first_element = ((page-1)*(items_per_page));
                last_element = first_element+items_per_page;
                jQuery( ".artist_box" ).fadeOut();
                jQuery( ".artist_box" ).slice( first_element, last_element ).fadeIn();
            }
            
            // Search bar
            jQuery('.search-form').submit( function(e){ 
                jQuery('.pagination').fadeOut();
                e.preventDefault();
                var wanted = jQuery('.search-form .search-field').val().toLowerCase();
                if(wanted == ''){
                    refresh_page( jQuery('.pagination').pagination('getCurrentPage'));
                    jQuery('.pagination').fadeIn();
                }else{
                    jQuery( ".artist_box" ).fadeOut();
                    jQuery('.artist_name').each(function(){
                        var artist = jQuery(this).text().toLowerCase();
                         if(artist.indexOf(wanted) !== -1){
                            jQuery(this).parent().parent().parent().parent().fadeIn();
                        }
                        
                    });
                }
            });
            
            // initiation
            add_events();
            refresh_page( jQuery('.pagination').pagination('getCurrentPage'));

        });
                            </script>
                       <div class='pagination'></div>
                        </div>
                        
                    </div>
                </div>
    
            </div><!-- #content -->           
            
        </div><!-- #primary -->
    
    </div><!-- .full-width-page -->
    
<?php get_footer(); ?>
