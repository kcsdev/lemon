<?php

if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $post;
//var_dump($post);
if (function_exists('wc_get_template')) {
    wc_get_template('archive-product.php');
} else {
    woocommerce_get_template('archive-product.php');
}
