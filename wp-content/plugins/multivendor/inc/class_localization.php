<?php

/* Localization Tab Settings */

class FPMultiVendorLocalizationTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_messages_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_localization', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_localization', array($this, 'multivendor_update_values_settings'));
    }

    public static function multivendor_messages_tab($settings_tabs) {
        $settings_tabs['multivendor_localization'] = __('Localization', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_localization_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_localization', array(
            array(
                'name' => __('Localization Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_localization_settings'
            ),
            array(
                'name' => __('Shortcode for Frontend Vendor Application [mv_vendor_application] and Vendor Status Application Shortcode is [mv_vendor_track_application]', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_for_shortcode',
            ),
            array(
                'name' => __('Vendor Name', 'multivendor'),
                'desc' => __('Customize Vendor Name in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_name',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Name',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_name',
                'desc_tip' => true,
            ),
            array(
                'name' => __('About Vendor', 'multivendor'),
                'desc' => __('Customize About Vendor in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_about_vendor',
                'css' => 'min-width:550px;',
                'std' => 'About Vendor',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_about_vendor',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message to Site Admin', 'multivendor'),
                'desc' => __('Customize Message to Site Admin in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_message_to_site_admin',
                'css' => 'min-width:550px;',
                'std' => 'Message to Site Admin',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_message_to_site_admin',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Commission Rate', 'multivendor'),
                'desc' => __('Customize Vendor Commission Rate in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_commission_rate',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Commission Rate',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_commission_rate',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Payment Method', 'multivendor'),
                'desc' => __('Customize Vendor Payment Method Caption in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_payment_method',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Payment Method',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_payment_method',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Paypal Address', 'multivendor'),
                'desc' => __('Customize Vendor Paypal Address in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_paypal_address',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Paypal Address',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_paypal_address',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Custom Payment', 'multivendor'),
                'desc' => __('Customize Vendor Custom Payment in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_custom_payment',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Custom Payment',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_custom_payment',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Logo/Shop Logo', 'multivendor'),
                'desc' => __('Customize Vendor Logo/Shop Logo Label in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_application_logo',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Logo/Shop Logo',
                'type' => 'textarea',
                'newids' => 'fpmv_vendor_application_logo',
                'desc_tip' => true,
            ),
            array(
                'name' => __('I Agree checkbox', 'multivendor'),
                'desc' => __('Customize I Agree checkbox in Front End Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_iagree_checkbox',
                'css' => 'min-width:550px;',
                'std' => 'I agree to submit the form',
                'type' => 'textarea',
                'newids' => 'fpmv_iagree_checkbox',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_application_localization_settings'),
            array(
                'name' => __('Application Status Shortcode Localization', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_application_localization_settings'
            ),
            array(
                'name' => __('Your Application Status', 'multivendor'),
                'desc' => __('Customize Vendor Your Application Status Message', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Your Application Status',
                'type' => 'textarea',
                'newids' => 'fpmv_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Name', 'multivendor'),
                'desc' => __('Customize Vendor Name in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_name_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Name',
                'type' => 'textarea',
                'newids' => 'fpmv_vendor_name_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('About Vendor', 'multivendor'),
                'desc' => __('Customize About Vendor in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_about_vendor_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'About Vendor',
                'type' => 'textarea',
                'newids' => 'fpmv_about_vendor_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Commission Rate', 'multivendor'),
                'desc' => __('Customize Vendor Commission Rate in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_commission_rate_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Commission Rate',
                'type' => 'textarea',
                'newids' => 'fpmv_vendor_commission_rate_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Vendor Paypal Address', 'multivendor'),
                'desc' => __('Customize Vendor Paypal Address in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_paypal_address_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Vendor Paypal Address',
                'type' => 'textarea',
                'newids' => 'fpmv_vendor_paypal_address_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Message from Reviewer', 'multivendor'),
                'desc' => __('Customize Message from Reviewer in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_message_from_reviewer_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Message from Reviewer',
                'type' => 'textarea',
                'newids' => 'fpmv_message_from_reviewer_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Status of Application', 'multivendor'),
                'desc' => __('Customize Status of Application in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_status_of_application_frontend_vendor_application_status',
                'css' => 'min-width:550px',
                'std' => 'Status of Application',
                'type' => 'textarea',
                'newids' => 'fpmv_status_of_application_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Date', 'multivendor'),
                'desc' => __('Customize Date in Frontend Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_date_frontend_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'Date',
                'type' => 'textarea',
                'newids' => 'fpmv_date_frontend_vendor_application_status',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_for_shortcode'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorLocalizationTab::multivendor_localization_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorLocalizationTab::multivendor_localization_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorLocalizationTab::multivendor_localization_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                //  if (get_option($settings['newids']) == FALSE) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}
//function ad_mail(){
//    
//    include 'application_status_mail_admin.php';
//    
//}
//add_action('wp_head','ad_mail');
new FPMultiVendorLocalizationTab();
