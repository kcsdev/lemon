<?php

function mv_product_accepted($post) {
    if (get_option('fpmv_product_approved_email_enable') == 'yes') {
        $status = 'Approved';
        mv_product_mail($post->ID, $status);
    }
}

add_action('pending_to_publish', 'mv_product_accepted');

function mv_product_submitted($post) {
    if (get_option('fpmv_product_submitted_email_enable') == 'yes') {
        $status = 'Submitted';
        mv_product_mail($post->ID, $status);
        $productname = get_the_title($post->ID);
    }
    if (get_option('fpmv_product_submitted_toadmin_email_enable') == 'yes') {
        $message = get_option('fpmv_product__email_message');
        $message = str_replace('{product_status}', $status, $message);
        $message = str_replace('{product_title}', $productname, $message);
        $userdata = get_user_by('email', get_option('admin_email'));
        $user_wmpl_lang = get_user_meta($userdata->ID, 'mv_wpml_lang', true);
        if (empty($user_wmpl_lang)) {
            $user_wmpl_lang = 'en';
        }
        $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_produt_email_subject', $user_wmpl_lang, get_option('fpmv_produt_email_subject'));
        ob_start();
        wc_get_template('emails/email-header.php', array('email_heading' => $subject));
        echo SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_product__email_message', $user_wmpl_lang, $message);
        wc_get_template('emails/email-footer.php');
        $temp_message = ob_get_clean();
        $admin_email = get_option('admin_email');

        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
        $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
        $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
        wp_mail($admin_email, $subject, $temp_message, $headers);
    }
}

add_action('draft_to_pending', 'mv_product_submitted');

function mv_product_rejected($post) {
    if (get_option('fpmv_product_rejected_email_enable') == 'yes') {
        $status = 'Rejected';
        mv_product_mail($post->ID, $status);
    }
}

add_action('pending_to_trash', 'mv_product_rejected');

function mv_product_pending($post) {
    if (get_option('fpmv_product_pending_email_enable') == 'yes') {
        $status = 'Pending';
        mv_product_mail($post->ID, $status);
    }
}

add_action('publish_to_pending', 'mv_product_pending');

function mv_product_deleted($post) {
    if (get_option('fpmv_product_deleted_email_enable') == 'yes') {
        $status = 'Deleted';
        mv_product_mail($post->ID, $status);
    }
}

add_action('publish_to_trash', 'mv_product_deleted');

function mv_product_mail($post_id, $status) {
    foreach (wp_get_post_terms($post_id, 'multi_vendor') as $categories) {
        $product_name = get_the_title($post_id);
        $vendor_admin_list = maybe_unserialize(get_woocommerce_term_meta($categories->term_id, 'fp_mv_vendor_admins', true));
        foreach ($vendor_admin_list as $user) {
            $userdata = get_userdata($user);
            $message = get_option('fpmv_product__email_message');
            $message = str_replace('{product_status}', $status, $message);
            $message = str_replace('{product_title}', $product_name, $message);
            $user_wmpl_lang = get_user_meta($userdata->ID, 'mv_wpml_lang', true);
            if (empty($user_wmpl_lang)) {
                $user_wmpl_lang = 'en';
            }
            $subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_product_email_subject', $user_wmpl_lang, get_option('fpmv_produt_email_subject'));
            ob_start();
            wc_get_template('emails/email-header.php', array('email_heading' => $subject));
            echo SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_product__email_message', $user_wmpl_lang, $message);
            wc_get_template('emails/email-footer.php');
            $temp_message = ob_get_clean();


            $headers = "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
            $headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            $headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
            wp_mail($userdata->user_email, $subject, $temp_message, $headers);
        }
    }
}
