<?php

class FPMultiVendorMainFunction {

    public function __construct() {
        if (get_option('fp_multi_vendor_order_status_control') != '') {
            foreach (get_option('fp_multi_vendor_order_status_control') as $values) {
                add_action('woocommerce_order_status_' . $values, array($this, 'multivendor_main_function'));
            }
        }
        //add_action('wp_head', array($this, 'get_line_total'));
    }

    /* From Order Get Product ID and Which Vendor */

    public static function multivendor_main_function($order_id) {
// $order_id = 201;
        global $wpdb;
        if (get_option('timezone_string') != '') {
            $timezonedate = date_default_timezone_set(get_option('timezone_string'));
        } else {
            $timezonedate = date_default_timezone_set('UTC');
        }

        $boolvaluechecker = get_post_meta($order_id, 'mv_commission_alloted', true);
        if ($boolvaluechecker == '') {
            $order = new WC_Order($order_id);
            $vendorids = array();
            $vendormasterlist = array();
            $currentvendordata = array();
            $order_total = $order->get_total();
            $getitemstotal = self::get_order_subtotal($order);
            $gettotaldiscount = $order->get_total_discount();
            $get_average_rate = self::get_average_discount_for_each_item($gettotaldiscount, $getitemstotal);

            foreach ($order->get_items() as $item) {
                if ($item['product_id'] != '') {
                    $vendorlist = wp_get_post_terms($item['product_id'], 'multi_vendor');
                    $getvendorcount = count($vendorlist);


                    if ($getvendorcount > 1) {
                        $getshippingcost = self::split_shipping_cost($order, $getvendorcount);
                        if ($item['variation_id'] != '') {
                            $variable_product1 = new WC_Product_Variation($item['variation_id']);
                            $newparentid = $variable_product1->parent->id;
                            if ($newparentid == $item['product_id']) {
                                if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                    $getregularprice = $variable_product1->regular_price;
                                } else {
                                    $getregularprice = $variable_product1->price;
                                }
                            } else {
                                if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                    $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                                } else {
                                    $getregularprice = get_post_meta($item['product_id'], '_price', true);
                                }
                            }
                        } else {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                            } else {
                                $getregularprice = get_post_meta($item['product_id'], '_price', true);
                            }
                        }
                        if (!empty($vendorlist)) {
                            if (get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true) != '') {
                                for ($i = 0; $i < $getvendorcount; $i++) {
                                    $getcommissionrate = get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true);
                                                 if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                    $commissionamount = $commissionaverage / 100;
                                    $commissionamount = $commissionamount / $getvendorcount;
                                    $table_name = $wpdb->prefix . "multi_vendor";
                                    $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'shippingcost' => $getshippingcost, 'productprice' => $getregularprice, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                                }
                            } elseif (get_option('fpmv_vendor_commission_rate_in_percentage') != '') {
                                for ($i = 0; $i < $getvendorcount; $i++) {
                                    $getcommissionrate = get_option('fpmv_vendor_commission_rate_in_percentage');
                                                  if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                    $commissionamount = $commissionaverage / 100;
                                    $commissionamount = $commissionamount / $getvendorcount;
                                    $table_name = $wpdb->prefix . "multi_vendor";
                                    $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'shippingcost' => $getshippingcost, 'productprice' => $getregularprice, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                                }
                            } else {
                                for ($i = 0; $i < $getvendorcount; $i++) {
                                    $getcommissionrate = 50;
                                                  if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                    $commissionamount = $commissionaverage / 100;
                                    $commissionamount = $commissionamount / $getvendorcount;
                                    $table_name = $wpdb->prefix . "multi_vendor";
                                    $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'shippingcost' => $getshippingcost, 'productprice' => $getregularprice, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendorlist[$i]->name, 'paidcommission' => '', 'vendorid' => $vendorlist[$i]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                                }
                            }
                        }
                    } else {
                        $getshippingcost = self::split_shipping_cost($order, $getvendorcount);
                        $vendor_list = wp_get_post_terms($item['product_id'], 'multi_vendor');
                        if ($item['variation_id'] != '') {
                            $variable_product1 = new WC_Product_Variation($item['variation_id']);
                            $newparentid = $variable_product1->parent->id;
                            if ($newparentid == $item['product_id']) {
                                if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                    $getregularprice = $variable_product1->regular_price;
                                } else {
                                    $getregularprice = $variable_product1->price;
                                }
                            } else {
                                if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                    $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                                } else {
                                    $getregularprice = get_post_meta($item['product_id'], '_price', true);
                                }
                            }
                        } else {
                            if (get_option('fpmv_vendor_calculate_product_price') == '1') {
                                $getregularprice = get_post_meta($item['product_id'], '_regular_price', true);
                            } else {
                                $getregularprice = get_post_meta($item['product_id'], '_price', true);
                            }
                        }


                        if (!empty($vendor_list)) {
                            if (get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true) != '') {


                                $getvendorid = $vendor_list[0]->term_id;
                                $getcommissionrate = get_post_meta($item['product_id'], '_fp_multi_vendor_commission_percentage', true);
                                              if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                $commissionamount = $commissionaverage / 100;
                                $vendorids[] = $getvendorid;
                                $vendorcommission[] = $commissionamount;
                                $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                                $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                                $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                                $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                                update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                                $table_name = $wpdb->prefix . "multi_vendor";
                                $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            } elseif (get_woocommerce_term_meta($vendor_list[0]->term_id, 'fp_mv_commissions_rate', true) != '') {
//category percentage

                                $getvendorid = $vendor_list[0]->term_id;
                                $getcommissionrate = get_woocommerce_term_meta($vendor_list[0]->term_id, 'fp_mv_commissions_rate', true);
                                              if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                $commissionamount = $commissionaverage / 100;
                                $vendorids[] = $getvendorid;
                                $vendorcommission[] = $commissionamount;
                                $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                                $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                                $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                                $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                                update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                                $table_name = $wpdb->prefix . "multi_vendor";
                                $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            } elseif (get_option('fpmv_vendor_commission_rate_in_percentage') != '') {
//Global Percentage;
                                $getvendorid = $vendor_list[0]->term_id;
                                $getcommissionrate = get_option('fpmv_vendor_commission_rate_in_percentage');
                                              if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                $commissionamount = $commissionaverage / 100;
                                $vendorids[] = $getvendorid;
                                $vendorcommission[] = $commissionamount;
                                $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                                $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                                $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                                $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                                update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                                $table_name = $wpdb->prefix . "multi_vendor";
                                $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            } else {
                                $getvendorid = $vendor_list[0]->term_id;
                                $getcommissionrate = 50;
                                              if ($gettotaldiscount == 0) {
                        $commissionaverage = ($get_average_rate * $getregularprice) * $getcommissionrate;
                    } else {
                        $commissionaverage = $get_average_rate * $getregularprice;
                        $commissionaverage = $getregularprice - $commissionaverage;
                        $commissionaverage = ($commissionaverage) * $getcommissionrate;
                    }
                                $commissionamount = $commissionaverage / 100;
                                $vendorids[] = $getvendorid;
                                $vendorcommission[] = $commissionamount;
                                $vendormasterlist[] = array('orderid' => $order_id, 'productid' => $item['product_id'], 'productname' => get_the_title($item['product_id']), 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s'));

//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist');
//delete_woocommerce_term_meta($vendor_list[0]->term_id, 'totalcommissions');

                                $oldvendormeta = (array) get_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', true);
                                $currentvendordata = array(array('orderid' => $order_id, 'productid' => $item['product_id'], 'productprice' => $getregularprice, 'shippingcost' => $getshippingcost, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorid' => $vendor_list[0]->name, 'date' => date('Y-m-d H:i:s')));
                                $updatedvendordata = array_merge($oldvendormeta, $currentvendordata);
                                update_woocommerce_term_meta($vendor_list[0]->term_id, 'mycommissionlist', array_filter($updatedvendordata));

                                $table_name = $wpdb->prefix . "multi_vendor";
                                $wpdb->insert($table_name, array('orderid' => $order_id, 'productid' => $item['product_id'], 'qty' => $item['qty'], 'productname' => get_the_title($item['product_id']), 'shippingcost' => $getshippingcost, 'productprice' => $getregularprice, 'discountedproductprice' => ($getregularprice-($get_average_rate * $getregularprice)), 'discountamount' => $gettotaldiscount, 'vendorcommissionrate' => $getcommissionrate, 'vendorcommission' => ($commissionamount * $item['qty']) + $getshippingcost, 'vendorname' => $vendor_list[0]->name, 'paidcommission' => '', 'vendorid' => $vendor_list[0]->term_id, 'status' => 'Due', 'date' => date('Y-m-d H:i:s')));
                            }
                        }
                    }
                }
            }
            $result = array();

            foreach ($vendorids as $i => $v) {
                if (!isset($result[$v])) {
                    $result[$v] = 0;
                }
                $result[$v] += $vendorcommission[$i];
            }
            foreach ($result as $value => $key) {
                $previouscommissions = get_woocommerce_term_meta($value, 'totalcommissions', true);
                $newcommissions = $previouscommissions + $key;
                update_woocommerce_term_meta($value, 'totalcommissions', $newcommissions);
            }
            $oldmasterlist = (array) get_option('fpmultivendormasterlist');
            $updatedvendormasterlist = array_merge($oldmasterlist, $vendormasterlist);
            update_option('fpmultivendormasterlist', array_filter($updatedvendormasterlist));

            update_post_meta($order_id, 'mv_commission_alloted', '1');
        }
    }

    public static function get_line_total() {
        $orderid = 63;
        $order = new WC_Order($orderid);
        echo "<pre>";
        var_dump($order->get_shipping_methods());

        foreach ($order->get_shipping_methods() as $values) {
            var_dump($values['cost']);
        }
        echo "</pre>";
    }

    public static function split_shipping_cost($order, $vendorcount) {
        if (get_option('fpmv_add_shipping_to_vendor_commission') == 'yes') {
            $getshippingmethods = $order->get_shipping_methods();
            $getshippingarraycost = array();
            foreach ($getshippingmethods as $eachshipping) {
                $getshippingarraycost[] = $eachshipping['cost'];
            }
            $arraysum = array_sum($getshippingarraycost);
            $calculate_withvendorcount = $arraysum / $vendorcount;
            return $calculate_withvendorcount;
        } else {
            return 0;
        }
    }

    public static function get_order_subtotal($order) {
        $subtotal = 0;
        // subtotal
        foreach ($order->get_items() as $item) {
            $subtotal += ( isset($item['line_subtotal']) ) ? $item['line_subtotal'] : 0;
        }
        return $subtotal;
    }

    public static function get_average_discount_for_each_item($discount, $items_total) {
        if (get_option('fpmv_calculate_commission_based_discounted_price') == 'yes') {
            $average = $discount / $items_total;
            return $average != 0 ? $average : 1;
        } else {
            return 1;
        }
    }

}

new FPMultiVendorMainFunction();
