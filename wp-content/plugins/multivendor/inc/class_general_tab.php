<?php

/* General Tab Settings */

class FPMultiVendorGeneralTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_general_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor', array($this, 'multivendor_update_values_settings'));
        add_action('admin_head', array($this, 'multivendor_add_chosen_to_status'));
    }

    public static function multivendor_general_tab($settings_tabs) {
        $settings_tabs['multivendor'] = __('General', 'multivendor');
        return $settings_tabs;
    }
    
   public static function multivendor_add_chosen_to_status() {
        global $woocommerce;
        if (isset($_GET['page'])) {
            if ($_GET['page'] == 'multivendor_menu') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                            jQuery('#fp_multi_vendor_exception_role_control').chosen();
                <?php } else { ?>
                            jQuery('#fp_multi_vendor_exception_role_control').select2();
                <?php } ?>
                    });
                </script>
                <?php
            }
    }
    }

    public static function multivendor_general_admin_settings() {
        global $woocommerce,$wp_roles;
//        global $current_user;
//        var_dump(get_role($current_user->roles[0]));
        $all_roles = $wp_roles->role_names;
//         foreach ($wp_roles->role_names as $value => $key) {
//             $testing1[$value]= $key; 
//         }
        return apply_filters('woocommerce_multivendor_general', array(
            array(
                'name' => __('General Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_general_settings'
            ),
            array(
                'name' => __('Vendor Commission in Percentage(%)', 'multivendor'),
                'desc' => __('Enter Value(without % symbol) for Vendor Commission. Please note if left empty by default 50% will be set', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_commission_rate_in_percentage',
                'css' => 'min-width:150px;',
                'std' => '50',
                'type' => 'text',
                'newids' => 'fpmv_vendor_commission_rate_in_percentage',
                'desc_tip' => true,
            ),
             array(
                'name' => __('Exception roles from Multi Vendor when vendor application is approved', 'multivendor'),
                'desc' => __('Note : If you select roles which can not access Multi Vendor Behaviour', 'multivendor'),
                'tip' => '',
                'id' => 'fp_multi_vendor_exception_role_control',
                'css' => 'min-width:150px;',
                'std' => array('Administrator','Shop Manager'),
                'type' => 'multiselect',
                'options' => $all_roles,
                'newids' => 'fp_multi_vendor_exception_role_control',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Calculate Vendor Commission based on Regular Price/Sale Price', 'multivendor'),
                'desc' => __('Choose an Option to calculate commission based on Regular Price or Sale Price', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_calculate_product_price',
                'css' => 'min-width:150px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('Based on Regular Price', 'multivendor'),
                    '2' => __('Based on Seller Price', 'multivendor'),
                ),
                'newids' => 'fpmv_vendor_calculate_product_price',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Calculate Commission based on Discounted Price', 'multivendor'),
                'desc' => __('Calculate the Commission based on Discounted Price if discount code is applied', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_calculate_commission_based_discounted_price',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'default' => 'yes',
                'newids' => 'fpmv_calculate_commission_based_discounted_price',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Add Shipping Cost to Vendor Commission', 'multivendor'),
                'desc' => __('Add a Shipping Cost directly to Vendor Commission without commission rate', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_add_shipping_to_vendor_commission',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'default' => 'yes',
                'newids' => 'fpmv_add_shipping_to_vendor_commission',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide Commission Field in Single Product Page for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Show/Hide Commission Field in Single Product Page for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_show_hide_commission_field_in_single_product',
                'css' => '',
                'std' => 'show',
                'default' => 'show',
                'type' => 'select',
                'options' => array(
                    'show' => __('Show', 'multivendor'),
                    'hide' => __('Hide', 'multivendor'),
                ),
                'newids' => 'fpmv_show_hide_commission_field_in_single_product',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_general_settings'),
            array(
                'name' => __('Frontend Application Form Customization', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_frontend_application_form_customization',
            ),
            array(
                'name' => __('Enable PayPal Payment Method Visible in Frontend Vendor Application Form', 'multivendor'),
                'desc' => __('Enable PayPal Payment Method Visible in Frontend Vendor Application Form', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_paypal_payment_method_in_frontend',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'default' => 'yes',
                'newids' => 'fpmv_paypal_payment_method_in_frontend',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Custom Payment Method Visible in Frontend Vendor Application Form', 'multivendor'),
                'desc' => __('Enable Custom Payment Method Visible in Frontend Vendor Application Form', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_custom_payment_method_in_frontend',
                'css' => 'min-width:150px;',
                'std' => 'yes',
                'type' => 'checkbox',
                'default' => 'yes',
                'newids' => 'fpmv_custom_payment_method_in_frontend',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Show/Hide I agree checkbox', 'multivendor'),
                'id' => 'fpmv_show_hide_iagree',
                'css' => 'min-width:100px;',
                'std' => '1',
                'type' => 'select',
                'options' => array(
                    '1' => __('Show', 'multivendor'),
                    '2' => __('Hide', 'multivendor'),
                ),
                
            ),
            array('type' => 'sectionend', 'id' => '_mv_frontend_application_form_customization'),
            array(
                'name' => __('CSV Settings (Export CSV for Paypal Mass Payment)', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_export_csv_settings'
            ),
            array(
                'name' => __('Custom Note for Paypal', 'multivendor'),
                'desc' => __('A Custom Note for Paypal', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_paypal_custom_notes',
                'css' => 'min-width:150px;',
                'std' => 'Thanks for your Business',
                'type' => 'textarea',
                'newids' => 'fpmv_vendor_paypal_custom_notes',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_export_csv_settings'),
            array(
                'name' => __('Socio Multi Vendor URL Slug Name Customization', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_vendor_slug_name_customization',
            ),
            array(
                'name' => __('Vendor URL Slug Name Customization', 'multivendor'),
                'desc' => __('Customize Slug Name for Vendor URL (Note It won\'t Work for Default Permalink Structure of WordPress)', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_url_slug_customization',
                'css' => 'min-width:150px;',
                'std' => 'multi_vendor',
                'type' => 'text',
                'newids' => 'fpmv_vendor_url_slug_customization',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_vendor_slug_name_customization'),
            array(
                'name' => __('Socio Multi Vendor Metabox Caption Customization', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_vendor_metabox_caption_customization',
            ),
            array(
                'name' => __('Socio Multi Vendor Metabox Name Customization', 'multivendor'),
                'desc' => __('Customize Metabox Name of Socio Multi Vendor from Product Page', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_vendor_metabox_name_customization',
                'css' => 'min-width:150px;',
                'std' => 'Artists',
                'type' => 'text',
                'newids' => 'fpmv_vendor_metabox_name_customization',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_vendor_metabox_caption_customization'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorGeneralTab::multivendor_general_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorGeneralTab::multivendor_general_admin_settings());
        FPMultiVendor::multi_vendor_create_custom_role_wordpress();
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorGeneralTab::multivendor_general_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                //  if (get_option($settings['newids']) == FALSE) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorGeneralTab();
