<?php

/* Vendor Capabilities Tab Settings */

class FPMultiVendorCapabilitiesTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_capabilities_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_capabilities', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_capabilities', array($this, 'multivendor_update_values_settings'), 10);
        add_action('woocommerce_update_options_multivendor_capabilities', array('FPMultiVendor', 'multi_vendor_create_custom_role_wordpress'), 99);
    }

    public static function multivendor_capabilities_tab($settings_tabs) {
        $settings_tabs['multivendor_capabilities'] = __('Vendor Capabilities', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_capabilities_admin_settings() {
        global $woocommerce;
//        global $current_user;
//        var_dump(get_role($current_user->roles[0]));
        return apply_filters('woocommerce_multivendor_capabilities', array(
            array(
                'name' => __('Vendor Capabilities', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_capabilities_user_role',
            ),
            array(
                'name' => __('Show/Hide Products for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Show/Hide Products for Multi Vendor User Role in Socio', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_hide_add_products_for_multivendor_user_role',
                'css' => '',
                'std' => 'yes',
                'type' => 'checkbox',
                'newids' => 'fpmv_hide_add_products_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Delete Products for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Enable Delete Products for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_enable_delete_products_for_multivendor_user_role',
                'css' => '',
                'std' => 'yes',
                'type' => 'checkbox',
                'newids' => 'fpmv_enable_delete_products_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Order Management for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Enable Order Management for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_enable_order_management_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_enable_order_management_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Delete Published Products in Multi Vendor User Role', 'multivendor'),
                'desc' => __('Enable Option to Delete Published Product in Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_delete_published_product_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_delete_published_product_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Edit Published Product for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Enable Option to Edit Published Product for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_edit_published_product_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_edit_published_product_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Publish Product Instantly', 'multivendor'),
                'desc' => __('Enable Publish Product Instantly for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_publish_product_instantly_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_publish_product_instantly_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Create Blog Post for Multi Vendor User Role', 'multivendor'),
                'desc' => __('Enable Option to Create Blog Post for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_create_blog_post_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_create_blog_post_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enable Publish Blog Post Instantly', 'multivendor'),
                'desc' => __('Enable Publish Blog Post Instantly for Multi Vendor User Role', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_publish_post_instantly_for_multivendor_user_role',
                'css' => '',
                'std' => 'no',
                'type' => 'checkbox',
                'newids' => 'fpmv_publish_post_instantly_for_multivendor_user_role',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_capabilities_user_role'),
            array(
                'name' => __('Vendor Application Approve Status', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_vendor_application_instantly',
            ),
            array(
                'name' => __('Automatic/Manual for Vendor Application Approval Process', 'multivendor'),
                'desc' => __('Approval Process of Vendor Application is Automatic or Manual by default Manual Approval in Admin Settings', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_approval_process_multi_vendor',
                'css' => '',
                'std' => '2',
                'type' => 'select',
                'options' => array(
                    '1' => __('Automatic Approval', 'multivendor'),
                    '2' => __('Manual Approval', 'multivendor')
                ),
                'newids' => 'fpmv_approval_process_multi_vendor',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_vendor_application_instantly'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorCapabilitiesTab::multivendor_capabilities_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorCapabilitiesTab::multivendor_capabilities_admin_settings());
        FPMultiVendor::multi_vendor_create_custom_role_wordpress();
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorCapabilitiesTab::multivendor_capabilities_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                //  if (get_option($settings['newids']) == FALSE) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorCapabilitiesTab();
