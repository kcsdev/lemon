<?php
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class FPMultiVendorListTable extends WP_List_Table {

    //var $fpmultivendormasterlist = (array) get_option('fpmultivendormasterlist');

    function __construct() {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'multi_vendor',
            'plural' => 'multi_vendors',
            'ajax' => true
        ));
    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    function column_orderid($item) {

        //Build row actions
        $actions = array(
            'edit' => sprintf('<a href="?page=multivendor_menu&tab=multivendor_commissions_edit&multivendor_id=%s">Edit</a>', $item['id']),
            'delete' => sprintf('<a href="?page=%s&tab=%s&action=%s&id=%s">Delete</a>', $_REQUEST['page'], $_REQUEST['tab'], 'delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
                /* $1%s */ $item['orderid'],
                /* $2%s */ $item['id'],
                /* $3%s */ $this->row_actions($actions)
        );
    }

    function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="id[]" value="%s" />', $item['id']
        );
    }

    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'orderid' => __('Order ID', 'multivendor'),
            'productname' => __('Product Name', 'multivendor'),
            'qty' => __('Product Quantity', 'multivendor'),
            'productprice' => __('Product Price ' . get_woocommerce_currency_symbol(), 'multivendor'),
            'discountedproductprice' => __('Discount Product Price ' . get_woocommerce_currency_symbol(), 'multivendor'),
            'discountamount' => __('Discount Coupon Value ' . get_woocommerce_currency_symbol(), 'multivendor'),
            'shippingcost' => __('Shipping Cost ' . get_woocommerce_currency_symbol(), 'multivendor'),
            'vendorcommissionrate' => __('Artist Commission Rate %', 'multivendor'),
            'vendorcommission' => __('Total Earned Commission ' . get_woocommerce_currency_symbol(), 'multivendor'),
            'vendorname' => __('Artist Name', 'multivendor'),
            'vendorid' => __('Artist ID', 'multivendor'),
            'status' => __('Status', 'multivendor'),
            'date' => __('Date', 'multivendor')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'title' => array('title', false), //true means it's already sorted
            'orderid' => array('orderid', false),
            'productname' => array('productname', false),
            'qty' => array('qty', false),
            'productprice' => array('productprice', false),
            'discountedproductprice' => array('discountedproductprice', false),
            'discountamount' => array('discountamount', false),
            'shippingcost' => array('shippingcost', false),
            'vendorcommissionrate' => array('vendorcommissionrate', false),
            'vendorcommission' => array('vendorcommission', false),
            'vendorname' => array('vendorname', false),
            'vendorid' => array('vendorid', false),
            'status' => array('status', false),
            'date' => array('date', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'delete' => __('Delete', 'multivendor'),
            'Due' => __('Mark as Due', 'multivendor'),
            'Paid' => __('Mark as Paid', 'multivendor'),
        );
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'multi_vendor'; // do not forget about tables prefix

        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        } elseif ('Paid' === $this->current_action()) {
            // var_dump($_REQUEST);
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
                $ids = explode(',', $ids);
                $countids = count($ids);
                foreach ($ids as $eachid) {
                    $wpdb->update($table_name, array('status' => 'Paid'), array('id' => $eachid));

                    $message = __($countids . ' Status Changed to Paid', 'multivendor');
                }
                if (!empty($message)):
                    ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php
                endif;
            }
        } else {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            if (!empty($ids)) {
                $ids = explode(',', $ids);
                $countids = count($ids);
                foreach ($ids as $eachid) {
                    $wpdb->update($table_name, array('status' => 'Due'), array('id' => $eachid));
                    $message = __($countids . ' Status Changed to Due', 'multivendor');
                }
                if (!empty($message)):
                    ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php
                endif;
            }
        }
    }

    function extra_tablenav($which) {
        global $wpdb;
        $mainlistarray = array();
        $tablename = $wpdb->prefix . 'multi_vendor';
        if (isset($_POST['select-vendor']) && !isset($_POST['fpmv_export_csv_paypal'])) {
            $param = array('select-vendor' => $_POST['select-vendor']);
            $url = get_permalink();
            $urli = esc_url_raw(add_query_arg($param, $url));
//            echo $urli;
            wp_safe_redirect($urli);
            exit();
        }
        if ($which == 'top') {
            ?>
            <div class="alignleft actions bulkactions">
                <?php
                $taxonomy = array('multi_vendor');
                $args = array(
                    'hide_empty' => false,
                );
                ?>

                <select name="select-vendor" class="filter-vendor">
                    <option value="0">All Vendors</option>
                    <?php
                    foreach (get_terms($taxonomy, $args) as $eachvendor) {
                        ?>
                        <option value="<?php echo $eachvendor->term_id; ?>"<?php selected($eachvendor->term_id, isset($_REQUEST['select-vendor']) ? $_REQUEST['select-vendor'] : '') ?>><?php echo $eachvendor->name; ?></option>
                        <?php
                    }
                    ?>
                </select>

                <input type="submit" class="button action" name="fpmv_due_pai_apply" id="fpmv_due_pai_apply" value="<?php _e('Filter', 'multivendor'); ?>"/>

            </div>
            <input type="submit" class="button-primary" name="fpmv_export_csv_paypal" id="fpmv_export_csv_paypal" value="<?php _e('Export Due Commission as CSV for Paypal Mass Payment', 'multivendor'); ?>"/>

            <?php
            if (isset($_REQUEST['select-vendor'])) {
                $filter_vendor = $_REQUEST['select-vendor'];
                if ($filter_vendor != 0) {
                    $getallresults = $wpdb->get_results("SELECT * FROM $tablename WHERE status='Due' and vendorid= $filter_vendor", ARRAY_A);
                } else {
                    $getallresults = $wpdb->get_results("SELECT * FROM $tablename WHERE status='Due'", ARRAY_A);
                }
            } else {
                $filter_vendor = 0;
                $getallresults = $wpdb->get_results("SELECT * FROM $tablename WHERE status='Due'", ARRAY_A);
            }


            if (isset($getallresults)) {
                foreach ($getallresults as $value) {
                    // var_dump($value['id']);
                    if ($value['vendorcommission'] != '') {
                        $mainlistarray[] = array(get_woocommerce_term_meta($value['vendorid'], 'mv_paypal_email', true), $value['vendorcommission'], get_woocommerce_currency(), $value['id'], get_option('fpmv_vendor_paypal_custom_notes'));
                    }
                }
                if (isset($_POST['fpmv_export_csv_paypal'])) {

                    ob_end_clean();
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=multivendor_paypal" . date("Y-m-d H:i:s") . ".csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");
                    $output = fopen("php://output", "w");
                    foreach ($mainlistarray as $row) {
                        fputcsv($output, $row); // here you can change delimiter/enclosure
                    }
                    fclose($output);
                    exit();
                }
            }
        }
    }

    function prepare_items() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'multi_vendor'; // do not forget about tables prefix

        if (isset($_REQUEST['select-vendor'])) {
            $filter_vendor = $_REQUEST['select-vendor'];
        } else {
            $filter_vendor = 0;
        }
        $this->process_bulk_action();
        // will be used in pagination settings
        if ($filter_vendor == 0) {
            $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name ");
            $newdata = $wpdb->get_results("SELECT * FROM $table_name", ARRAY_A);
        } else {
            $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name WHERE vendorid= $filter_vendor");
            $newdata = $wpdb->get_results("SELECT * FROM $table_name WHERE vendorid=$filter_vendor", ARRAY_A);
        }

        //usort($newdata, array(&$this, 'sort_data'));

        $perPage = 10;
        $currentPage = $this->get_pagenum();
        $totalItems = $this->set_pagination_args(array(
            'total_items' => $total_items,
            'per_page' => $perPage
        ));

        $newdata = array_slice($newdata, (($currentPage - 1) * $perPage), $perPage);

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        $this->items = $newdata;
    }

}

//new FPMultiVendorListTable();
function multivendor_add_chosen_to_vendor_name() {
    global $woocommerce;
    if (isset($_GET['tab'])) {
        if ($_GET['tab'] == 'multivendor_commissions') {
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        jQuery('#fpmv_search_vendor_name').chosen();
            <?php } else { ?>
                        jQuery('#fpmv_search_vendor_name').select2();
            <?php } ?>
                });
            </script>
            <?php
        }
    }
}

add_action('admin_head', 'multivendor_add_chosen_to_vendor_name');