<?php
/* Reports Tab Settings */

class FPMultiVendorReportsTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_reports_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_reports', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_reports', array($this, 'multivendor_update_values_settings'));
        add_action('woocommerce_admin_field_mv_report_generation', array($this, 'multivendor_report_generation'));
        //  add_action('woocommerce_admin_field_mv_export', array($this, 'mv_export'));
    }

    public static function multivendor_reports_tab($settings_tabs) {
        $settings_tabs['multivendor_reports'] = __('Reports', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_report_generation() {
        global $woocommerce;
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <script type="text/javascript">
            jQuery(document).ready(function () {
        <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    jQuery('#_fp_mv_select_vendor_to_exports').chosen();
                    jQuery('#_fp_mv_select_vendor_status_to_export').chosen();
        <?php } else { ?>
                    jQuery('#_fp_mv_select_vendor_to_exports').select2();
                    jQuery('#_fp_mv_select_vendor_status_to_export').select2();
        <?php } ?>
                if (jQuery('#_fp_mv_export_vendor_data_types').val() == '1') {
                    jQuery('#_fp_mv_select_vendor_to_exports').parent().parent().hide();
                } else {
                    jQuery('#_fp_mv_select_vendor_to_exports').parent().parent().show();
                }

                if (jQuery('#_fp_mv_export_vendor_status_data_types').val() == '1') {
                    jQuery('#_fp_mv_select_vendor_status_to_export').parent().parent().hide();
                } else {
                    jQuery('#_fp_mv_select_vendor_status_to_export').parent().parent().show();
                }
                jQuery(document).on('change', '#_fp_mv_export_vendor_data_types', function () {
                    if (jQuery(this).val() === '1') {
                        jQuery('#_fp_mv_select_vendor_to_exports').parent().parent().hide();
                    } else {
                        jQuery('#_fp_mv_select_vendor_to_exports').parent().parent().show();
                    }
                });

                jQuery(document).on('change', '#_fp_mv_export_vendor_status_data_types', function () {
                    if (jQuery(this).val() === '1') {
                        jQuery('#_fp_mv_select_vendor_status_to_export').parent().parent().hide();
                    } else {
                        jQuery('#_fp_mv_select_vendor_status_to_export').parent().parent().show();
                    }
                });
            });
        </script>
        <?php
        global $wpdb;
        $mainlistarray = array();
        $vendorexport = '';
        $tablename = $wpdb->prefix . 'multi_vendor';


        if (isset($_POST['_fp_mv_export_vendor_data_types'])) {
            if ($_POST['_fp_mv_export_vendor_data_types'] == '1') {
                $vendorexport = 1;
            } else {
                $vendorexport = 2;
            }
        }

        if (isset($_POST['_fp_mv_export_vendor_status_data_types'])) {
            if ($_POST['_fp_mv_export_vendor_status_data_types'] == '1') {
                $vendorstatusexport = 1;
            } else {
                $vendorstatusexport = 2;
            }
        }



        if (isset($_POST['_fp_mv_select_vendor_to_exports'])) {
            $vendorids = $_POST['_fp_mv_select_vendor_to_exports'];
        }
        ?>
        <input type="submit" class="button-primary" name="fpmv_export_csv_individual" id="fpmv_export_csv_individual" value="<?php _e('Export CSV', 'multivendor'); ?>"/>
        <?php
        if (($vendorexport == '1') && ($vendorstatusexport == '1')) {
            $getallresults = $wpdb->get_results("SELECT * FROM $tablename", ARRAY_A);
            if (isset($getallresults)) {
                foreach ($getallresults as $value) {
                    $mainlistarray[] = array($value['orderid'], $value['productname'], $value['vendorcommissionrate'], $value['vendorname'], $value['status'], $value['vendorcommission'], $value['date']);
                }
            }
        } elseif (($vendorexport == '1') && ($vendorstatusexport == '2')) {
            if (isset($_POST['_fp_mv_select_vendor_status_to_export'])) {
                foreach ($_POST['_fp_mv_select_vendor_status_to_export'] as $newstatus) {
                    $getallresults = $wpdb->get_results("SELECT * FROM `$tablename` WHERE `status`='$newstatus'", ARRAY_A);
                    if (isset($getallresults)) {
                        foreach ($getallresults as $value) {
                            $mainlistarray[] = array($value['orderid'], $value['productname'], $value['vendorcommissionrate'], $value['vendorname'], $value['status'], $value['vendorcommission'], $value['date']);
                        }
                    }
                }
            }
        } elseif (($vendorexport == '2') && ($vendorstatusexport == '1')) {
            if (isset($_POST['_fp_mv_select_vendor_to_exports'])) {
                foreach ($_POST['_fp_mv_select_vendor_to_exports'] as $vendorexport) {
                    $getallresults = $wpdb->get_results("SELECT * FROM $tablename WHERE vendorid=$vendorexport", ARRAY_A);
                    if (isset($getallresults)) {
                        foreach ($getallresults as $value) {
                            $mainlistarray[] = array($value['orderid'], $value['productname'], $value['vendorcommissionrate'], $value['vendorname'], $value['status'], $value['vendorcommission'], $value['date']);
                        }
                    }
                }
            }
        } elseif (($vendorexport == '2') && ($vendorstatusexport == '2')) {
            if (isset($_POST['_fp_mv_select_vendor_to_exports'])) {
                foreach ($_POST['_fp_mv_select_vendor_to_exports'] as $vendorexport) {
                    foreach ($_POST['_fp_mv_select_vendor_status_to_export'] as $newstatus) {
                        $getallresults = $wpdb->get_results("SELECT * FROM `$tablename` WHERE `vendorid`= '$vendorexport' and `status`= '$newstatus'", ARRAY_A);
                        if (isset($getallresults)) {
                            foreach ($getallresults as $value) {
                                $mainlistarray[] = array($value['orderid'], $value['productname'], $value['vendorcommissionrate'], $value['vendorname'], $value['status'], $value['vendorcommission'], $value['date']);
                            }
                        }
                    }
                }
            }
        }
        if (isset($_POST['fpmv_export_csv_individual'])) {
            ob_end_clean();
            header("Content-type: text/csv");
            header("Content-Disposition: attachment; filename=multivendor" . date("Y-m-d H:i:s") . ".csv");
            header("Pragma: no-cache");
            header("Expires: 0");
            $output = fopen("php://output", "w");
            foreach ($mainlistarray as $row) {
                fputcsv($output, $row); // here you can change delimiter/enclosure
            }
            fclose($output);
            exit();
        }
    }

    public static function multivendor_reports_admin_settings() {
        global $woocommerce;
        $listarray = array();
        $listarraytermname = array();
        $listarraytermid = array();
        $newfinalarray = array();
        $taxonomy = array('multi_vendor');
        $args = array(
            'hide_empty' => false,
        );
        foreach (get_terms($taxonomy, $args) as $eachvendor) {
            $listarraytermid[] = $eachvendor->term_id;
            $listarraytermname[] = $eachvendor->name;
        }
        if (is_array($listarraytermid) && is_array($listarraytermname)) {
            if (!empty($listarraytermid) && (!empty($listarraytermname))) {
                $newfinalarray = array_combine($listarraytermid, $listarraytermname);
            }
        }
        return apply_filters('woocommerce_multivendor_reports', array(
            array(
                'name' => __('Reports Settings', 'multivendor'),
                'type' => 'title',
                'desc' => __('<strong>Here you can export the data as CSV (This CSV cannot be used for PayPal Mass Payment) </strong>', 'multivendor'),
                'id' => '_mv_title_reports_settings'
            ),
            array(
                'name' => __('Export Vendor Data', 'multivendor'),
                'desc' => __('Choose an Option to Export Data as All or Custom', 'multivendor'),
                'id' => '_fp_mv_export_vendor_data_types',
                'class' => '_fp_mv_export_vendor_data_types',
                'std' => '1',
                'default' => '1',
                'newids' => '_fp_mv_export_vendor_data_types',
                'type' => 'select',
                'options' => array(
                    '1' => __('All', 'multivendor'),
                    '2' => __('Custom', 'multivendor'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Select Vendor to Export', 'multivendor'),
                'desc' => __('Choose the Vendor to Export the Data', 'multivendor'),
                'id' => '_fp_mv_select_vendor_to_exports',
                'css' => 'min-width:550px;',
                'std' => '',
                'default' => '',
                'newids' => '_fp_mv_select_vendor_to_exports',
                'type' => 'multiselect',
                'options' => $newfinalarray,
                'desc_tip' => true,
            ),
            array(
                'name' => __('Export Vendor Status Data', 'multivendor'),
                'desc' => __('Choose an Option to Export Status Data as All or Custom', 'multivendor'),
                'id' => '_fp_mv_export_vendor_status_data_types',
                'class' => '_fp_mv_export_vendor_status_data_types',
                'std' => '1',
                'default' => '1',
                'newids' => '_fp_mv_export_vendor_status_data_types',
                'type' => 'select',
                'options' => array(
                    '1' => __('All', 'multivendor'),
                    '2' => __('Custom', 'multivendor'),
                ),
                'desc_tip' => true,
            ),
            array(
                'name' => __('Export Based on Status', 'multivendor'),
                'desc' => __('Choose the Status of Vendor to Export', 'multivendor'),
                'id' => '_fp_mv_select_vendor_status_to_export',
                'css' => 'min-width:150px;',
                'std' => '',
                'default' => '',
                'newids' => '_fp_mv_select_vendor_status_to_export',
                'type' => 'multiselect',
                'options' => array(
                    'Due' => __('Due', 'multivendor'),
                    'Paid' => __('Paid', 'multivendor'),
                ),
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_reports_settings'),
            array(
                'type' => 'mv_export',
            ),
            array(
                'type' => 'mv_report_generation',
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_reports_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorReportsTab::multivendor_reports_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorReportsTab::multivendor_reports_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {

        foreach (FPMultiVendorReportsTab::multivendor_reports_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

}

new FPMultiVendorReportsTab();
