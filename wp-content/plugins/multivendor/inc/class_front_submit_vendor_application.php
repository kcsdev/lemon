<?php

class FP_Multi_Vendor_Submit_Applications {

    public function __construct() {
        add_shortcode('mv_vendor_application', array($this, 'create_frontend_vendor_application_form'));
        add_shortcode('mv_vendor_track_application', array($this, 'track_your_application'));
        add_action('wp_ajax_nopriv_mv_process_vendor_form', array($this, 'process_vendor_application'));
        add_action('wp_ajax_mv_process_vendor_form', array($this, 'process_vendor_application'));
        add_action('wp_ajax_nopriv_mv_delete_vendor_application', array($this, 'delete_vendor_application'));
        add_action('wp_ajax_mv_delete_vendor_application', array($this, 'delete_vendor_application'));
        if (get_option('fpmv_show_hide_vendor_application_status') == '1') {
            add_action('woocommerce_after_my_account', array($this, 'track_your_application_in_my_account'));
        }
    }

    public static function process_vendor_application() {
        global $wpdb;
// var_dump($_POST);
        if (isset($_POST)) {
            require_once(ABSPATH . "wp-admin" . '/includes/image.php');
            require_once(ABSPATH . "wp-admin" . '/includes/file.php');
            require_once(ABSPATH . "wp-admin" . '/includes/media.php');
            $getdataarray = $_POST;
            //$getvendordata = parse_str($_POST['data'], $getdataarray);

            $vendorname = $getdataarray['mvsetvendorname'];
            $aboutvendor = $getdataarray['mvsetvendordescription'];
            $messagevendor = $getdataarray['mvsetmessagetoadmin'];
            $vendorcommissionrate = $getdataarray['mvsetvendorcommissionrate'];
            if(isset($getdataarray['fpmv_frontend_vendor_payment_method_options'])){
                $vendorpaymentoption = $getdataarray['fpmv_frontend_vendor_payment_method_options'];
            }
            else{
                $vendorpaymentoption = '';
            }
            if(isset($getdataarray['fpmv_frontend_vendor_custom_payment_value'])){
                $vendorcustompaymentvalue = $getdataarray['fpmv_frontend_vendor_custom_payment_value'];
            }
            else{
                $vendorcustompaymentvalue = '';
            }
            if(isset($getdataarray['mvsetvendorpaypaladdress'])){
                $vendorpaypal = $getdataarray['mvsetvendorpaypaladdress'];
            }
            else{
                $vendorpaypal = '';
            }
            $vendoradmin = $getdataarray['mv_vendor_userid'];
            $setusernickname = $getdataarray['mv_vendor_username'];
            $table_name = $wpdb->prefix . "multi_vendor_submit_application";

            //check_ajax_referer('mv_upload_thumbnail');
            //get POST data
            //require the needed files
            if ($_FILES) {
                foreach ($_FILES as $file => $array) {
                    if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {
                        echo "upload error : " . $_FILES[$file]['error'];
                        die();
                    }
                    $attach_id = media_handle_upload($file, 0);
                }
            }



            if (get_option('fpmv_approval_process_multi_vendor') == '1') {
                $vendorslugname = strtolower(str_replace(' ', '-', $vendorname));
                $testingterm = wp_insert_term(
                        "$vendorname", // the term
                        'multi_vendor', // the taxonomy
                        array(
                    'description' => $aboutvendor,
                    'slug' => $vendorslugname,
                ));
                $vendoruser = new WP_User($vendoradmin);
                if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                    foreach ($vendoruser->roles as $role) {
                        add_user_meta($vendoruser->ID, 'multivendor_backuprole', $role);
                         if (get_option('fp_multi_vendor_exception_role_control') != '') {
                                                foreach (get_option('fp_multi_vendor_exception_role_control') as $values){
                                                    $exceptroles1[] = $values;
                                                }
//                                                var_dump($exceptroles1);
                                                $exceptroles2 = array('administrator', 'shop_manager');
                                                $exceptroles3 = array_merge($exceptroles2,$exceptroles1);
                                                $exceptroles = $exceptroles3;
                                             }
                                             else{
                                                $exceptroles = array('administrator', 'shop_manager');
                                             }
                        if (!in_array($role, $exceptroles)) {
                            $vendoruser->remove_role($role);
                            $vendoruser->add_role('multi_vendor');
                        }
                    }
                }
                //application approval
                if (get_option('fpmv_vendor_app_approval_email_enable') == 'yes') {
                    $mail_list = $vendoradmin;
                    $status = 'Approved';
                    include 'application_status_mail.php';
                }
                if (get_option('fpmv_vendor_app_approval_email_enable_admin') == 'yes') {
                    $status = 'Approved';
                    $getusernamebyid = get_user_by('id', $vendoradmin);
                    $newvendorname = $getusernamebyid->user_login;
                    $newvendorapplication = $vendorname;
                    include 'application_status_mail_admin.php';
                }



                //then loop over the files that were sent and store them using  media_handle_upload();

                update_woocommerce_term_meta($testingterm['term_id'], 'thumbnail_id', $attach_id);

                update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_vendor_admins', serialize(array($vendoradmin)));
                update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_commissions_rate', $vendorcommissionrate);
                update_woocommerce_term_meta($testingterm['term_id'], 'mv_paypal_email', $vendorpaypal);
                update_woocommerce_term_meta($testingterm['term_id'], 'mv_payment_option', $vendorpaymentoption);
                update_woocommerce_term_meta($testingterm['term_id'], 'mv_custom_payment', $vendorcustompaymentvalue);

                $wpdb->insert($table_name, array('setvendorname' => $vendorname, 'setmessagetoreviewer' => $messagevendor, 'setvendordescription' => $aboutvendor, 'setcreatedvendorid' => $testingterm['term_id'], 'setvendorcustompayment' => $vendorcustompaymentvalue, 'vendorthumbnail' => @$attach_id, 'setvendorpaymentoption' => @$vendorpaymentoption, 'setusernickname' => $setusernickname, 'setvendorcommission' => $vendorcommissionrate, 'setvendorpaypalemail' => @$vendorpaypal, 'setvendoradmins' => $vendoradmin, 'setvendordescription' => $aboutvendor, 'status' => get_option("fpmv_approval_process_multi_vendor") == "1" ? 'Approved' : 'Pending', 'date' => date('Y-m-d H:i:s')));
            } else {

                //then loop over the files that were sent and store them using  media_handle_upload();

                $wpdb->insert($table_name, array('setvendorname' => $vendorname, 'setmessagetoreviewer' => $messagevendor, 'setvendordescription' => $aboutvendor, 'setcreatedvendorid' => '0', 'setusernickname' => $setusernickname, 'setvendorcommission' => $vendorcommissionrate, 'vendorthumbnail' => @$attach_id, 'setvendorcustompayment' => @$vendorcustompaymentvalue, 'setvendorpaymentoption' => @$vendorpaymentoption, 'setvendorpaypalemail' => @$vendorpaypal, 'setvendoradmins' => $vendoradmin, 'setvendordescription' => $aboutvendor, 'status' => get_option("fpmv_approval_process_multi_vendor") == "1" ? 'Approved' : 'Pending', 'date' => date('Y-m-d H:i:s')));
                //application Pending
                if (get_option('fpmv_vendor_app_pending_email_enable') == 'yes') {
                    $mail_list = $vendoradmin;
                    $status = 'Pending';

                    include 'application_status_mail.php';
                }
                if (get_option('fpmv_vendor_app_pending_email_enable_admin') == 'yes') {

                    $status = 'Pending';
                    $getusernamebyid = get_user_by('id', $vendoradmin);
                    $newvendorname = $getusernamebyid->user_login;
                    $newvendorapplication = $vendorname;
                    include 'application_status_mail_admin.php';
                }
            }



            echo "success";
        }
        exit();
    }

    public static function delete_vendor_application() {
        global $wpdb;

        if (isset($_POST['applicationid'])) {
            $table_name = $wpdb->prefix . "multi_vendor_submit_application";
            $applicationid = $_POST['applicationid'];

            $checkresults = $wpdb->get_results("SELECT * FROM $table_name WHERE setcreatedvendorid NOT IN ( 0 ) and id=$applicationid", ARRAY_A);
            foreach ($checkresults as $eachvalues) {
                wp_delete_term($eachvalues['setcreatedvendorid'], 'multi_vendor');
                if ($eachvalues['vendorthumbnail'] != '') {
                    wp_delete_attachment($eachvalues['vendorthumbnail']);
                }
                $wpdb->update($table_name, array('setcreatedvendorid' => '0'), array('id' => $applicationid));
            }

            $wpdb->delete($table_name, array("id=>$applicationid"));
            if (get_option('fpmv_vendor_app_deleted_email_enable') == 'yes') {
                $mail_list = $eachvalues['setvendoradmins'];
                $status = 'Deleted';
                include 'application_status_mail.php';
            }
            if (get_option('fpmv_vendor_app_deleted_email_enable_admin') == 'yes') {
                $vendorname = $value->setvendorname;
                $status = 'Deleted';
                $getusernamebyid = get_user_by('id', $eachvalues['setvendoradmins']);
                $newvendorname = $getusernamebyid->user_login;
                $newvendorapplication = $eachvalues['setvendorname'];
                include 'application_status_mail_admin.php';
            }
            echo "success";
        }
        exit();
    }

    public static function edit_vendor_application() {
        global $wpdb;
        if (isset($_POST['editapplicationid'])) {
            $table_name = $wpdb->prefix . "multi_vendor_submit_application";
        }
    }

    public static function create_frontend_vendor_application_form($content) {
        global $wpdb;

        ob_start();
        if (is_user_logged_in()) {
            ?>
            <style type="text/css">
                td {
                    border-top:none;
                }
                .mvsetvendorname,.mvsetvendordescription,.mvsetmessagetoadmin,.mvsetvendorpaypaladdress,.mvsetvendorcommissionrate,.fpmv_frontend_vendor_custom_payment_value {
                    width:100%;
                }
                .mv_success_message {
                    display:none;
                }
                .newError, .rerror {
                    color:#ff0000;
                }
            </style>

            <script type="text/javascript">
                jQuery(document).ready(function () {
                    var currentvalue = jQuery('#fpmv_frontend_vendor_payment_method_options').val();
                    if (currentvalue === '1') {
                        jQuery('.fpmv_frontend_vendor_custom_payment_value').parent().parent().css('display', 'none');
                    } else {
                        jQuery('.fpmv_frontend_vendor_custom_payment_value').parent().parent().css('display', 'table-row');
                    }
                    jQuery('#fpmv_frontend_vendor_payment_method_options').change(function () {
                        var thisvalue = jQuery(this).val();
                        if (thisvalue === '1') {
                            jQuery('.mvsetvendorpaypaladdress').parent().parent().css('display', 'table-row');
                            jQuery('.fpmv_frontend_vendor_custom_payment_value').parent().parent().css('display', 'none');
                        } else {
                            if (thisvalue === '2') {
                                jQuery('.mvsetvendorpaypaladdress').parent().parent().css('display', 'none');
                                jQuery('.fpmv_frontend_vendor_custom_payment_value').parent().parent().css('display', 'table-row');
                            }
                        }
                        // alert(jQuery(this).val());
                    });


                    jQuery(".mv_submit_vendor_form").validate({
                        rules: {
                            mvsetvendorname: "required",
                            mvsetvendordescription: "required",
                            mvsetmessagetoadmin: "required",
                            mvsetvendorcommissionrate: "required",
                            mvsetvendorpaypaladdress: {
                                required: true,
                                email: true
                            },
                            fpmv_frontend_vendor_custom_payment_value: "required",
                            iagreeform: "required"

                        },
                        errorPlacement: function (error, element) {

                            console.log(jQuery(element).next());

                            jQuery(element).next().html(error);
                        }, messages: {
                            mvsetvendorname: "<?php echo addslashes(get_option('fpmv_error_message_for_vendor_name')); ?>",
                            mvsetvendordescription: "<?php echo addslashes(get_option('fpmv_error_message_for_vendor_description')); ?>",
                            mvsetmessagetoadmin: "<?php echo addslashes(get_option('fpmv_error_message_for_message_to_site_admin')); ?>",
                            mvsetvendorcommissionrate: "<?php echo addslashes(get_option('fpmv_error_message_for_vendor_commission_rate')); ?>",
                            mvsetvendorpaypaladdress: "<?php echo addslashes(get_option('fpmv_error_message_for_vendor_paypal_address')); ?>",
                            fpmv_frontend_vendor_custom_payment_value: "<?php echo addslashes(get_option('fpmv_error_message_for_vendor_custom_payment')); ?>",
                            iagreeform: "<?php echo "<br>" . addslashes(get_option('fpmv_error_message_for_checkbox')); ?>"
                        }

                    });

                    var options = {
                        beforeSubmit: showRequest, // pre-submit callback
                        success: showResponse, // post-submit callback
                        url: "<?php echo admin_url('admin-ajax.php'); ?>", // since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
                    };
                    // bind form using 'ajaxForm'
                    jQuery('.mv_submit_vendor_form').ajaxForm(options);
                    function showRequest(formData, jqForm, options) {
                        //alert('going_to_trigger');
                        //do extra stuff before submit like disable the submit button
                        //                        $('#output1').html('Sending...');
                        jQuery('#mv_submit').attr("disabled", "disabled");
                    }
                    function showResponse(responseText, statusText, xhr, $form) {
                        console.log(responseText);
                        console.log(statusText);
                        //do extra stuff after submit
                        var newresponse = responseText.replace(/\s/g, '');
                        if (newresponse === 'success') {

                            jQuery('.mv_submit_vendor_form')[0].reset();
                            jQuery('.mv_submit_vendor_form').delay(400).fadeOut(function () {
                                jQuery('.mv_success_message').delay(1000).css('display', 'block').html('<?php echo get_option('fpmv_success_message_for_vendor_application'); ?>');
                            });
                        }
                    }

                });
            </script>


            <div align="center" class="mv_success_message"></div>
            <form method="post"  class="mv_submit_vendor_form" name="mv_submit_vendor_form" action="#"  enctype="multipart/form-data">

                <table class="mv_create_form_vendor">

                    <tr>
                        <th>
                            <label><?php echo get_option('fpmv_frontend_vendor_name') ?></label>
                        </th>
                        <td>
                            <input type="text"  name="mvsetvendorname" style="border-color:black" class="mvsetvendorname" id="mvsetvendorname cemail" value=""/>
                            <div class="rerror"></div>
                        </td>
                    </tr>
                    <tr>
                        <th>
                            <label><?php echo get_option('fpmv_frontend_about_vendor'); ?></label>
                        </th>
                        <td>
                            <textarea name="mvsetvendordescription" style="border-color:black"  rows="3" cols="30" class="mvsetvendordescription" id="mvsetvendordescription"></textarea>
                            <div class="rerror"></div>
                        </td>
                    </tr>

                    <tr>
                        <th>
                            <label><?php echo get_option('fpmv_frontend_message_to_site_admin'); ?></label>
                        </th>
                        <td>
                            <textarea name="mvsetmessagetoadmin" style="border-color:black" rows="3" cols="30" class="mvsetmessagetoadmin" id="mvsetmessagetoadmin"></textarea>
                            <div class="rerror"></div>
                        </td>
                    </tr>

                    <tr>
                        <th>
                            <label><?php echo get_option('fpmv_frontend_vendor_commission_rate'); ?></label>
                        </th>
                        <td>
                            <input type="number" name="mvsetvendorcommissionrate"  style="border-color:black" class="mvsetvendorcommissionrate" id="mvsetvendorcommissionrate" value=""/>
                            <div class="rerror"></div>

                        </td>
                    </tr>
                    <?php
                    if ((get_option('fpmv_custom_payment_method_in_frontend') == 'yes') && (get_option('fpmv_paypal_payment_method_in_frontend') == 'yes')) {
                        ?>
                        <tr>
                            <th>
                                <label><?php echo get_option('fpmv_frontend_vendor_payment_method'); ?></label>
                            </th>
                            <td>
                                <select name="fpmv_frontend_vendor_payment_method_options" style="width:100%;" id="fpmv_frontend_vendor_payment_method_options">
                                    <option value="1"><?php _e('Paypal Address', 'multivendor'); ?></option>
                                    <option value="2"><?php _e('Custom Payment', 'multivendor'); ?></option>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <th>
                                <label><?php echo get_option('fpmv_frontend_vendor_paypal_address'); ?></label>
                            </th>
                            <td>
                                <input type="text" style="border-color:black"  name="mvsetvendorpaypaladdress" class="mvsetvendorpaypaladdress" id="mvsetvendorpaypaladdress" value=""/>
                                <div class="rerror"></div>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <label><?php echo get_option('fpmv_frontend_vendor_custom_payment'); ?></label>
                            </th>
                            <td>
                                <textarea name="fpmv_frontend_vendor_custom_payment_value" style="border-color:black" rows="3" cols="30" class="fpmv_frontend_vendor_custom_payment_value" id="fpmv_frontend_vendor_custom_payment_value"></textarea>
                                <div class="rerror"></div>
                            </td>
                        </tr>
                        <?php
                    } elseif ((get_option('fpmv_custom_payment_method_in_frontend') == 'no') && (get_option('fpmv_paypal_payment_method_in_frontend') == 'yes')) {
                        ?>
                        <tr>
                            <th>
                                <label><?php echo get_option('fpmv_frontend_vendor_paypal_address'); ?></label>
                            </th>
                            <td>
                                <input type="hidden" value="1" name="fpmv_frontend_vendor_payment_method_options"/>
                                <input type="text" data-rule-email="true" style="border-color:black"  name="mvsetvendorpaypaladdress" class="mvsetvendorpaypaladdress" id="mvsetvendorpaypaladdress" value=""/>
                            </td>
                        </tr>
                        <?php
                    } elseif((get_option('fpmv_custom_payment_method_in_frontend') == 'yes') && (get_option('fpmv_paypal_payment_method_in_frontend') == 'no')) {
                        ?>
                        <tr>
                            <th>
                                <label><?php echo get_option('fpmv_frontend_vendor_custom_payment'); ?></label>
                            </th>
                            <td>
                                <input type="hidden" value="2" name="fpmv_frontend_vendor_payment_method_options"/>
                                <textarea name="fpmv_frontend_vendor_custom_payment_value" style="border-color:black" rows="3" cols="30" class="fpmv_frontend_vendor_custom_payment_value" id="fpmv_frontend_vendor_custom_payment_value"></textarea>
                            </td>
                        </tr>
                        <?php
                    }
                    else{
                        
                    }
                    ?>
                    <tr>
                        <th>
                            <label><?php echo get_option('fpmv_vendor_application_logo'); ?></label>
                        </th>
                        <td>
                            <input type="file" name="fpmv_frontend_vendor_upload_image" class="fpmv_frontend_vendor_upload_image" id="fpmv_frontend_vendor_upload_image"/>
                        </td>

                    </tr>
                    <?php
                    $get_value_showhide = get_option('fpmv_show_hide_iagree');
                    if ($get_value_showhide == '1') {
                        ?>
                        <tr>

                            <th> </th>

                            <td>
                                <input type="checkbox"   name="iagreeform" value="1" /><?php echo get_option('fpmv_iagree_checkbox'); ?>
                                <div class="rerror"></div>
                                <!--<div style=" margin-top: -40px; margin-left:25px;  "><?php echo get_option('fpmv_error_message_for_checkbox'); ?></div>-->
                            </td>

                        </tr>
                        <?php
                    }
                    ?>

                </table>
                <input type='hidden' value='<?php wp_create_nonce('mv_upload_thumbnail'); ?>' name='_nonce' />

                <input type="hidden" name="mv_vendor_userid" class="mv_vendor_userid" id="mv_vendor_userid" value="<?php echo get_current_user_id(); ?>"/>
                <input type="hidden" name="action" id="action" value="mv_process_vendor_form">
                <?php $getuserdata = get_userdata(get_current_user_id());
                ?>
                <input type="hidden" name="mv_vendor_username" class="mv_vendor_username" id="mv_vendor_username" value="<?php echo $getuserdata->user_login; ?>"/>
                <input type="submit" name="mv_submit" class="mv_submit" id="mv_submit" value="<?php _e('Submit Application', 'multivendor'); ?>"/>
            </form>
            <?php
        } else {
            echo get_option('fpmv_message_to_guest_on_vendor_application');
        }
        $content = ob_get_clean();
        return $content;
    }

    public static function track_your_application($content) {
        global $wpdb;
        ob_start();
        if (is_user_logged_in()) {
            $getcurrentuserid = get_current_user_id();
            $getapplicationlist = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "multi_vendor_submit_application WHERE setvendoradmins=$getcurrentuserid", ARRAY_A);

            if (!empty($getapplicationlist)) {
                ?>
                <?php
                echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_track_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_track_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
                ?>

                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        //jQuery('.mv_track_application_status').footable();

                        jQuery('.mv_track_application_status').footable().bind('footable_filtering', function (e) {
                            var selected = jQuery('.filter-status').find(':selected').text();
                            if (selected && selected.length > 0) {
                                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                                e.clear = !e.filter;
                            }
                        });
                        jQuery('#multivendor_track_change-page-sizes').change(function (e) {
                            e.preventDefault();
                            var pageSize = jQuery(this).val();
                            jQuery('.footable').data('page-size', pageSize);
                            jQuery('.footable').trigger('footable_initialized');
                        });
                        jQuery('.mv_delete_app').click(function () {
                            if (confirm("Are you sure want to delete your Application?")) {
                                console.log(jQuery(this).parent().parent().hide());
                                var footable = jQuery('.mv_track_application_status').data('footable');
                                //get the row we are wanting to delete
                                var row = jQuery(this).parents('tr:first');
                                //delete the row
                                footable.removeRow(row);
                                var dataparam = ({
                                    action: 'mv_delete_vendor_application',
                                    applicationid: jQuery(this).attr('data-appid'),
                                });
                                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                        function (response) {
                                            //alert(response);
                                            var newresponse = response.replace(/\s/g, '');
                                            if (newresponse === 'success') {
                                            }
                                        });
                                return false;
                            }
                            return false;
                        });
                    });</script>
                <h3><?php echo get_option('fpmv_frontend_vendor_application_status'); ?></h3>
                <table data-filter = "#multivendor_track_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" class="mv_track_application_status">
                    <thead>
                        <tr>
                            <th data-toggle="true" data-sort-initial = "true"><?php echo get_option('fpmv_vendor_name_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_about_vendor_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_vendor_commission_rate_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_vendor_paypal_address_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_message_from_reviewer_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide=""><?php echo get_option('fpmv_status_of_application_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_date_frontend_vendor_application_status'); ?></th>
                            <th><?php _e('Delete Application', 'multivendor'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($getapplicationlist as $eachapplication) { ?>
                            <tr data-value="<?php echo $eachapplication['id']; ?>">
                                <td><?php echo $eachapplication['setvendorname']; ?></td>
                                <td><?php echo $eachapplication['setvendordescription']; ?></td>
                                <td><?php echo $eachapplication['setvendorcommission']; ?></td>
                                <td><?php echo $eachapplication['setvendorpaypalemail']; ?></td>
                                <td><?php echo $eachapplication['getmessagefromreviewer']; ?></td>
                                <td><?php echo $eachapplication['status']; ?></td>
                                <td><?php echo $eachapplication['date']; ?></td>
                                <?php
                                $addproduct = '<a href="' . admin_url() . 'post-new.php?post_type=product">' . __('Add Product', 'multivendor') . '</a>';
                                // $editfield = '<a href="" data-editid="' . $eachapplication['id'] . '" class="mv_edit_app">' . __('Edit', 'multivendor') . '</a>';
                                $deletefield = '<a href="javascript:void(0)" data-appid="' . $eachapplication['id'] . '" class="mv_delete_app" id="mv_delete_app">' . __('Delete', 'multivendor') . '</a>';
                                $edit_application = $eachapplication['status'] == 'Approved' ? $addproduct . " | " . $deletefield : $deletefield;
                                ?>
                                <td><?php echo $edit_application; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>

                <?php
            }
        } else {
            echo get_option('fpmv_message_to_guest_on_vendor_application_status');
        }
        $content = ob_get_clean();
        return $content;
    }

    public static function track_your_application_in_my_account() {
        global $wpdb;
        if (is_user_logged_in()) {
            $getcurrentuserid = get_current_user_id();
            $getapplicationlist = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "multi_vendor_submit_application WHERE setvendoradmins=$getcurrentuserid", ARRAY_A);

            if (!empty($getapplicationlist)) {
                ?>
                <?php
                echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_track_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_track_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
                ?>

                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        //jQuery('.mv_track_application_status').footable();

                        jQuery('.mv_track_application_status').footable().bind('footable_filtering', function (e) {
                            var selected = jQuery('.filter-status').find(':selected').text();
                            if (selected && selected.length > 0) {
                                e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                                e.clear = !e.filter;
                            }
                        });
                        jQuery('#multivendor_track_change-page-sizes').change(function (e) {
                            e.preventDefault();
                            var pageSize = jQuery(this).val();
                            jQuery('.footable').data('page-size', pageSize);
                            jQuery('.footable').trigger('footable_initialized');
                        });
                        jQuery('.mv_delete_app').click(function () {
                            if (confirm("Are you sure want to delete your Application?")) {
                                console.log(jQuery(this).parent().parent().hide());
                                var footable = jQuery('.mv_track_application_status').data('footable');
                                //get the row we are wanting to delete
                                var row = jQuery(this).parents('tr:first');
                                //delete the row
                                footable.removeRow(row);
                                var dataparam = ({
                                    action: 'mv_delete_vendor_application',
                                    applicationid: jQuery(this).attr('data-appid'),
                                });
                                jQuery.post("<?php echo admin_url('admin-ajax.php'); ?>", dataparam,
                                        function (response) {
                                            //alert(response);
                                            var newresponse = response.replace(/\s/g, '');
                                            if (newresponse === 'success') {
                                            }
                                        });
                                return false;
                            }
                            return false;
                        });
                    });
                </script>
                <h3><?php echo get_option('fpmv_frontend_vendor_application_status'); ?></h3>
                <table data-filter = "#multivendor_track_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" class="mv_track_application_status">
                    <thead>
                        <tr>
                            <th data-toggle="true" data-sort-initial = "true"><?php echo get_option('fpmv_vendor_name_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_about_vendor_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_vendor_commission_rate_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_vendor_paypal_address_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_message_from_reviewer_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide=""><?php echo get_option('fpmv_status_of_application_frontend_vendor_application_status'); ?></th>
                            <th data-toggle="true" data-hide="phone,tablet"><?php echo get_option('fpmv_date_frontend_vendor_application_status'); ?></th>
                            <th><?php _e('Delete Application', 'multivendor'); ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($getapplicationlist as $eachapplication) { ?>
                            <tr data-value="<?php echo $eachapplication['id']; ?>">
                                <td><?php echo $eachapplication['setvendorname']; ?></td>
                                <td><?php echo $eachapplication['setvendordescription']; ?></td>
                                <td><?php echo $eachapplication['setvendorcommission']; ?></td>
                                <td><?php echo $eachapplication['setvendorpaypalemail']; ?></td>
                                <td><?php echo $eachapplication['getmessagefromreviewer']; ?></td>
                                <td><?php echo $eachapplication['status']; ?></td>
                                <td><?php echo $eachapplication['date']; ?></td>
                                <?php
                                $addproduct = '<a href="' . admin_url() . 'post-new.php?post_type=product">Add Product</a>';
                                // $editfield = '<a href="" data-editid="' . $eachapplication['id'] . '" class="mv_edit_app">' . __('Edit', 'multivendor') . '</a>';
                                $deletefield = '<a href="javascript:void(0)" data-appid="' . $eachapplication['id'] . '" class="mv_delete_app" id="mv_delete_app">' . __('Delete', 'multivendor') . '</a>';
                                $edit_application = $eachapplication['status'] == 'Approved' ? $addproduct . " | " . $deletefield : $deletefield;
                                ?>
                                <td><?php echo $edit_application; ?></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>

                <?php
            }
        } else {
            echo get_option('fpmv_message_to_guest_on_vendor_application_status');
        }
    }

}

new FP_Multi_Vendor_Submit_Applications();
