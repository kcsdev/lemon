<?php
if (!class_exists('WP_List_Table')) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class FPMultiVendorApplicationsList extends WP_List_Table {

    function __construct() {
        global $status, $page;
        parent::__construct(array(
            'singular' => 'multi_vendor_application',
            'plural' => 'multi_vendors_applications',
            'ajax' => true
        ));
    }

    function column_default($item, $column_name) {
        return $item[$column_name];
    }

    function column_setvendorname($item) {

        //Build row actions
        $actions = array(
            'edit' => sprintf('<a href="?page=multivendor_menu&tab=multivendor_applications_edit&multivendor_application_id=%s">Edit</a>', $item['id']),
            'delete' => sprintf('<a href="?page=%s&tab=%s&action=%s&id=%s">Delete</a>', $_REQUEST['page'], $_REQUEST['tab'], 'application_delete', $item['id']),
        );

        //Return the title contents
        return sprintf('%1$s %3$s',
                /* $1%s */ $item['setvendorname'],
                /* $2%s */ $item['id'],
                /* $3%s */ $this->row_actions($actions)
        );
    }

    function column_cb($item) {
        return sprintf(
                '<input type="checkbox" name="id[]" value="%s" />', $item['id']
        );
    }

    function get_columns() {
        $columns = array(
            'cb' => '<input type="checkbox" />', //Render a checkbox instead of text
            'setvendorname' => __('Vendor Name', 'multivendor'),
            'setvendordescription' => __('About Vendor', 'multivendor'),
            'setusernickname' => __('Created by Vendor Admin (Username)', 'multivendor'),
            'setvendorcommission' => __('Vendor Commission Rate in %', 'multivendor'),
            'setvendorpaypalemail' => __('Vendor Paypal Address ', 'multivendor'),
            'status' => __('Application Status', 'multivendor'),
            'date' => __('Application Submission Date', 'multivendor')
        );
        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            'title' => array('title', false), //true means it's already sorted
            'setvendorname' => array('setvendorname', false),
            'setvendordescription' => array('setvendordescription', false),
            'setusernickname' => array('setusernickname', false),
            'setvendorcommission' => array('setvendorcommission', false),
            'status' => array('status', false),
            'date' => array('date', false)
        );
        return $sortable_columns;
    }

    function get_bulk_actions() {
        $actions = array(
            'application_delete' => __('Delete', 'multivendor'),
            'Approved' => __('Mark as Approve', 'multivendor'),
            'Rejected' => __('Mark as Reject', 'multivendor'),
        );
        return $actions;
    }

    function process_bulk_action() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'multi_vendor_submit_application'; // do not forget about tables prefix


        if ('application_delete' === $this->current_action()) {

            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            $mainids = explode(',', $ids);
            if (isset($mainids)) {


                foreach ($mainids as $deleteeachid) {
                    $checkresults = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=$deleteeachid", ARRAY_A);
                    foreach ($checkresults as $eachvalues) {


                        $vendoruser = new WP_User($eachvalues['setvendoradmins']);

                        $getmytaxonomy = array('multi_vendor');
                        $arguments = array(
                            'hide_empty' => false,
                        );
                        $termsid = array();
                        $includetermsid = array();
                        $getallvendors = get_terms($getmytaxonomy, $arguments);
                        foreach ($getallvendors as $eachvendor) {
                            $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                            // var_dump($vendoradminids);

                            if (!in_array($vendoruser->ID, (array) $vendoradminids)) {
                                $termsid[] = $eachvendor->term_id;
                            }

                            if (in_array($vendoruser->ID, (array) $vendoradminids)) {
                                $includetermsid[] = $eachvendor->term_id;
                            }
                        }


                        $countofuserwithvendor = count($includetermsid);

                        if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                            foreach ($vendoruser->roles as $role) {
                                $backuprole = get_user_meta($vendoruser->ID, 'multivendor_backuprole', true);
                                $exceptroles = array('multi_vendor');
                                if (in_array($role, $exceptroles)) {
                                    if ($countofuserwithvendor <= 0) {
                                        $vendoruser->remove_role($role);
                                        $vendoruser->add_role($backuprole);
                                    }
                                }
                            }
                        }

                        //application delete
                        if (get_option('fpmv_vendor_app_deleted_email_enable') == 'yes') {
                            $mail_list = $eachvalues['setvendoradmins'];
                            $status = 'Deleted';
                            include 'application_status_mail.php';
                        }
                        if (get_option('fpmv_vendor_app_deleted_email_enable_admin') == 'yes') {
                            $status = 'Deleted';
                            $getusernamebyid = get_user_by('id', $eachvalues['setvendoradmins']);
                            $newvendorname = $getusernamebyid->user_login;
                            $newvendorapplication = $eachvalues['setvendorname'];
                            include 'application_status_mail_admin.php';
                        }

                        if ($eachvalues['vendorthumbnail'] != '') {
                            wp_delete_attachment($eachvalues['vendorthumbnail']);
                        }
                        wp_delete_term($eachvalues['setcreatedvendorid'], 'multi_vendor');

                        $wpdb->update($table_name, array('setcreatedvendorid' => '0'), array('id' => $deleteeachid));
                    }
                }
            }



            if (!empty($ids)) {
                $wpdb->query("DELETE FROM $table_name WHERE id IN($ids)");
            }
        } elseif ('Approved' === $this->current_action()) {
            // var_dump($_REQUEST);
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (is_array($ids))
                $ids = implode(',', $ids);

            $mainids = explode(',', $ids);
            if (isset($mainids)) {
                $countids = count($mainids);
                foreach ($mainids as $eachid) {
                    $wpdb->update($table_name, array('status' => 'Approved'), array('id' => $eachid));

                    $getresults = $wpdb->get_results("SELECT * FROM $table_name WHERE setcreatedvendorid='0' and id=$eachid", ARRAY_A);
                    foreach ($getresults as $eachresults) {
                        $vendorname = $eachresults['setvendorname'];
                        $vendordescription = $eachresults['setvendordescription'];
                        $vendorslugname = strtolower(str_replace(' ', '-', $eachresults['setvendorname']));
                        $testingterm = wp_insert_term(
                                "$vendorname", // the term
                                'multi_vendor', // the taxonomy
                                array(
                            'description' => $vendordescription,
                            'slug' => $vendorslugname,
                        ));
                        $vendoruser = new WP_User($eachresults['setvendoradmins']);
                        if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                            foreach ($vendoruser->roles as $role) {
                                add_user_meta($vendoruser->ID, 'multivendor_backuprole', $role);
                                    if (get_option('fp_multi_vendor_exception_role_control') != '') {
                                                foreach (get_option('fp_multi_vendor_exception_role_control') as $values){
                                                    $exceptroles1[] = $values;
                                                }
//                                                var_dump($exceptroles1);
                                                $exceptroles2 = array('administrator', 'shop_manager');
                                                $exceptroles3 = array_merge($exceptroles2,$exceptroles1);
                                                $exceptroles = $exceptroles3;
                                             }
                                             else{
                                                $exceptroles = array('administrator', 'shop_manager');
                                             }
                                if (!in_array($role, $exceptroles)) {
                                    $vendoruser->remove_role($role);
                                    $vendoruser->add_role('multi_vendor');
                                }
                            }
                        }

                        //application approval
                        if (get_option('fpmv_vendor_app_approval_email_enable_admin') == 'yes') {
                            $status = 'Approved';
                            $getusernamebyid = get_user_by('id', $eachresults['setvendoradmins']);
                            $newvendorname = $getusernamebyid->user_login;
                            $newvendorapplication = $eachresults['setvendorname'];
                            include 'application_status_mail_admin.php';
                        }
                        if (get_option('fpmv_vendor_app_approval_email_enable') == 'yes') {
                            $mail_list = $eachresults['setvendoradmins'];
                            $status = 'Approved';
                            include 'application_status_mail.php';
                        }
                        update_woocommerce_term_meta($testingterm['term_id'], 'thumbnail_id', $eachresults['vendorthumbnail']);

                        update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_vendor_admins', serialize(array($eachresults['setvendoradmins'])));
                        update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_commissions_rate', $eachresults['setvendorcommission']);
                        update_woocommerce_term_meta($testingterm['term_id'], 'mv_paypal_email', $eachresults['setvendorpaypalemail']);
                        update_woocommerce_term_meta($testingterm['term_id'], 'mv_payment_option', $eachresults['setvendorpaymentoption']);
                        update_woocommerce_term_meta($testingterm['term_id'], 'mv_custom_payment', $eachresults['setvendorcustompayment']);

                        $wpdb->update($table_name, array('setcreatedvendorid' => $testingterm['term_id']), array('id' => $eachid));
                    }
                    $message = __($countids . ' Status Changed to Approved', 'multivendor');
                }
                if (!empty($message)):
                    ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php
                endif;
            }
        }else {
            if ('Rejected' === $this->current_action()) {
                $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
                if (is_array($ids))
                    if (!empty($ids)) {
                        $ids = implode(',', $ids);
                        $ids = explode(',', $ids);
                        $countids = count($ids);
                        foreach ($ids as $eachid) {
                            $wpdb->update($table_name, array('status' => 'Rejected'), array('id' => $eachid));

                            $checkresults = $wpdb->get_results("SELECT * FROM $table_name WHERE `id`=$eachid", ARRAY_A);

                            foreach ($checkresults as $eachvalues) {




                                $vendoruser = new WP_User($eachvalues['setvendoradmins']);

                                $getmytaxonomy = array('multi_vendor');
                                $arguments = array(
                                    'hide_empty' => false,
                                );
                                $termsid = array();
                                $includetermsid = array();
                                $getallvendors = get_terms($getmytaxonomy, $arguments);
                                foreach ($getallvendors as $eachvendor) {
                                    $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                                    // var_dump($vendoradminids);

                                    if (!in_array($vendoruser->ID, (array) $vendoradminids)) {
                                        $termsid[] = $eachvendor->term_id;
                                    }

                                    if (in_array($vendoruser->ID, (array) $vendoradminids)) {
                                        $includetermsid[] = $eachvendor->term_id;
                                    }
                                }

                                $countofuserwithvendor = count($includetermsid);

                                if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                                    foreach ($vendoruser->roles as $role) {
                                        $backuprole = get_user_meta($vendoruser->ID, 'multivendor_backuprole', true);
                                        $exceptroles = array('multi_vendor');
                                        if (in_array($role, $exceptroles)) {
                                            if ($countofuserwithvendor <= 0) {
                                                $vendoruser->remove_role($role);
                                                $vendoruser->add_role($backuprole);
                                            }
                                        }
                                    }
                                }


                                //application rejection
                                if (get_option('fpmv_vendor_app_rejection_email_enable') == 'yes') {
                                    $mail_list = $eachvalues['setvendoradmins'];
                                    $status = 'Rejected';
                                    include 'application_status_mail.php';
                                    if (get_option('fpmv_vendor_app_rejection_email_enable_admin') == 'yes') {
                                        $status = 'Rejected';
                                        $getusernamebyid = get_user_by('id', $eachvalues['setvendoradmins']);
                                        $newvendorname = $getusernamebyid->user_login;
                                        $newvendorapplication = $eachvalues['setvendorname'];
                                        include 'application_status_mail_admin.php';
                                    }
                                }


                                if ($eachvalues['vendorthumbnail'] != '') {
                                    wp_delete_attachment($eachvalues['vendorthumbnail']);
                                }

                                wp_delete_term($eachvalues['setcreatedvendorid'], 'multi_vendor');
                                $wpdb->update($table_name, array('setcreatedvendorid' => '0'), array('id' => $eachid));
                            }
                            $message = __($countids . ' Status Changed to Rejected', 'multivendor');
                        }
                        if (!empty($message)):
                            ?>
                            <div id="message" class="updated"><p><?php echo $message ?></p></div>
                            <?php
                        endif;
                    }
            }
        }
    }

    function prepare_items() {
        global $wpdb;
        $table_name = $wpdb->prefix . 'multi_vendor_submit_application'; // do not forget about tables prefix

        $per_page = 10; // constant, how much records will be shown per page

        $columns = $this->get_columns();
        $hidden = array();
        $sortable = $this->get_sortable_columns();

        // here we configure table headers, defined in our methods
        $this->_column_headers = array($columns, $hidden, $sortable);

        // [OPTIONAL] process bulk action if any
        $this->process_bulk_action();

        // will be used in pagination settings
        $total_items = $wpdb->get_var("SELECT COUNT(id) FROM $table_name");

        // prepare query params, as usual current page, order by and order direction
        $paged = isset($_REQUEST['paged']) ? max(0, intval($_REQUEST['paged']) - 1) : 0;
        $orderby = (isset($_REQUEST['orderby']) && in_array($_REQUEST['orderby'], array_keys($this->get_sortable_columns()))) ? $_REQUEST['orderby'] : 'id';
        $order = (isset($_REQUEST['order']) && in_array($_REQUEST['order'], array('asc', 'desc'))) ? $_REQUEST['order'] : 'asc';

        // [REQUIRED] define $items array
        // notice that last argument is ARRAY_A, so we will retrieve array
        $this->items = $wpdb->get_results($wpdb->prepare("SELECT * FROM $table_name ORDER BY $orderby $order LIMIT %d OFFSET %d", $per_page, $paged), ARRAY_A);

        // [REQUIRED] configure pagination
        $this->set_pagination_args(array(
            'total_items' => $total_items, // total items defined above
            'per_page' => $per_page, // per page constant defined at top of method
            'total_pages' => ceil($total_items / $per_page) // calculate pages count
        ));
    }

}
