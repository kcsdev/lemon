<?php

class FPMultiVendorCustomCategory {

    public function __construct() {
        add_action('woocommerce_register_taxonomy', array($this, 'register_new_taxonomy'), '0');
        add_action('multi_vendor_add_form_fields', array($this, 'add_category_fields'));
        add_action('multi_vendor_edit_form_fields', array($this, 'edit_category_fields'), 10, 2);
        add_action('created_term', array($this, 'save_category_fields'), 10, 3);
        add_action('edit_term', array($this, 'save_category_fields'), 10, 3);
        add_action('delete_term', array($this, 'delete_term_fields'), 1, 3);
        add_action('admin_head-edit-tags.php', array($this, 'remove_parent_selection'));
    }

    /* Register New Taxonomy for WooCommerce Products */

    public static function register_new_taxonomy() {
        $labels = array(
            'name' => _x(get_option('fpmv_vendor_metabox_name_customization'), 'taxonomy general name'),
            'singular_name' => _x(get_option('fpmv_vendor_metabox_name_customization'), 'taxonomy singular name'),
            'search_items' => __('Search ' . get_option('fpmv_vendor_metabox_name_customization'), 'multivendor'),
            'all_items' => __(get_option('fpmv_vendor_metabox_name_customization'), 'multivendor'),
            'parent_item' => __('Parent ' . get_option('fpmv_vendor_metabox_name_customization'), 'multivendor'),
            'parent_item_colon' => __('Parent ' . get_option('fpmv_vendor_metabox_name_customization') . ':'),
            'edit_item' => __('Edit ' . get_option('fpmv_vendor_metabox_name_customization'), 'multivendor'),
            'update_item' => __('Update ' . get_option('fpmv_vendor_metabox_name_customization'), 'multivendor'),
            'add_new_item' => __('Add New Vendor', 'multivendor'),
            'new_item_name' => __('New Vendor', 'multivendor'),
        );

        register_taxonomy('multi_vendor', array('product'), array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'show_in_nav_menus' => true,
            'capabilities' => array(
                //'manage_terms' => 'manage_product_terms',
                //'edit_terms' => 'edit_product_terms',
                //'delete_terms' => 'delete_product_terms',
                'assign_terms' => 'assign_product_terms',
            ),
            'rewrite' => array('slug' => get_option('fpmv_vendor_url_slug_customization'), 'with_front' => false),
        ));
        //   flush_rewrite_rules();
    }

    public static function add_category_fields() {
        global $woocommerce;
        ?>
        <?php
        if (isset($_GET['taxonomy'])) {
            if ($_GET['taxonomy'] == 'multi_vendor') {
                ?>
                <script type="text/javascript">
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        // jQuery('#fp_mv_vendor_admins').chosen();
                        jQuery(function () {
                            // Ajax Chosen Product Selectors
                            jQuery("select.fp_mv_vendor_admins").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_customers',
                                    security: '<?php echo wp_create_nonce("search-customers"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
                <?php } ?>

                </script>
                <style type="text/css">
                    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
                        height:25px !important;
                    }
                </style>
                <?php
            }
        }
        ?>


        <div class="form-field">
            <label for="fp_mv_commissions_rate"><?php _e('Vendor Commission in Percentage(%)', 'multivendor'); ?></label>
            <input type="text" name="fp_mv_commissions_rate" id="fp_mv_commissions_rate" value=""/>
        </div>
        <div class="form-field">
            <label for="fp_mv_vendor_admins"><?php _e('Vendor Admins', 'multivendor'); ?></label>
            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                <select id="fp_mv_vendor_admins" name="fp_mv_vendor_admins[]" class="fp_mv_vendor_admins" style="width:90%;" multiple="multiple">
                    <option value=""></option>
                </select>
            <?php } else { ?>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            <!--                <input type="hidden" class="wc-customer-search" name="fp_mv_vendor_admins" data-multiple="true" data-placeholder="<?php _e('Search for a customer&hellip;', 'multivendor'); ?>" data-selected="" value="" data-allow_clear="true" />
                -->

                <input type="hidden" class="wc-customer-search" style="width: 100%;" id="fp_mv_vendor_admins" name="fp_mv_vendor_admins" data-placeholder="<?php _e('Search for members', 'multivendor'); ?>" data-action="woocommerce_json_search_customers" data-multiple="true" data-selected="" value="" />


            <?php } ?>
            <?php
            ?>

        </div>
        <script type="text/javascript">
            jQuery(document).ready(function () {
                var initvalue = jQuery('.mv_payment_option').val();
                if (initvalue === '1') {
                    jQuery('.mvpaypalform').css('display', 'block');
                    jQuery('.mvcustomform').css('display', 'none');
                } else {
                    jQuery('.mvpaypalform').css('display', 'none');
                    jQuery('.mvcustomform').css('display', 'block');
                }
                jQuery('.mv_payment_option').change(function () {
                    var thisvalue = jQuery(this).val();
                    if (thisvalue === '1') {
                        jQuery('.mvpaypalform').css('display', 'block');
                        jQuery('.mvcustomform').css('display', 'none');
                    } else {
                        jQuery('.mvpaypalform').css('display', 'none');
                        jQuery('.mvcustomform').css('display', 'block');
                    }
                });
            });
        </script>
        <div class="form-field" class="mv_payment_option_class">
            <label for="mv_payment_option"><?php _e('Payment Option', 'multivendor'); ?></label>
            <select name="mv_payment_option" class="mv_payment_option">
                <option value="1"><?php _e('Paypal Address', 'multivendor'); ?></option>
                <option value="2"><?php _e('Custom Payment', 'multivendor'); ?></option>
            </select>
        </div>
        <div class="form-field mvpaypalform">
            <label for="mv_paypal_email"><?php _e('Vendor PayPal Email Address', 'multivendor'); ?></label>
            <input type="text" name="mv_paypal_email" id="mv_paypal_email" value=""/>
        </div>
        <div class="form-field mvcustomform">
            <label for="mv_custom_payment"><?php _e('Custom Payment', 'multivendor'); ?></label>
            <textarea name="mv_custom_payment" rows="3" cols="30"></textarea>
        </div>
        <div class="form-field">
            <label><?php _e('Thumbnail', 'multivendor'); ?></label>
            <div id="multi_vendor_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo wc_placeholder_img_src(); ?>" width="60px" height="60px" /></div>
            <div style="line-height:60px;">
                <input type="hidden" id="multi_vendor_thumbnail_id" name="multi_vendor_thumbnail_id" />
                <button type="button" class="upload_image_button button"><?php _e('Upload/Add image', 'multivendor'); ?></button>
                <button type="button" class="remove_image_button button"><?php _e('Remove image', 'multivendor'); ?></button>
            </div>
            <script type="text/javascript">

                // Only show the "remove image" button when needed
                if (!jQuery('#multi_vendor_thumbnail_id').val())
                    jQuery('.remove_image_button').hide();

                // Uploading files
                var file_frame;

                jQuery(document).on('click', '.upload_image_button', function (event) {

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if (file_frame) {
                        file_frame.open();
                        return;
                    }

                    // Create the media frame.
                    file_frame = wp.media.frames.downloadable_file = wp.media({
                        title: '<?php _e('Choose an image', 'woocommerce'); ?>',
                        button: {
                            text: '<?php _e('Use image', 'woocommerce'); ?>',
                        },
                        multiple: false
                    });

                    // When an image is selected, run a callback.
                    file_frame.on('select', function () {
                        attachment = file_frame.state().get('selection').first().toJSON();

                        jQuery('#multi_vendor_thumbnail_id').val(attachment.id);
                        jQuery('#multi_vendor_thumbnail img').attr('src', attachment.url);
                        jQuery('.remove_image_button').show();
                    });

                    // Finally, open the modal.
                    file_frame.open();
                });

                jQuery(document).on('click', '.remove_image_button', function (event) {
                    jQuery('#multi_vendor_thumbnail img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
                    jQuery('#multi_vendor_thumbnail_id').val('');
                    jQuery('.remove_image_button').hide();
                    return false;
                });

            </script>
            <div class="clear"></div>
        </div>
        <?php
    }

    public static function edit_category_fields($term, $taxonomy) {
        global $woocommerce;
        $getpaypaladdress = get_woocommerce_term_meta($term->term_id, 'mv_paypal_email', true);
        $getcommissionrate = get_woocommerce_term_meta($term->term_id, 'fp_mv_commissions_rate', true);
        $getvendoradmins = unserialize(get_woocommerce_term_meta($term->term_id, 'fp_mv_vendor_admins', true));
        $getpaymentoptions = get_woocommerce_term_meta($term->term_id, 'mv_payment_option', true);
        $getcustompayment = get_woocommerce_term_meta($term->term_id, 'mv_custom_payment', true);
        $image = '';
        $thumbnail_id = absint(get_woocommerce_term_meta($term->term_id, 'thumbnail_id', true));
        if ($thumbnail_id)
            $image = wp_get_attachment_thumb_url($thumbnail_id);
        else
            $image = wc_placeholder_img_src();
        ?>

        <?php
        if (isset($_GET['taxonomy'])) {
            if ($_GET['taxonomy'] == 'multi_vendor') {
                ?>
                <script type="text/javascript">

                    // jQuery('#fp_mv_vendor_admins').chosen();
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                        jQuery(function () {
                            // Ajax Chosen Product Selectors
                            jQuery("select.fp_mv_vendor_admins").ajaxChosen({
                                method: 'GET',
                                url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                dataType: 'json',
                                afterTypeDelay: 100,
                                data: {
                                    action: 'woocommerce_json_search_customers',
                                    security: '<?php echo wp_create_nonce("search-customers"); ?>'
                                }
                            }, function (data) {
                                var terms = {};

                                jQuery.each(data, function (i, val) {
                                    terms[i] = val;
                                });
                                return terms;
                            });
                        });
                <?php } ?>


                </script>
                <script type="text/javascript">

                    jQuery(document).ready(function () {
                        var initvalue = jQuery('.mv_payment_option').val();
                        if (initvalue === '1') {
                            jQuery('.mv_paypal_email').parent().parent().css('display', 'table-row');
                            jQuery('.mv_custom_payment').parent().parent().css('display', 'none');
                        } else {
                            jQuery('.mv_paypal_email').parent().parent().css('display', 'none');
                            jQuery('.mv_custom_payment').parent().parent().css('display', 'table-row');
                        }
                        jQuery('.mv_payment_option').change(function () {
                            var thisvalue = jQuery(this).val();
                            if (thisvalue === '1') {
                                jQuery('.mv_paypal_email').parent().parent().css('display', 'table-row');
                                jQuery('.mv_custom_payment').parent().parent().css('display', 'none');
                            } else {
                                jQuery('.mv_paypal_email').parent().parent().css('display', 'none');
                                jQuery('.mv_custom_payment').parent().parent().css('display', 'table-row');
                            }
                        });
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                            jQuery('#fp_mv_vendor_admins').chosen();
                <?php } ?>


                    });</script>
                <style type="text/css">
                    .chosen-container-multi .chosen-choices li.search-field input[type="text"] {
                        height:25px !important;
                    }
                </style>
                <?php
            }
        }
        ?>
        <tr class="form-field">
            <th scope="row">
                <label for="fp_mv_commissions_rate"><?php _e('Vendor Commission in Percentage(%)', 'multivendor'); ?></label>
            </th>
            <td>
                <input type="text" name="fp_mv_commissions_rate" value="<?php echo $getcommissionrate; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row">
                <label for="fp_mv_vendor_admins"><?php _e('Vendor Admins', 'multivendor'); ?></label>
            </th>
            <td>
                <?php //var_dump($getvendoradmins); ?>
                <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                    <select data-placeholder="Select Vendor Admins" style="width:90%;" name="fp_mv_vendor_admins[]" class="fp_mv_vendor_admins" id="fp_mv_vendor_admins" multiple="multiple">
                        <?php
                        if (is_array($getvendoradmins)) {
                            foreach ($getvendoradmins as $eachvendor) {
                                $user = get_user_by('id', absint($eachvendor));
                                echo '<option value="' . absint($user->ID) . '" ';
                                selected(1, 1);
                                echo '>' . esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email) . ')</option>';
                            }
                        }
                        ?>
                    </select>
                <?php } else { ?>
                    <input type="hidden" class="wc-customer-search" style="width: 100%;" id="fp_mv_vendor_admins" name="fp_mv_vendor_admins" data-placeholder="<?php _e('Search for members&hellip;', 'multivendor'); ?>" data-action="woocommerce_json_search_customers" data-multiple="true" data-selected="<?php
                    $json_ids = array();
                    if ($getvendoradmins != "") {
                        $list_of_users = $getvendoradmins;
                        if (!is_array($list_of_users)) {
                            $userids = array_filter(array_map('absint', (array) explode(',', $list_of_users)));
                        } else {
                            $userids = $list_of_users;
                        }
                        foreach ($userids as $userid) {
                            $user = get_user_by('id', $userid);
                            $json_ids[$user->ID] = esc_html($user->display_name) . ' (#' . absint($user->ID) . ' &ndash; ' . esc_html($user->user_email);
                        } echo esc_attr(json_encode($json_ids));
                    }
                    ?>" value="<?php echo implode(',', array_keys($json_ids)); ?>" />
                    <script type="text/javascript">
                        jQuery(function () {
                            jQuery('body').trigger('wc-enhanced-select-init');
                        });

                    </script>
                <?php } ?>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row">
                <label for="mv_payment_option"><?php _e('Payment Option', 'multivendor'); ?></label>
            </th>
            <td>
                <?php
                $selectedvalues = $getpaymentoptions == '1' ? 'selected=selected' : '';
                $getselectedvalues = $getpaymentoptions == '2' ? 'selected=selected' : '';
                ?>
                <select name="mv_payment_option" class="mv_payment_option" id="mv_payment_option">
                    <option value="1" <?php echo $selectedvalues; ?>><?php _e('Paypal Address', 'multivendor'); ?></option>
                    <option value="2" <?php echo $getselectedvalues; ?>><?php _e('Custom Payment', 'multivendor'); ?></option>
                </select>
            </td>
        </tr>

        <tr class="form-field">
            <th scope="row">
                <label for="mv_paypal_email"><?php _e('Vendor PayPal Email Address', 'multivendor'); ?></label>
            </th>
            <td>
                <input type="text" class="mv_paypal_email" name ="mv_paypal_email" value="<?php echo $getpaypaladdress; ?>"/>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row">
                <label for="mv_custom_payment"><?php _e('Custom Payment', 'multivendor'); ?></label>
            </th>
            <td>
                <textarea name="mv_custom_payment"  class="mv_custom_payment" rows="3" cols="30"><?php echo $getcustompayment; ?></textarea>
            </td>
        </tr>
        <tr class="form-field">
            <th scope="row" valign="top"><label><?php _e('Thumbnail', 'multivendor'); ?></label></th>
            <td>
                <div id="multi_vendor_thumbnail" style="float:left;margin-right:10px;"><img src="<?php echo $image; ?>" width="60px" height="60px" /></div>
                <div style="line-height:60px;">
                    <input type="hidden" id="multi_vendor_thumbnail_id" name="multi_vendor_thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
                    <button type="submit" class="upload_image_button button"><?php _e('Upload/Add image', 'woocommerce'); ?></button>
                    <button type="submit" class="remove_image_button button"><?php _e('Remove image', 'woocommerce'); ?></button>
                </div>
                <script type="text/javascript">

                    // Uploading files
                    var file_frame;

                    jQuery(document).on('click', '.upload_image_button', function (event) {

                        event.preventDefault();

                        // If the media frame already exists, reopen it.
                        if (file_frame) {
                            file_frame.open();
                            return;
                        }

                        // Create the media frame.
                        file_frame = wp.media.frames.downloadable_file = wp.media({
                            title: '<?php _e('Choose an image', 'woocommerce'); ?>',
                            button: {
                                text: '<?php _e('Use image', 'woocommerce'); ?>',
                            },
                            multiple: false
                        });

                        // When an image is selected, run a callback.
                        file_frame.on('select', function () {
                            attachment = file_frame.state().get('selection').first().toJSON();

                            jQuery('#multi_vendor_thumbnail_id').val(attachment.id);
                            jQuery('#multi_vendor_thumbnail img').attr('src', attachment.url);
                            jQuery('.remove_image_button').show();
                        });

                        // Finally, open the modal.
                        file_frame.open();
                    });

                    jQuery(document).on('click', '.remove_image_button', function (event) {
                        jQuery('#multi_vendor_thumbnail img').attr('src', '<?php echo wc_placeholder_img_src(); ?>');
                        jQuery('#multi_vendor_thumbnail_id').val('');
                        jQuery('.remove_image_button').hide();
                        return false;
                    });

                </script>
                <div class="clear"></div>
            </td>
        </tr>
        <?php
    }

    public static function save_category_fields($term_id, $tt_id, $taxonomy) {
        if (isset($_POST['mv_paypal_email']))
            update_woocommerce_term_meta($term_id, 'mv_paypal_email', $_POST['mv_paypal_email']);

        if (isset($_POST['fp_mv_commissions_rate']))
            update_woocommerce_term_meta($term_id, 'fp_mv_commissions_rate', $_POST['fp_mv_commissions_rate']);

        if (isset($_POST['mv_payment_option']))
            update_woocommerce_term_meta($term_id, 'mv_payment_option', $_POST['mv_payment_option']);

        if (isset($_POST['mv_custom_payment']))
            update_woocommerce_term_meta($term_id, 'mv_custom_payment', $_POST['mv_custom_payment']);

        if (isset($_POST['multi_vendor_thumbnail_id'])) {
            update_woocommerce_term_meta($term_id, 'thumbnail_id', absint($_POST['multi_vendor_thumbnail_id']));
        }

        if (isset($_POST['fp_mv_vendor_admins'])) {
            // var_dump($_POST['fp_mv_vendor_admins']);
            if (!is_array($_POST['fp_mv_vendor_admins'])) {
                $_POST['fp_mv_vendor_admins'] = explode(',', $_POST['fp_mv_vendor_admins']);
            } else {
                $_POST['fp_mv_vendor_admins'] = $_POST['fp_mv_vendor_admins'];
            }
            $updatedata = $_POST['fp_mv_vendor_admins'] == '' ? $_POST['fp_mv_vendor_admins'] : serialize($_POST['fp_mv_vendor_admins']);
            update_woocommerce_term_meta($term_id, 'fp_mv_vendor_admins', $updatedata);


            if (isset($_POST['fp_mv_vendor_admins'])) {

                foreach ($_POST['fp_mv_vendor_admins'] as $value) {
                    $vendoruser = new WP_User($value);
                    if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {
                        foreach ($vendoruser->roles as $role) {
                            add_user_meta($vendoruser->ID, 'multivendor_backuprole', $role);
                               if (get_option('fp_multi_vendor_exception_role_control') != '') {
                                                foreach (get_option('fp_multi_vendor_exception_role_control') as $values){
                                                    $exceptroles1[] = $values;
                                                }
//                                                var_dump($exceptroles1);
                                                $exceptroles2 = array('administrator', 'shop_manager');
                                                $exceptroles3 = array_merge($exceptroles2,$exceptroles1);
                                                $exceptroles = $exceptroles3;
                                             }
                                             else{
                                                $exceptroles = array('administrator', 'shop_manager');
                                             }
                            if (!in_array($role, $exceptroles)) {
                                $vendoruser->remove_role($role);
                                $vendoruser->add_role('multi_vendor');
                            }
                        }
                    }
                }
            }



            foreach ($_POST['fp_mv_vendor_admins'] as $user_id) {
                //application approval
                if (get_option('fpmv_vendor_app_approval_email_enable_admin') == 'yes') {
                    $status = 'Approved';
                    $getusernamebyid = get_user_by('id', $user_id);
                    $newvendorname = $getusernamebyid->user_login;
                    $newvendorapplication = $_POST['name'];
                    include 'application_status_mail_admin.php';
                }
                if (get_woocommerce_term_meta($term_id, 'mv_app_created', true) != 'yes') {
                    if (get_option('fpmv_vendor_app_approval_email_enable') == 'yes') {
                        $mail_list = $user_id;
                        $status = 'Approved';
                        include 'application_status_mail.php';
                    }

                    update_woocommerce_term_meta($term_id, 'mv_app_created', 'yes');
                }
            }
        } else {
            if (empty($_POST['fp_mv_vendor_admins'])) {
                $maindata = '';
                $updatedata = serialize($maindata);
                update_woocommerce_term_meta($term_id, 'fp_mv_vendor_admins', $updatedata);
            }
        }


        delete_transient('wc_term_counts');
    }

    public static function delete_term_fields($term_id, $tt_id, $taxonomy) {

        $current_vendor_admins = unserialize(get_woocommerce_term_meta($term_id, 'fp_mv_vendor_admins', true));

        if (!is_array($current_vendor_admins)) {
            $current_vendor_admins = explode(',', $current_vendor_admins);
        } else {
            $current_vendor_admins = $current_vendor_admins;
        }

        if (!empty($current_vendor_admins)) {

            foreach ($current_vendor_admins as $user_id) {
                //application delete
                if (get_option('fpmv_vendor_app_deleted_email_enable') == 'yes') {
                    $mail_list = $user_id;
                    $status = 'Deleted';
                    include 'application_status_mail.php';
                }

                if (get_option('fpmv_vendor_app_deleted_email_enable_admin') == 'yes') {
                    $status = 'Deleted';
                    $getusernamebyid = get_user_by('id', $user_id);
                    $newvendorname = $getusernamebyid->user_login;
                    $newvendorapplication = get_woocommerce_term_meta($term_id, 'name', true);
                    include 'application_status_mail_admin.php';
                }
            }
        }


        if (!empty($current_vendor_admins)) {
            foreach ($current_vendor_admins as $vendoradmin) {
                $vendoruser = new WP_User($vendoradmin);
                $getmytaxonomy = array('multi_vendor');
                $arguments = array(
                    'hide_empty' => false,
                );
                $termsid = array();
                $includetermsid = array();
                $getallvendors = get_terms($getmytaxonomy, $arguments);

                foreach ($getallvendors as $eachvendor) {
                    $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                    if (!is_array($vendoradminids)) {
                        $vendoradminids = explode(',', $vendoradminids);
                    } else {
                        $vendoradminids = $vendoradminids;
                    }
                    if (!in_array($vendoruser->ID, (array) $vendoradminids)) {
                        $termsid[] = $eachvendor->term_id;
                    }
                    if (in_array($vendoruser->ID, (array) $vendoradminids)) {
                        $includetermsid[] = $eachvendor->term_id;
                    }
                }

                $countofuserwithvendor = count($includetermsid);
                if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {
                    foreach ($vendoruser->roles as $role) {
                        $backuprole = get_user_meta($vendoruser->ID, 'multivendor_backuprole', true);
                        $exceptroles = array('multi_vendor');
                        if (in_array($role, $exceptroles)) {
                            if ($countofuserwithvendor <= 0) {
                                $vendoruser->remove_role($role);
                                $vendoruser->add_role($backuprole);
                            }
                        }
                    }
                }
            }
        }
    }

    public static function remove_parent_selection() {
        if ('multi_vendor' != $_GET['taxonomy'])
            return;

        $parent = 'parent()';
        if (isset($_GET['action']))
            $parent = 'parent().parent()';
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($)
            {
                $('label[for=parent]').<?php echo $parent; ?>.remove();
            });
        </script>
        <?php
    }

}

new FPMultiVendorCustomCategory();
