<?php

/* Single Product Page Tab Settings */

class FPMultiVendorSingleProductTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_singleproduct_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_singleproduct', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_singleproduct', array($this, 'multivendor_update_values_settings'));
    }

    public static function multivendor_singleproduct_tab($settings_tabs) {
        $settings_tabs['multivendor_singleproduct'] = __('Single Product', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_singleproduct_admin_settings() {
        global $woocommerce;

        return apply_filters('woocommerce_multivendor_singleproduct', array(
            array(
                'name' => __('Single Product Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_single_product_settings'
            ),
            array(
                'name' => __('Enable Seller Info Tab in Single Product Page', 'multivendor'),
                'desc' => __('Enable Seller Info Tab in Single Product Page', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_enable_seller_info_tab_in_single_product_page',
                'css' => '',
                'std' => 'yes',
                'type' => 'checkbox',
                'newids' => 'fpmv_enable_seller_info_tab_in_single_product_page',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Seller Info Tab Name Customization', 'multivendor'),
                'desc' => __('Customize Seller Info Tab Name in Single Product Page', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_customize_seller_info_tab_name_single_product_page',
                'css' => '',
                'std' => 'Seller Info',
                'type' => 'text',
                'newids' => 'fpmv_customize_seller_info_tab_name_single_product_page',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_single_product_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorSingleProductTab::multivendor_singleproduct_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorSingleProductTab::multivendor_singleproduct_admin_settings());
        //FPMultiVendor::multi_vendor_create_custom_role_wordpress();
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorSingleProductTab::multivendor_singleproduct_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorSingleProductTab();
