<?php

class MVCustomOrderStatusCreation {

    public function __construct() {
        add_action('init', array($this, 'register_custom_order_status'));
        add_filter('wc_order_statuses', array($this, 'create_custom_order_status'));
        add_action('init', array($this, 'MV_backwardcheck_order_statuses'));
        add_action('wp_print_scripts', array($this, 'add_shipped_order_status_icon'));
        //add_filter('get_terms_args', array($this, 'restrict_order_status_visibility_for_multi_vendor'), 10, 2);
    }

    public static function MV_backwardcheck_order_statuses() {
        if (!function_exists('wc_get_order_statuses')) {
            wp_insert_term('shipped', 'shop_order_status');
            wp_insert_term('awaiting_shipping', 'shop_order_status');
        }
    }

    public static function register_custom_order_status() {
// if (function_exists('wc_get_order_statuses')) {
        register_post_status('wc-shipped', array(
            'label' => __('Shipped', 'multivendor'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Shipped <span class="count">(%s)</span>', 'Shipped <span class="count">(%s)</span>')
        ));
        register_post_status('wc-awaiting_shipping', array(
            'label' => __('Awaiting Shipment', 'multivendor'),
            'public' => true,
            'exclude_from_search' => false,
            'show_in_admin_all_list' => true,
            'show_in_admin_status_list' => true,
            'label_count' => _n_noop('Awaiting Shipment <span class="count">(%s)</span>', 'Awaiting Shipment <span class="count">(%s)</span>')
        ));
    }

    public static function create_custom_order_status($order_statuses) {

//Shipping
//if (function_exists('wc_get_order_statuses')) {
        $new_order_statuses = array();
// add new order status after processing
        foreach ($order_statuses as $key => $status) {
            $new_order_statuses[$key] = $status;
            if ('wc-processing' === $key) {
//                $new_order_statuses['wc_awaiting_shipping'] = __('Awaiting Shipment', 'multivendor');
                $new_order_statuses['wc-shipped'] = __('Shipped', 'multivendor');
            }
            if ('wc-on-hold' === $key) {
                $new_order_statuses['wc-awaiting_shipping'] = __('Awaiting Shipment', 'multivendor');
            }
        }

        return $new_order_statuses;
    }

    public static function add_shipped_order_status_icon() {
        if (!is_admin()) {
            return;
        }
        ?> <style>
            /* Add custom status order icons */
            .column-order_status mark.shipped
            {
                background: #3cc101;
                border-radius: 100px;
            }
            .column-order_status mark.awaiting_shipping{
                background: #cc4add;
                border-radius: 100px;
            }
        </style> <?php

    }

}

new MVCustomOrderStatusCreation();
