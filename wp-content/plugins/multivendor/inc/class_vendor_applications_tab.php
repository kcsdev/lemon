<?php
/* Vendor Application List Tab Settings */

class FPMultiVendorApplicationsTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_applications_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_applications', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_applications', array($this, 'multivendor_update_values_settings'));
        add_action('admin_head', array($this, 'add_hide_menu_below_woocommerce'));
        add_action('woocommerce_admin_field_mv_vendor_applications_list', array($this, 'multivendor_list_overall_applications'));
        add_action('woocommerce_admin_field_mv_vendor_applications_edit_list', array($this, 'multivendor_applications_list_table'));
    }

    public static function multivendor_applications_tab($settings_tabs) {
        $settings_tabs['multivendor_applications'] = __('Vendor Applications', 'multivendor');
        return $settings_tabs;
    }

    public static function add_hide_menu_below_woocommerce() {
        add_submenu_page('', __('Edit Vendor', 'multivendor'), __('Edit Vendor Master List', 'multivendor'), 'manage_options', 'multivendor_menu', array('FPMultiVendorApplicationsTab', 'multivendor_applications_list_table'));
    }

    public static function multivendor_applications_list_table($item) {
        global $wpdb;

        $table_name = $wpdb->prefix . 'multi_vendor_submit_application';
        $message = '';
        $notice = '';
        $default = array(
            'id' => 0,
            'setvendorname' => '',
            'setvendordescription' => '',
            'setvendoradmins' => '',
            'setmessagetoreviewer' => '',
            'setvendorcustompayment' => '',
            'setvendorpaymentoption' => '',
            'setvendorcommission' => '',
            'getmessagefromreviewer' => '',
            'setvendorpaypalemail' => '',
            'status' => '',
        );

        if (isset($_REQUEST['nonce'])) {
            if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {

                //  var_dump($_REQUEST);
                // combine our default item with request params
                $item = shortcode_atts($default, $_REQUEST);
                $item_valid = FPMultiVendorCommissionsTab::multi_vendor_validation($item);
                if ($item_valid === true) {
                    if ($item['id'] == 0) {
                        $result = $wpdb->insert($table_name, $item);
                        $item['id'] = $wpdb->insert_id;
                        //send_mail_admin::send_admin_vendor_name($item);
                        if ($result) {
                            $message = __('Item was successfully saved');
                        } else {
                            $notice = __('There was an error while saving item');
                        }
                    } else {
                        $result = $wpdb->update($table_name, $item, array('id' => $item['id']));



                        if ($result) {
                            $getitemid = $item['id'];
                            $getstatus = $item['status'];
                            if ($item['status'] == 'Approved') {
                                $getresults = $wpdb->get_results("SELECT * FROM $table_name WHERE setcreatedvendorid='0' and id= $getitemid", ARRAY_A);
                                foreach ($getresults as $eachresults) {
                                    $vendorname = $eachresults['setvendorname'];
                                    $vendordescription = $eachresults['setvendordescription'];
                                    $vendorslugname = strtolower(str_replace(' ', '-', $eachresults['setvendorname']));
                                    $testingterm = wp_insert_term(
                                            "$vendorname", // the term
                                            'multi_vendor', // the taxonomy
                                            array(
                                        'description' => $vendordescription,
                                        'slug' => $vendorslugname,
                                    ));

                                    $vendoruser = new WP_User($eachresults['setvendoradmins']);
                                    if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                                        foreach ($vendoruser->roles as $role) {
                                            add_user_meta($vendoruser->ID, 'multivendor_backuprole', $role);
                                            if (get_option('fp_multi_vendor_exception_role_control') != '') {
                                                foreach (get_option('fp_multi_vendor_exception_role_control') as $values){
                                                    $exceptroles1[] = $values;
                                                }
//                                                var_dump($exceptroles1);
                                                $exceptroles2 = array('administrator', 'shop_manager');
                                                $exceptroles3 = array_merge($exceptroles2,$exceptroles1);
                                                $exceptroles = $exceptroles3;
                                             }
                                             else{
                                                $exceptroles = array('administrator', 'shop_manager');
                                             }
                                            if (!in_array($role, $exceptroles)) {
                                                $vendoruser->remove_role($role);
                                                $vendoruser->add_role('multi_vendor');
                                            }
                                        }
                                    }

                                    //application approval
                                    if (get_option('fpmv_vendor_app_approval_email_enable') == 'yes') {
                                        $mail_list = $eachresults['setvendoradmins'];
                                        $status = 'Approved';


                                        include 'application_status_mail.php';
                                    }
                                    if (get_option('fpmv_vendor_app_approval_email_enable_admin') == 'yes') {


                                        $status = 'Approved';
                                        //send_mail_admin::send_admin_vendor_name();
                                        $getusernamebyid = get_user_by('id', $eachresults['setvendoradmins']);
                                        $newvendorname = $getusernamebyid->user_login;
                                        $newvendorapplication = $eachresults['setvendorname'];
                                        include 'application_status_mail_admin.php';
                                    }


                                    update_woocommerce_term_meta($testingterm['term_id'], 'thumbnail_id', $eachresults['vendorthumbnail']);

                                    update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_vendor_admins', serialize(array($eachresults['setvendoradmins'])));
                                    update_woocommerce_term_meta($testingterm['term_id'], 'fp_mv_commissions_rate', $eachresults['setvendorcommission']);
                                    update_woocommerce_term_meta($testingterm['term_id'], 'mv_paypal_email', $eachresults['setvendorpaypalemail']);
                                    update_woocommerce_term_meta($testingterm['term_id'], 'mv_payment_option', $eachresults['setvendorpaymentoption']);
                                    update_woocommerce_term_meta($testingterm['term_id'], 'mv_custom_payment', $eachresults['setvendorcustompayment']);
                                    $wpdb->update($table_name, array('setcreatedvendorid' => $testingterm['term_id']), array('id' => $getitemid));
                                }
                            } elseif ($item['status'] == 'Rejected') {
                                $checkresults = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$getitemid", ARRAY_A);
                                foreach ($checkresults as $eachvalues) {




                                    $vendoruser = new WP_User($eachvalues['setvendoradmins']);

                                    $getmytaxonomy = array('multi_vendor');
                                    $arguments = array(
                                        'hide_empty' => false,
                                    );
                                    $termsid = array();
                                    $includetermsid = array();
                                    $getallvendors = get_terms($getmytaxonomy, $arguments);
                                    foreach ($getallvendors as $eachvendor) {
                                        $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
//                                         var_dump($vendoradminids);

                                        if (!in_array($vendoruser->ID, (array) $vendoradminids)) {
                                            $termsid[] = $eachvendor->term_id;
                                        }

                                        if (in_array($vendoruser->ID, (array) $vendoradminids)) {
                                            $includetermsid[] = $eachvendor->term_id;
                                        }
                                    }


                                    $countofuserwithvendor = count($includetermsid);

                                    if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {

                                        foreach ($vendoruser->roles as $role) {
                                            $backuprole = get_user_meta($vendoruser->ID, 'multivendor_backuprole', true);
                                            $exceptroles = array('multi_vendor');
                                            if (in_array($role, $exceptroles)) {
                                                if ($countofuserwithvendor <= 0) {
                                                    $vendoruser->remove_role($role);
                                                    $vendoruser->add_role($backuprole);
                                                }
                                            }
                                        }
                                    }

                                    //application rejection
                                    if (get_option('fpmv_vendor_app_rejection_email_enable') == 'yes') {
                                        $mail_list = $eachvalues['setvendoradmins'];
                                        $status = 'Rejected';
                                        include 'application_status_mail.php';
                                    }
                                    if (get_option('fpmv_vendor_app_rejection_email_enable_admin') == 'yes') {

                                        $status = 'Rejected';
                                        // send_mail_admin::send_admin_vendor_name();
                                        $getusernamebyid = get_user_by('id', $eachvalues['setvendoradmins']);
                                        $newvendorname = $getusernamebyid->user_login;
                                        $newvendorapplication = $eachvalues['setvendorname'];
                                        include 'application_status_mail_admin.php';
                                    }


                                    if ($eachvalues['vendorthumbnail'] != '') {
                                        wp_delete_attachment($eachvalues['vendorthumbnail']);
                                    }
                                    wp_delete_term($eachvalues['setcreatedvendorid'], 'multi_vendor');

                                    $wpdb->update($table_name, array('setcreatedvendorid' => '0'), array('id' => $getitemid));
                                }
                            } else {
                                $checkresults = $wpdb->get_results("SELECT * FROM $table_name WHERE id=$getitemid", ARRAY_A);
                                foreach ($checkresults as $eachvalues) {



                                    $vendoruser = new WP_User($eachvalues['setvendoradmins']);

                                    $getmytaxonomy = array('multi_vendor');
                                    $arguments = array(
                                        'hide_empty' => false,
                                    );
                                    $termsid = array();
                                    $includetermsid = array();
                                    $getallvendors = get_terms($getmytaxonomy, $arguments);
                                    foreach ($getallvendors as $eachvendor) {
                                        $vendoradminids = unserialize(get_woocommerce_term_meta($eachvendor->term_id, 'fp_mv_vendor_admins', true));
                                        // var_dump($vendoradminids);

                                        if (!in_array($vendoruser->ID, (array) $vendoradminids)) {
                                            $termsid[] = $eachvendor->term_id;
                                        }

                                        if (in_array($vendoruser->ID, (array) $vendoradminids)) {
                                            $includetermsid[] = $eachvendor->term_id;
                                        }
                                    }


                                    $countofuserwithvendor = count($includetermsid);

                                    if (!empty($vendoruser->roles) && is_array($vendoruser->roles)) {
                                        foreach ($vendoruser->roles as $role) {
                                            $backuprole = get_user_meta($vendoruser->ID, 'multivendor_backuprole', true);
                                            $exceptroles = array('multi_vendor');
                                            if (in_array($role, $exceptroles)) {
                                                if ($countofuserwithvendor <= 0) {
                                                    $vendoruser->remove_role($role);
                                                    $vendoruser->add_role($backuprole);
                                                }
                                            }
                                        }
                                    }

                                    //application Pending
                                    if (get_option('fpmv_vendor_app_pending_email_enable_admin') == 'yes') {
                                        $status = 'Pending';
                                        $getusernamebyid = get_user_by('id', $eachvalues['setvendoradmins']);
                                        $newvendorname = $getusernamebyid->user_login;
                                        $newvendorapplication = $eachvalues['setvendorname'];
                                        include 'application_status_mail_admin.php';
                                    }

                                    if (get_option('fpmv_vendor_app_pending_email_enable') == 'yes') {
                                        $mail_list = $eachvalues['setvendoradmins'];
                                        $status = 'Pending';

                                        include 'application_status_mail.php';
                                    }


                                    if ($eachvalues['vendorthumbnail'] != '') {
                                        wp_delete_attachment($eachvalues['vendorthumbnail']);
                                    }
                                    wp_delete_term($eachvalues['setcreatedvendorid'], 'multi_vendor');
                                    $wpdb->update($table_name, array('setcreatedvendorid' => '0'), array('id' => $getitemid));
                                }
                            }

                            $message = __('Item was successfully updated');
                        } else {
                            $notice = __('There was an error while updating item');
                        }
                    }
                } else {
                    // if $item_valid not true it contains error message(s)
                    $notice = $item_valid;
                }
            }
        } else {
            // if this is not post back we load item to edit or give new one to create
            $item = $default;
            if (isset($_REQUEST['multivendor_application_id'])) {
                $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['multivendor_application_id']), ARRAY_A);

                if (!$item) {
                    $item = $default;
                    $notice = __('Item not found');
                }
            }
        }
        ?>
        <?php
        if (isset($_REQUEST['multivendor_application_id'])) {
            //    var_dump($_REQUEST);
            ?>
            <style type="text/css">
                p.submit {
                    display:none;
                }
                #mainforms {
                    display:none;
                }
            </style>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    var currentvalue = jQuery('#setvendorpaymentoption').val();
                    if (currentvalue === '1') {
                        jQuery('.setvendorpaypalemail').parent().parent().css('display', 'table-row');
                        jQuery('.setvendorcustompayment').parent().parent().css('display', 'none');
                    } else {
                        jQuery('.setvendorcustompayment').parent().parent().css('display', 'table-row');
                        jQuery('.setvendorpaypalemail').parent().parent().css('display', 'none');
                    }
                    jQuery('#setvendorpaymentoption').change(function () {
                        var thisvalue = jQuery(this).val();
                        if (thisvalue === '1') {
                            jQuery('.setvendorpaypalemail').parent().parent().css('display', 'table-row');
                            jQuery('.setvendorcustompayment').parent().parent().css('display', 'none');
                        } else {
                            if (thisvalue === '2') {
                                jQuery('.setvendorpaypalemail').parent().parent().css('display', 'none');
                                jQuery('.setvendorcustompayment').parent().parent().css('display', 'table-row');
                            }
                        }
                        // alert(jQuery(this).val());
                    });
                });
            </script>
            <div class="wrap">
                <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
                <h3>Edit Commission Status<a class="add-new-h2"
                                             href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=multivendor_menu&tab=multivendor_applications'); ?>"><?php _e('back to list') ?></a>
                </h3>
                <?php if (!empty($notice)): ?>
                    <div id="notice" class="error"><p><?php echo $notice ?></p></div>
                <?php endif; ?>
                <?php if (!empty($message)): ?>
                    <div id="message" class="updated"><p><?php echo $message ?></p></div>
                <?php endif; ?>
                <form id="form" method="POST">
                    <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__)) ?>"/>
                    <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>
                    <input type="hidden" name="setcreatedvendorid" value="<?php echo $item['setcreatedvendorid']; ?>"/>
                    <input type="hidden" value="<?php echo $item['setvendoradmins']; ?>" name="setvendoradmins"/>
                    <input type="hidden" value="<?php echo $item['setusernickname']; ?>" name="setusernickname"/>
                    <input type="hidden" value="<?php echo $item['vendorthumbnail']; ?>" name="vendorthumbnail"/>
                    <input type="hidden" value="<?php echo date("Y-m-d H:i:s"); ?>" name="date"/>
                    <div class="metabox-holder" id="poststuff">
                        <div id="post-body">
                            <div id="post-body-content">
                                <table class="form-table">
                                    <tbody>
                                        <tr>
                                            <th scope="row"><?php _e('Vendor Name', 'multivendor'); ?></th>
                                            <td>
                                                <input type="text" name="setvendorname" id="setvendorname" value="<?php echo $item['setvendorname']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('About Vendor', 'multivendor'); ?></th>
                                            <td>
                                                <textarea name="setvendordescription" rows="3" cols="30"><?php echo $item['setvendordescription']; ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Application Status', 'multivendor'); ?></th>
                                            <td>
                                                <?php
                                                $selected_pending = $item['status'] == 'Pending' ? "selected=selected" : '';
                                                $selected_approved = $item['status'] == 'Approved' ? "selected=selected" : '';
                                                $selected_rejected = $item['status'] == 'Rejected' ? "selected=selected" : '';
                                                ?>
                                                <select name = "status">
                                                    <option value = "Pending" <?php echo $selected_pending; ?>><?php _e('Pending', 'multivendor'); ?></option>
                                                    <option value = "Approved" <?php echo $selected_approved; ?>><?php _e('Approved', 'multivendor'); ?></option>
                                                    <option value = "Rejected" <?php echo $selected_rejected; ?>><?php _e('Rejected', 'multivendor'); ?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope='row'><?php _e('Message from Vendor', 'multivendor'); ?></th>
                                            <td>
                                                <textarea name='setmessagetoreviewer' rows='3' cols='30' id='setmessagetoreviewer' class='setmessagetoreviewer'><?php echo $item['setmessagetoreviewer']; ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Message to Vendor', 'multivendor'); ?></th>
                                            <td><textarea name="getmessagefromreviewer" rows="3" cols="30" id="getmessagefromreviewer" class="getmessagefromreviewer"><?php echo $item['getmessagefromreviewer']; ?></textarea> </td>
                                        </tr>

                                        <tr>
                                            <th scope="row"><?php _e('Commission Rate', 'multivendor'); ?></th>
                                            <td>
                                                <input type="text" name="setvendorcommission" class="setvendorcommission" value="<?php echo $item['setvendorcommission']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Vendor Payment Option', 'multivendor'); ?></th>
                                            <td>
                                                <!--<input type="text" name="setvendorpaymentoption" class="setvendorpaymentoption" value="<?php echo $item['setvendorpaymentoption']; ?>"/>-->
                                                <?php
                                                $selectedpaymentoption = $item['setvendorpaymentoption'] == '1' ? "selected=selected" : "";
                                                $mainselectedpaymentoption = $item['setvendorpaymentoption'] == '2' ? "selected=selected" : "";
                                                ?>
                                                <select id="setvendorpaymentoption" name="setvendorpaymentoption">
                                                    <option value="1" <?php echo $selectedpaymentoption; ?>><?php _e('Paypal Address', 'multivendor'); ?></option>
                                                    <option value="2" <?php echo $mainselectedpaymentoption; ?>><?php _e('Custom Payment', 'multivendor'); ?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Vendor Paypal Email', 'multivendor'); ?></th>
                                            <td>
                                                <input type="text" name="setvendorpaypalemail" class="setvendorpaypalemail" value="<?php echo $item['setvendorpaypalemail']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Vendor Custom Payment', 'multivendor'); ?></th>
                                            <td>
                                                <textarea name='setvendorcustompayment' rows='3' cols='30' id='setvendorcustompayment' class='setvendorcustompayment'><?php echo $item['setvendorcustompayment']; ?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <input type="submit" value="<?php _e('Save Changes', 'multivendor') ?>" id="submit" class="button-primary" name="submit">
                </form>

            </div>
        <?php } ?>

        <?php
    }

    public static function multivendor_add_chosen_to_applications() {
        if (isset($_GET['tab'])) {
            if ($_GET['tab'] == 'multivendor_applications') {
                ?>
                <script type="text/javascript">
                    jQuery(document).ready(function () {
                        //jQuery('#fp_multi_vendor_order_status_control').chosen();
                    });
                </script>
                <?php
            }
        }
    }

    public static function multivendor_list_overall_applications() {
        global $wpdb;
        global $current_section;
        global $current_tab;
        $getoptionarray = get_option('fpmultivendormasterlist');
        $testListTable = new FPMultiVendorApplicationsList();
        $testListTable->prepare_items();



        if (!isset($_REQUEST['multivendor_application_id'])) {
            $array_list = array();
            $message = '';
            if ('application_delete' === $testListTable->current_action()) {
                $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d'), count($_REQUEST['id'])) . '</p></div>';
            }
            echo $message;
            $testListTable->display();
            ?>

            <style type="text/css">
                p.submit {
                    display:none;
                }
                #mainforms {
                    display:none;
                }
            </style>

            <?php
        }
    }

    public static function multivendor_applications_admin_settings() {
        global $woocommerce;

        return apply_filters('woocommerce_multivendor_applications', array(
            array(
                'name' => __('Vendor Applications', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_status_settings'
            ),
            array(
                'type' => 'mv_vendor_applications_list',
            ),
            array(
                'type' => 'mv_vendor_applications_edit_list',
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_status_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorApplicationsTab::multivendor_applications_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorApplicationsTab::multivendor_applications_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {

        foreach (FPMultiVendorApplicationsTab::multivendor_applications_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

}

new FPMultiVendorApplicationsTab();
