<?php
/* Commissions Tab Settings */

class FPMultiVendorCommissionsTab {

    public function __construct() {
        add_action('admin_menu', array($this, 'add_hide_menu_below_woocommerce'));
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_commissions_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_commissions', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_commissions', array($this, 'multivendor_update_values_settings'));
        add_action('woocommerce_admin_field_mv_commission_list', array($this, 'list_all_vendors'));
        add_action('woocommerce_admin_field_mv_master_report', array($this, 'list_overall_commission_data'));
        add_action('woocommerce_admin_field_edit_master_report', array($this, 'multi_vendor_management'));
    }

    public static function multivendor_commissions_tab($settings_tabs) {
        $settings_tabs['multivendor_commissions'] = __('Commissions', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_commissions_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_commissions', array(
            array(
                'name' => __('Commissions Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_commissions_settings'
            ),
            array(
                'type' => 'mv_master_report',
            ),
            array(
                'type' => 'edit_master_report',
            ),
            array(
                'type' => 'mv_commission_list'
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_commissions_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorCommissionsTab::multivendor_commissions_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorCommissionsTab::multivendor_commissions_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {

        foreach (FPMultiVendorCommissionsTab::multivendor_commissions_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

    public static function multivendor_show_all_user_commissions_list() {
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>

        <?php
    }

    public static function list_all_vendors() {
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>
        <?php
    }

    public static function multi_vendor_validation($item) {
        $messages = array();

        if (empty($messages))
            return true;
        return implode('<br />', $messages);
    }

    public static function multi_vendor_management($item) {
        global $wpdb;
        global $woocommerce;

        $table_name = $wpdb->prefix . 'multi_vendor';
        $message = '';
        $notice = '';
        $default = array(
            'id' => 0,
            'orderid' => '',
            'productid' => '',
            'productname' => '',
            'qty' => '',
            'productprice' => '',
            'discountedproductprice' => '',
            'discountamount' => '',
            'shippingcost' => '',
            'vendorcommission' => '',
            'vendorcommissionrate' => '',
            'vendorname' => '',
            'vendorid' => '',
            'status' => '',
            'date' => '',
        );
        if (isset($_REQUEST['nonce'])) {
            if (wp_verify_nonce($_REQUEST['nonce'], basename(__FILE__))) {

                // combine our default item with request params
                $item = shortcode_atts($default, $_REQUEST);
                $item_valid = FPMultiVendorCommissionsTab::multi_vendor_validation($item);
                if ($item_valid === true) {
                    if ($item['id'] == 0) {
                        $item['productname'] = get_the_title($item['productid']);
                        $item['vendorname'] = get_term_by('id', $item['vendorid'], 'multi_vendor');
                        $item['vendorname'] = $item['vendorname']->name;
                        $item['date'] = date("Y-m-d H:i:s");
                        //var_dump($item);
                        $result = $wpdb->insert($table_name, $item);
                        $item['id'] = $wpdb->insert_id;
                        if ($result) {
                            $message = __('Item was successfully saved');
                        } else {
                            $notice = __('There was an error while saving item');
                        }
                    } else {
                        $item['productname'] = get_the_title($item['productid']);
                        $item['vendorname'] = get_term_by('id', $item['vendorid'], 'multi_vendor');
                        $item['vendorname'] = $item['vendorname']->name;
                        $item['date'] = date("Y-m-d H:i:s");
                        $result = $wpdb->update($table_name, $item, array('id' => $item['id']));
                        if ($result) {
                            $message = __('Item was successfully updated');
                        } else {
                            $notice = __('There was an error while updating item');
                        }
                    }
                } else {
                    // if $item_valid not true it contains error message(s)
                    $notice = $item_valid;
                }
            }
        } else {
            // if this is not post back we load item to edit or give new one to create
            $item = $default;
            if (isset($_REQUEST['multivendor_id'])) {
                $item = $wpdb->get_row($wpdb->prepare("SELECT * FROM $table_name WHERE id = %d", $_REQUEST['multivendor_id']), ARRAY_A);

                if (!$item) {
                    $item = $default;
                    $notice = __('Item not found');
                }
            }
        }
        ?>
        <?php
        if (isset($_REQUEST['tab'])) {
            if ($_REQUEST['tab'] == 'multivendor_commissions_edit') {
                //    var_dump($_REQUEST);
                ?>
                <style type="text/css">
                    p.submit {
                        display:none;
                    }
                    #mainforms {
                        display:none;
                    }
                </style>
                <div class="wrap">
                    <div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
                    <h3> <?php echo isset($_REQUEST['multivendor_id']) ? "Edit Commission" : "Add New Commission"; ?>  <a class="add-new-h2"
                                                                                                                     href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=multivendor_menu&tab=multivendor_commissions'); ?>"><?php _e('back to list') ?></a>
                    </h3>
                    <?php if (!empty($notice)): ?>
                        <div id="notice" class="error"><p><?php echo $notice ?></p></div>
                    <?php endif; ?>
                    <?php if (!empty($message)): ?>
                        <div id="message" class="updated"><p><?php echo $message ?></p></div>
                    <?php endif; ?>
                    <form id="form" method="POST">
                        <input type="hidden" name="nonce" value="<?php echo wp_create_nonce(basename(__FILE__)) ?>"/>
                        <input type="hidden" name="id" value="<?php echo $item['id'] ?>"/>
                        <input type="hidden" name="orderid" value="<?php echo $item['orderid']; ?>"/>
                        <input type="hidden" name="productname" value="<?php echo $item['productname']; ?>"/>
                        <!--
                        <input type="hidden" name="qty" value="<?php echo $item['qty']; ?>"/>
                        <input type="hidden" name="productprice" value="<?php echo $item['productprice']; ?>"/>
                        -->                 <input type="hidden" name="vendorname" value="<?php echo $item['vendorname']; ?>"/>
                        <!--                <input type="hidden" name="vendorcommissionrate" value="<?php echo $item['vendorcommissionrate']; ?>"/>
                                            <input type="hidden" name="discountedproductprice" value="<?php echo $item['discountedproductprice']; ?>"/>
                                            <input type="hidden" name="discountamount" value="<?php echo $item['discountamount']; ?>"/>-->
                        <input type="hidden" value="<?php echo $item['date']; ?>" name="date"/>
                        <div class="metabox-holder" id="poststuff">
                            <div id="post-body">
                                <div id="post-body-content">
                                    <table class="form-table">
                                        <tbody>
                                            <?php
//                                       var_dump($item);

                                            if ((float) $woocommerce->version > (float) ('2.2.0')) {
                                                ?>
                                                <tr valign="top">
                                                    <th class="titledesc" scope="row">
                                                        <label for="product_name"><?php _e('Product Name', 'multivendor'); ?></label>
                                                    </th>
                                                    <td class="forminp forminp-select">

                                                        <input type="hidden" class="wc-product-search" style="min-width:150px;max-width: 230px;" required="required" id="productid" name="productid" data-placeholder="<?php _e('Search for a product&hellip;', 'multivendor'); ?>" data-action="woocommerce_json_search_products_and_variations" data-multiple="false" data-selected="<?php
                                                        $product_id = $item['productid'];
                                                        if (isset($product_id)) {
                                                            $product = wc_get_product($product_id);
                                                            if (is_object($product)) {
                                                                $json_ids = $product->get_formatted_name();
                                                            }
                                                        }
                                                        echo esc_attr(str_replace('"', '', json_encode($json_ids)));
                                                        ?>" value="<?php echo $product_id; ?>" />
                                                    </td>
                                                </tr>
                                            <?php } else { ?>
                                                <tr valign="top">
                                                    <th class="titledesc" scope="row">
                                                        <label for="product_name"><?php _e('Product Name', 'multivendor'); ?></label>
                                                    </th>
                                                    <td class="forminp forminp-select">
                                                        <select multiple name="productid" style='min-width:150px;max-width: 230px;' required="required" id='productid' class="productid">
                                                            <?php
                                                            if ((array) $item['productid'] != "") {
                                                                $list_of_produts = (array) $item['productid'];
                                                                foreach ($list_of_produts as $rs_free_id) {
                                                                    echo '<option value="' . $rs_free_id . '" ';
                                                                    selected(1, 1);
                                                                    echo '>' . ' #' . $rs_free_id . ' &ndash; ' . get_the_title($rs_free_id);
                                                                }
                                                            } else {
                                                                ?>
                                                                <option value=""></option>
                                                                <?php
                                                            }
                                                            ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            <?php }
                                            ?>
                                            <?php if ((float) $woocommerce->version <= (float) ('2.2.0')) { ?>
                                            <script type="text/javascript">
                                                jQuery(function () {

                                                    // Ajax Chosen Product Selectors

                                                    jQuery("select.productid").ajaxChosen({
                                                        method: 'GET',
                                                        url: '<?php echo admin_url('admin-ajax.php'); ?>',
                                                        dataType: 'json',
                                                        afterTypeDelay: 100,
                                                        data: {
                                                            action: 'woocommerce_json_search_products_and_variations',
                                                            security: '<?php echo wp_create_nonce("search-products"); ?>'
                                                        }
                                                    }, function (data) {
                                                        var terms = {};

                                                        jQuery.each(data, function (i, val) {
                                                            terms[i] = val;
                                                        });
                                                        return terms;
                                                    });
                                                });
                                            </script>
                                        <?php } ?>
                                        <tr>
                                            <th scope="row"><?php _e('Product Quantity', 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" name="qty" id="qty" min="1" required="required" value="<?php echo $item['qty']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Product Price '.get_woocommerce_currency_symbol(), 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" step="0.01" min="0" name="productprice" id="productprice" required="required" value="<?php echo $item["productprice"]; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Discount Product Price '.get_woocommerce_currency_symbol(), 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" step="0.01" min="0" name="discountedproductprice" id="discountedproductprice" required="required" value="<?php echo $item['discountedproductprice']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Discount Coupon Value '.get_woocommerce_currency_symbol(), 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" min="0" step="0.01" name="discountamount" id="discountamount" required="required" value="<?php echo $item['discountamount']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Shipping Cost '.get_woocommerce_currency_symbol(), 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" step="0.01" min="0" name="shippingcost" id="shippingcost" required="required" value="<?php echo $item['shippingcost']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Vendor Commission Rate %', 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" step="0.01" min="0" name="vendorcommissionrate" id="vendorcommissionrate" required="required" value="<?php echo $item['vendorcommissionrate']; ?>"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Total Earned Commission '.get_woocommerce_currency_symbol(), 'multivendor'); ?></th>
                                            <td>
                                                <input type="number" step="0.01" min="0" name="vendorcommission" id="vendorcommission" required="required" value="<?php echo $item['vendorcommission']; ?>"/>
                                            </td>
                                        </tr>
                                        <?php
                                        $taxonomy = array('multi_vendor');
                                        $args = array(
                                            'hide_empty' => false,
                                        );
                                        ?>
                                        <tr>
                                            <th scope="row">
                                                <label><?php _e('Vendor Name', 'multivendor'); ?></label>
                                            </th>
                                            <td>
                                                <select name="vendorid" class="vendorid">;

                                                    <?php
                                                    foreach (get_terms($taxonomy, $args) as $eachvendor) {
                                                        ?>
                                                        <option value="<?php echo $eachvendor->term_id; ?>"<?php selected($eachvendor->term_id, isset($item['vendorid']) ? $item['vendorid'] : '') ?>><?php echo $eachvendor->name; ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th scope="row"><?php _e('Status'); ?></th>
                                            <td>
                                                <?php
                                                $selected_due = $item['status'] == 'Due' ? "selected=selected" : '';
                                                $selected_paid = $item['status'] == 'Paid' ? "selected=selected" : '';
                                                ?>
                                                <select name = "status">
                                                    <option value = "Due" <?php echo $selected_due; ?>><?php _e('Due', 'multivendor');
                                                ?></option>
                                                    <option value="Paid" <?php echo $selected_paid; ?>><?php _e('Paid', 'multivendor'); ?></option>
                                                </select>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <input type="submit" value="<?php _e('Save Changes', 'multivendor') ?>" id="submit" class="button-primary" name="submit">
                    </form>

                </div>
                <?php
            }
        }
        ?>

        <?php
    }

    public static function edit_individual_status_for_commission($item) {
        global $wpdb;
        $tablename = $wpdb->prefix . 'multi_vendor';
    }

    public static function add_hide_menu_below_woocommerce() {
        add_submenu_page('', __('Edit Vendor', 'multivendor'), __('Edit Vendor Master List', 'multivendor'), 'manage_options', 'multivendor_menu', array('FPMultiVendorCommissionsTab', 'multi_vendor_management'));
    }

    public static function list_overall_commission_data() {
        global $wpdb;
        global $current_section;
        global $current_tab;
        $getoptionarray = get_option('fpmultivendormasterlist');
        $testListTable = new FPMultiVendorListTable();
        $testListTable->prepare_items();
        ?><div class="wrap">

            <h3><a class="add-new-h2"
                   href="<?php echo get_admin_url(get_current_blog_id(), 'admin.php?page=multivendor_menu&tab=multivendor_commissions_edit'); ?>"><?php _e('Add New Commission') ?></a>
            </h3>
        </div>
        <?php
        if (!isset($_REQUEST['multivendor_id'])) {
            $array_list = array();
            $message = '';
            if ('delete' === $testListTable->current_action()) {
                $message = '<div class="updated below-h2" id="message"><p>' . sprintf(__('Items deleted: %d'), count($_REQUEST['id'])) . '</p></div>';
            }
            echo $message;
            $testListTable->display();
            // FPMultiVendorCommissionsTab::multi_vendor_management($item);
            ?>
            <!--            <ul class="subsubsub">
            <?php
            $args = array(
                'hide_empty' => FALSE,
            );
            $taxonomies = get_terms(array('multi_vendor'), $args);
            ?>
                            <li><a class="<?php echo $current_section == '' ? 'current' : ''; ?>" href="<?php echo admin_url('admin.php?page=multivendor_menu&tab=multivendor_commissions'); ?>" >Master List </a>|</li>
            <?php
            for ($i = 0; $i < count($taxonomies); $i++) {
                ?>
                                                                                                                                                                                                                        <li><a class="<?php echo $current_section == $taxonomies[$i]->term_id ? 'current' : ''; ?>" href="<?php echo admin_url('admin.php?page=multivendor_menu&tab=multivendor_commissions&section=' . $taxonomies[$i]->term_id) ?>"><?php echo $taxonomies[$i]->name; ?></a><?php echo $i == (count($taxonomies) - 1) ? '' : '|'; ?></li>
                <?php
            }
            ?>
                        </ul>-->
            <br>
            <br>
            <br>
            <br>
            <style type="text/css">
                p.submit {
                    display:none;
                }
                #mainforms {
                    display:none;
                }
            </style>


            <h3><?php _e('Total Commission Earned by each Vendor', 'multivendor'); ?></h3>
            <?php
            echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_admin_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_admin_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('.multivendor_admin_total').footable().bind('footable_filtering', function (e) {
                        var selected = jQuery('.filter-status').find(':selected').text();
                        if (selected && selected.length > 0) {
                            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                            e.clear = !e.filter;
                        }
                    });
                    jQuery('#multivendor_admin_change-page-sizes').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        jQuery('.footable').data('page-size', pageSize);
                        jQuery('.footable').trigger('footable_initialized');
                    });
                });</script>
            <table data-filter = "#multivendor_admin_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" class="multivendor_admin_total wp-list-table widefat fixed posts" style="max-width:600px;">
                <thead>
                    <tr>
                        <th data-toggle="true" data-sort-initial = "true">
                            <?php _e('Vendor Name', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Total Earned Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Due Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone">
                            <?php _e('Vendor Paid Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $taxonomy = array('multi_vendor');
                    $args = array(
                        'hide_empty' => false,
                    );
                    foreach (get_terms($taxonomy, $args) as $eachvendor) {
                        $multivendorduelist = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS myduecommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Due' and vendorid=$eachvendor->term_id", ARRAY_A
                        );
                        $multivendorpaidlist = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS mypaidcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Paid' and vendorid=$eachvendor->term_id", ARRAY_A
                        );
                        $totalearnedcommission = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS mytotalcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$eachvendor->term_id", ARRAY_A
                        );

                        // var_dump($multivendorduelist[0]['myduecommission']);
                        $paypalmultivendorduecommission = $multivendorduelist[0]['myduecommission'] != '' ? $multivendorduelist[0]['myduecommission'] : 0;
                        if ($paypalmultivendorduecommission != '0') {
                            $array_list[] = array(get_woocommerce_term_meta($eachvendor->term_id, 'mv_paypal_email', true), $paypalmultivendorduecommission, get_woocommerce_currency(), $eachvendor->term_id, get_option('fpmv_vendor_paypal_custom_notes'));
                        }
                        ?>

                        <tr data-value="<?php echo $eachvendor->term_id; ?>">
                            <td>
                                <?php echo $eachvendor->name; ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($totalearnedcommission[0]['mytotalcommission'])) {
                                    echo $totalearnedcommission[0]['mytotalcommission'];
                                } else {
                                    echo "0";
                                }
                                ?>
                            </td>

                            <td>
                                <?php
                                if (!empty($multivendorduelist[0]['myduecommission'])) {
                                    echo $multivendorduelist[0]['myduecommission'];
                                } else {
                                    echo "0";
                                }
                                $totalcommissions = get_woocommerce_term_meta($eachvendor->term_id, 'totalcommissions', true);
                                $totalcommissions == '' ? FPMultiVendor::get_woocommerce_formatted_price("0") : FPMultiVendor::get_woocommerce_formatted_price($totalcommissions);
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($multivendorpaidlist[0]['mypaidcommission'])) {
                                    echo $multivendorpaidlist[0]['mypaidcommission'];
                                } else {
                                    echo "0";
                                }
                                ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>
            </table>
            <div class="pagination"></div>
            <?php
        }
    }

}

new FPMultiVendorCommissionsTab();
