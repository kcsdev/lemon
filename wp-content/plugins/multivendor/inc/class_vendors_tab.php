<?php
/* Vendors Tab Settings */

class FPMultiVendorVendorsTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_vendors_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_vendors', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_vendors', array($this, 'multivendor_update_values_settings'));
        add_action('woocommerce_admin_field_additional_field', array($this, 'multivendor_add_additional_field_to_vendor_tab'));
    }

    public static function multivendor_vendors_tab($settings_tabs) {
        $settings_tabs['multivendor_vendors'] = __('Vendors', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_vendors_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_vendors', array(
            array(
                'name' => __('', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_vendors_settings'
            ),
            array(
                'type' => 'additional_field',
            ),
            array('type' => 'sectionend', 'id' => '_mv_title_vendors_settings'),
        ));
    }

    public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorVendorsTab::multivendor_vendors_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorVendorsTab::multivendor_vendors_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {

        foreach (FPMultiVendorVendorsTab::multivendor_vendors_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                if (get_option($settings['newids']) == FALSE) {
                    add_option($settings['newids'], $settings['std']);
                }
            }
        }
    }

    public static function multivendor_add_additional_field_to_vendor_tab() {
        global $wpdb;
        ?>
        <style type="text/css">
            p.submit {
                display:none;
            }
            #mainforms {
                display:none;
            }
        </style>

        <?php if (!isset($_GET['viewvendorid'])) { ?>

            <h3><?php _e('Total Commission Earned by each Vendor', 'multivendor'); ?></h3>
            <?php
            echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_vendor_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_vendor_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('.multivendor_vendor_total').footable().bind('footable_filtering', function (e) {
                        var selected = jQuery('.filter-status').find(':selected').text();
                        if (selected && selected.length > 0) {
                            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                            e.clear = !e.filter;
                        }
                    });
                    jQuery('#multivendor_vendor_change-page-sizes').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        jQuery('.footable').data('page-size', pageSize);
                        jQuery('.footable').trigger('footable_initialized');
                    });
                });</script>
            <table data-filter = "#multivendor_vendor_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next" class="multivendor_vendor_total wp-list-table widefat fixed posts" style="max-width:100%;">
                <thead>
                    <tr>
                        <th data-toggle="true" data-sort-initial = "true">
                            <?php _e('Vendor Name', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-sort-initial="true">
                            <?php _e('Vendor ID', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" >
                            <?php _e('Vendor Total Earned Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Due Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('Vendor Paid Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('View Log', 'multivendor'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $taxonomy = array('multi_vendor');
                    $args = array(
                        'hide_empty' => false,
                    );
                    foreach (get_terms($taxonomy, $args) as $eachvendor) {
                        $multivendorduelist = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS myduecommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='due' and vendorid=$eachvendor->term_id", ARRAY_A
                        );
                        $multivendorpaidlist = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS mypaidcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='paid' and vendorid=$eachvendor->term_id", ARRAY_A
                        );
                        $totalearnedcommission = $wpdb->get_results(
                                "SELECT sum(vendorcommission) AS mytotalcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$eachvendor->term_id", ARRAY_A
                        );

                        // var_dump($multivendorduelist[0]['myduecommission']);
                        $paypalmultivendorduecommission = $multivendorduelist[0]['myduecommission'] != '' ? $multivendorduelist[0]['myduecommission'] : 0;
                        if ($paypalmultivendorduecommission != '0') {
                            $array_list[] = array(get_woocommerce_term_meta($eachvendor->term_id, 'mv_paypal_email', true), $paypalmultivendorduecommission, get_woocommerce_currency(), $eachvendor->term_id, get_option('fpmv_vendor_paypal_custom_notes'));
                        }
                        ?>

                        <tr data-value="<?php echo $eachvendor->term_id; ?>">
                            <td>
                                <?php echo $eachvendor->name; ?>
                            </td>

                            <td>
                                <?php echo $eachvendor->term_id; ?>
                            </td>

                            <td>
                                <?php
                                if (!empty($totalearnedcommission[0]['mytotalcommission'])) {
                                    echo $totalearnedcommission[0]['mytotalcommission'];
                                } else {
                                    echo "0";
                                }
                                ?>
                            </td>

                            <td>
                                <?php
                                if (!empty($multivendorduelist[0]['myduecommission'])) {
                                    echo $multivendorduelist[0]['myduecommission'];
                                } else {
                                    echo "0";
                                }
                                $totalcommissions = get_woocommerce_term_meta($eachvendor->term_id, 'totalcommissions', true);
                                $totalcommissions == '' ? FPMultiVendor::get_woocommerce_formatted_price("0") : FPMultiVendor::get_woocommerce_formatted_price($totalcommissions);
                                ?>
                            </td>
                            <td>
                                <?php
                                if (!empty($multivendorpaidlist[0]['mypaidcommission'])) {
                                    echo $multivendorpaidlist[0]['mypaidcommission'];
                                } else {
                                    echo "0";
                                }
                                ?>
                            </td>
                            <td>
                                <a href="admin.php?page=multivendor_menu&tab=multivendor_vendors&viewvendorid=<?php echo $eachvendor->term_id; ?>"><?php _e('View Vendor Data', 'multivendor'); ?></a>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>

            </table>

            <div class="pagination pagination-centered"></div>

            <?php
        } else {
            ?>
            <h3><?php _e('Vendor Log', 'multivendor'); ?></h3>
            <?php
            echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_each_vendor_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_each_vendor_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('.multivendor_each_vendor_log').footable().bind('footable_filtering', function (e) {
                        var selected = jQuery('.filter-status').find(':selected').text();
                        if (selected && selected.length > 0) {
                            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                            e.clear = !e.filter;
                        }
                    });
                    jQuery('#multivendor_each_vendor_change-page-sizes').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        jQuery('.footable').data('page-size', pageSize);
                        jQuery('.footable').trigger('footable_initialized');
                    });
                });</script>
            <table data-filter = "#multivendor_each_vendor_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next"  class="multivendor_each_vendor_log wp-list-table widefat fixed posts" style="max-width:100%;">
                <thead>
                    <tr>
                        <th data-toggle="true" data-sort-initial = "true">
                            <?php _e('Order ID', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Product Name', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Product Price ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Discounted Product Price ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Discount Coupon Value ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Shipping Cost ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Commission Rate %', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Commission', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('Vendor Name', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('Status', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('Date', 'multivendor'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $vendorid = $_GET['viewvendorid'];
                    $getresults = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$vendorid", ARRAY_A);
                    foreach ($getresults as $result) {
                        ?>
                        <tr data-value="<?php echo $result['id']; ?>">
                            <td>
                                <?php echo $result['orderid']; ?>
                            </td>
                            <td>
                                <?php echo $result['productname']; ?>
                            </td>
                            <td>
                                <?php echo $result['productprice']; ?>
                            </td>
                            <td>
                                <?php echo $result['discountedproductprice']; ?>
                            </td>
                            <td>
                                <?php echo $result['discountamount']; ?>
                            </td>
                            <td>
                                <?php echo $result['shippingcost']; ?>
                            </td>
                            <td>
                                <?php echo $result['vendorcommissionrate']; ?>
                            </td>
                            <td>
                                <?php echo $result['vendorcommission']; ?>
                            </td>
                            <td>
                                <?php echo $result['vendorname']; ?>
                            </td>
                            <td>
                                <?php echo $result['status']; ?>
                            </td>
                            <td>
                                <?php echo $result['date']; ?>
                            </td>
                        </tr>
                        <?php
                    }
                    ?>
                </tbody>

            </table>
            <div class="pagination pagination-centered"></div>

            <h3><?php _e('Vendor Total Log', 'multivendor'); ?></h3>
            <?php
            echo '<p> ' . __('Search:', 'multivendor') . '<input id="multivendor_each_vendor_total_filters" type="text"/>  ' . __('Page Size:', 'multivendor') . '
                <select id="multivendor_each_vendor_total_change-page-sizes">
                <option value="5">5</option>
                <option value="10">10</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                </select></p>';
            ?>
            <script type="text/javascript">
                jQuery(document).ready(function () {
                    jQuery('.multivendor_total_each_vendor').footable().bind('footable_filtering', function (e) {
                        var selected = jQuery('.filter-status').find(':selected').text();
                        if (selected && selected.length > 0) {
                            e.filter += (e.filter && e.filter.length > 0) ? ' ' + selected : selected;
                            e.clear = !e.filter;
                        }
                    });
                    jQuery('#multivendor_each_vendor_total_change-page-sizes').change(function (e) {
                        e.preventDefault();
                        var pageSize = jQuery(this).val();
                        jQuery('.footable').data('page-size', pageSize);
                        jQuery('.footable').trigger('footable_initialized');
                    });
                });</script>
            <table  data-filter = "#multivendor_each_vendor_total_filters" data-page-size="5" data-page-previous-text = "prev" data-filter-text-only = "true" data-page-next-text = "next"  class="multivendor_total_each_vendor wp-list-table widefat fixed posts" style="max-width:600px;">
                <thead>
                    <tr>
                        <th data-toggle="true" data-sort-initial = "true">
                            <?php _e('Vendor Name', 'multivendor'); ?>
                        </th>
                        <th data-toggle="true">
                            <?php _e('Vendor Total Earned Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone">
                            <?php _e('Vendor Due Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                        <th data-toggle="true" data-hide="phone,tablet">
                            <?php _e('Vendor Paid Commission ' . get_woocommerce_currency_symbol(), 'multivendor'); ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $myvendorid = $_GET['viewvendorid'];
                    $multivendorduelist = $wpdb->get_results(
                            "SELECT sum(vendorcommission) AS myduecommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Due' and vendorid=$myvendorid", ARRAY_A
                    );
                    $multivendorpaidlist = $wpdb->get_results(
                            "SELECT sum(vendorcommission) AS mypaidcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE status='Paid' and vendorid=$myvendorid", ARRAY_A
                    );
                    $totalearnedcommission = $wpdb->get_results(
                            "SELECT sum(vendorcommission) AS mytotalcommission
	FROM " . $wpdb->prefix . "multi_vendor WHERE vendorid=$myvendorid", ARRAY_A
                    );
                    ?>

                    <tr data-value="<?php echo $myvendorid; ?>">
                        <td>
                            <?php
                            $vendordata = get_term_by('id', $myvendorid, 'multi_vendor');
                            echo $vendordata->name;
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($totalearnedcommission[0]['mytotalcommission'])) {
                                echo $totalearnedcommission[0]['mytotalcommission'];
                            } else {
                                echo "0";
                            }
                            ?>
                        </td>

                        <td>
                            <?php
                            if (!empty($multivendorduelist[0]['myduecommission'])) {
                                echo $multivendorduelist[0]['myduecommission'];
                            } else {
                                echo "0";
                            }
                            ?>
                        </td>
                        <td>
                            <?php
                            if (!empty($multivendorpaidlist[0]['mypaidcommission'])) {
                                echo $multivendorpaidlist[0]['mypaidcommission'];
                            } else {
                                echo "0";
                            }
                            ?>
                        </td>

                    </tr>

                </tbody>
            </table>

            <?php
        }
    }

}

new FPMultiVendorVendorsTab();
