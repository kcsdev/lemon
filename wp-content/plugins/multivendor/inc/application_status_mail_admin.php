<?php

//class send_mail_admin{
//
//public static function send_admin_vendor_name($item){
//
global $woocommerce;
global $wpdb;
$admin_mail = get_option('admin_email');
$user = get_user_by('email', $admin_mail);
//var_dump($user);

$user_wmpl_lang = get_user_meta(@$user->ID, 'mv_wpml_lang', true);
if (empty($user_wmpl_lang)) {
    $user_wmpl_lang = 'en';
}
$message = get_option('fpmv_vendor_application_email_message_admin');

//FPMultiVendorApplicationsTab::multivendor_applications_list_table($item);




$message = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_application_email_message_admin', $user_wmpl_lang, $message);
$subject = SocioMultiVendorWPMLSupport::fp_mv_get_wpml_text('fpmv_vendor_application_email_subject_admin', $user_wmpl_lang, get_option('fpmv_vendor_application_email_subject_admin'));
ob_start();
if (function_exists('wc_get_template')) {
    wc_get_template('emails/email-header.php', array('email_heading' => $subject));
} else {
    woocommerce_get_template('emails/email-header.php', array('email_heading' => $subject));
}
$message = str_replace('{application_status}', $status, $message);
$message = str_replace('{vendor_admin_name}', $newvendorname, $message);
$message = str_replace('{vendor_shop_name}', $newvendorapplication, $message);



echo $message;

if (function_exists('wc_get_template')) {
    wc_get_template('emails/email-footer.php');
} else {
    woocommerce_get_template('emails/email-footer.php');
}
$temp_message = ob_get_clean();

$headers = "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
$headers .= "From: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
$headers .= "Reply-To: " . get_option('woocommerce_email_from_name') . " <" . get_option('woocommerce_email_from_address') . ">\r\n";
//wp_mail($userdata->user_email, $subject, $temp_message, $headers);
if ((float) $woocommerce->version <= (float) ('2.2.0')) {

    wp_mail($admin_mail, $subject, $temp_message, $headers);
} else {
    $mailer = WC()->mailer();
    $mailer->send($admin_mail, $subject, $temp_message, '', '');
}
//}
//}
//
//new send_mail_admin();

//add_action('admin_head',array('send_mail_admin','send_admin_vendor_name'));