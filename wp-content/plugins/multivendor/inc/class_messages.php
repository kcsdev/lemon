<?php

/* Messages Tab Settings */

class FPMultiVendorMessagesTab {

    public function __construct() {
        add_filter('woocommerce_mv_settings_tabs_array', array($this, 'multivendor_messages_tab'), 100);
        add_action('admin_init', array($this, 'multivendor_add_default_values_settings'));
        add_action('woocommerce_mv_settings_tabs_multivendor_messages', array($this, 'multivendor_register_admin_settings'));
        add_action('woocommerce_update_options_multivendor_messages', array($this, 'multivendor_update_values_settings'));
    }

    public static function multivendor_messages_tab($settings_tabs) {
        $settings_tabs['multivendor_messages'] = __('Messages', 'multivendor');
        return $settings_tabs;
    }

    public static function multivendor_messages_admin_settings() {
        global $woocommerce;
        return apply_filters('woocommerce_multivendor_messages', array(
            array(
                'name' => __('Message Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_title_message_settings'
            ),
            array(
                'name' => __('Sell Individually Error Settings', 'multivendor'),
                'type' => 'title',
                'id' => '_mv_sell_individually_settings',
            ),
            array(
                'name' => __('Error Message for Restrict Product in Cart by Product Level', 'multivendor'),
                'desc' => __('Enter the Custom Message for Cart Restriction', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_product_level',
                'css' => 'min-width:550px;',
                'std' => 'Only One Product can add to Cart',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_product_level',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Error Message for Restrict Product in Cart by Vendor Level', 'multivendor'),
                'desc' => __('Enter the Custom Message for Cart Restriction in Vendor Level', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_level',
                'css' => 'min-width:550px;',
                'std' => 'Same Vendor Items only can add to Cart',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_level',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_mv_sell_individually_settings'),
            array(
                'name' => __('Error Settings for Frontend Shortcode Validation', 'multivendor'),
                'type' => 'title',
                'id' => '_fpmv_error_settings_for_frontend_shortcode',
            ),
            array(
                'name' => __('Enter Message for Vendor Name when it is Empty', 'multivendor'),
                'desc' => __('Enter the Custom Message for Vendor Name', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_name',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Vendor Name',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_name',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter Message for Vendor Description when it is Empty', 'multivendor'),
                'desc' => __('Display this Message when Vendor Description when Vendor Description is Empty', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_description',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter a Description about Vendor',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_description',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Message to Site Admin when it is Empty', 'multivendor'),
                'desc' => __('Display this Message when Message to Site Admin Field is Empty', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_message_to_site_admin',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter your Message to Site Admin or Reviewer',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_message_to_site_admin',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Vendor Commission Field is Empty', 'multivendor'),
                'desc' => __('Display this Message when Vendor Commission Rate Field is Empty', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_commission_rate',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Vendor Commission Rate in Percentage(%) ',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_commission_rate',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Vendor Commission Field is not a Number', 'multivendor'),
                'desc' => __('Display this Message when Vendor Commission Rate Field is not a Number', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_commission_rate_is_valid',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Only Numbers ',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_commission_rate_is_valid',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Vendor Paypal Address is Empty', 'multivendor'),
                'desc' => __('Display this Message when Vendor Paypal Address is Empty', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_paypal_address',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter your Paypal Email Address',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_paypal_address',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Vendor Paypal Address is not Valid', 'multivendor'),
                'desc' => __('Display this Message when Vendor Paypal Address is not Valid', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_paypal_address_is_valid',
                'css' => 'min-width:550px;',
                'std' => 'Please Enter Valid Email Address ',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_paypal_address_is_valid',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter your Message for Vendor Custom Payment Field is Empty', 'multivendor'),
                'desc' => __('Display this Message when Vendor Custom Payment Field is Empty', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_vendor_custom_payment',
                'css' => 'min-width:550px;',
                'std' => 'Custom Payment Field not to be Empty',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_vendor_custom_payment',
                'desc_tip' => true,
            ),
            
            array(
                'name' => __('Enter your Message for I agree checkbox is Unchecked', 'multivendor'),
                'desc' => __('Display this Message when I agree checkbox is Unchecked', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_error_message_for_checkbox',
                'css' => 'min-width:550px;',
                'std' => 'Please check this before submit',
                'type' => 'textarea',
                'newids' => 'fpmv_error_message_for_checkbox',
                'desc_tip' => true,
            ),
            
            array(
                'name' => __('Success Message for Vendor Application', 'multivendor'),
                'desc' => __('Display Success Message on After Completing Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_success_message_for_vendor_application',
                'css' => 'min-width:550px;',
                'std' => 'Your Vendor Application is Successfully Submitted',
                'type' => 'textarea',
                'newids' => 'fpmv_success_message_for_vendor_application',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => '_fpmv_error_settings_for_frontend_shortcode'),
            array(
                'name' => __('Message to Guest in Vendor Application and Vendor Application Status', 'multivendor'),
                'type' => 'title',
                'id' => 'fpmv_message_to_guest_on_vendor_application'
            ),
            array(
                'name' => __('Enter Your Message when User is Guest(not logged in) in Submitting Vendor Application', 'multivendor'),
                'desc' => __('Display this Message when User is Guest on Submitting Vendor Application', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_message_to_guest_on_vendor_application',
                'css' => 'min-width:550px;',
                'std' => 'You Must Login to Submit Vendor Application',
                'type' => 'textarea',
                'newids' => 'fpmv_message_to_guest_on_vendor_application',
                'desc_tip' => true,
            ),
            array(
                'name' => __('Enter Your Message when User is Guest(not logged in) on Viewing Vendor Application Status', 'multivendor'),
                'desc' => __('Display this Message when User is Guest on Viewing Vendor Application Status', 'multivendor'),
                'tip' => '',
                'id' => 'fpmv_message_to_guest_on_vendor_application_status',
                'css' => 'min-width:550px;',
                'std' => 'You Must Login to View your Application Status',
                'type' => 'textarea',
                'newids' => 'fpmv_message_to_guest_on_vendor_application_status',
                'desc_tip' => true,
            ),
            array('type' => 'sectionend', 'id' => 'fpmv_message_to_guest_on_vendor_application'),
            array('type' => 'sectionend', 'id' => '_mv_title_message_settings'),
        ));
    }
     public static function multivendor_register_admin_settings() {
        woocommerce_admin_fields(FPMultiVendorMessagesTab::multivendor_messages_admin_settings());
    }

    public static function multivendor_update_values_settings() {
        woocommerce_update_options(FPMultiVendorMessagesTab::multivendor_messages_admin_settings());
    }

    public static function multivendor_add_default_values_settings() {
        foreach (FPMultiVendorMessagesTab::multivendor_messages_admin_settings() as $settings) {
            if ((isset($settings['newids'])) && (isset($settings['std']))) {
                //  if (get_option($settings['newids']) == FALSE) {
                add_option($settings['newids'], $settings['std']);
            }
        }
    }

}

new FPMultiVendorMessagesTab();
