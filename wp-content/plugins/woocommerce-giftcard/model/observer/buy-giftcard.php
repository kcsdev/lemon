<?php
include_once GIFTCARD_PATH .'model/giftcard.php';

 class Magenest_Giftcard_Buygiftcard {
 	
 	public function __construct() {
 		add_action( 'woocommerce_add_to_cart', array( $this,'add_giftcard_extra_info'), 10, 3 );
 		add_action("woocommerce_checkout_order_processed", array($this , 'generateGiftcard'),10);
 		add_filter('woocommerce_get_item_data', array($this,'add_giftcard_item_data'), 10,2);

 		///////////////////////////

 		add_action ( 'woocommerce_order_status_pending', array ( $this, 'active_giftcard' ),10 );
 		add_action ( 'woocommerce_order_status_on-hold', array ( $this, 'active_giftcard' ) ,10);
 		add_action ( 'woocommerce_order_status_processing', array ( $this, 'active_giftcard') ,10 );
 		add_action ( 'woocommerce_order_status_completed', array ( $this, 'active_giftcard' ),10 );
 	}
 	
 	/**
 	 * change gift card status to active and send mail to the recipient and the giver
 	 * @param int $order_id
 	 */
 	public function active_giftcard($order_id) {
 		
 		global $wpdb;
 		$tbl = $wpdb->prefix .'postmeta';
 		
 		$order = new WC_Order($order_id);
 		
 		$status = $order->get_status();
 		$active_status = get_option('magenest_giftcard_active_when');
 		
 		if ($status == $active_status) {
 			$giftcard = new Magenest_Giftcard();
 			$giftcard->active_giftcard($order_id);
 			
 		}
 	}
 	public function add_giftcard_item_data($a, $cart_item) {
 		$other_data = array();
 		
 		if (is_array($cart_item['variation'])) {
 			
 			if (isset($cart_item['variation']['To Name'])) {
 				$other_data[] = array('name'=> __('To Name', GIFTCARD_TEXT_DOMAIN) ,'value' => $cart_item['variation']['To Name']);
 			}
 			
 			if (isset($cart_item['variation']['To Email'])) {
 				$other_data[] = array('name'=> __('To Email', GIFTCARD_TEXT_DOMAIN) ,'value' => $cart_item['variation']['To Email']);
 			}
 			
 			if (isset($cart_item['variation']['Message'])) {
 				$other_data[] = array('name'=> __('Message', GIFTCARD_TEXT_DOMAIN) ,'value' => $cart_item['variation']['Message']);
 			}
 		}
 		
 		return $other_data;
 	}
	public function add_giftcard_extra_info($cart_item_key, $product_id, $quantity) {
		global $woocommerce, $post;
		
		$is_giftcard = get_post_meta ( $product_id, '_giftcard', true );
		
		if ($is_giftcard == "yes") {
			
			$giftcard_data = array ();
			if (isset($_REQUEST['giftcard-amount'])) {
				$gc = 'gc'.$product_id ;
				//$woocommerce->session->$gc= $_POST['giftcard-amount'];
				$_SESSION[$gc] = $_REQUEST['giftcard-amount'];
			}
			if (isset ( $_POST ['send_to_name'] ) && $_POST ['send_to_name'])
				$giftcard_data ['To Name'] = woocommerce_clean ( $_POST ['send_to_name'] );
			
			if (isset ( $_POST ['send_to_email'] )&& $_POST ['send_to_email'])
				$giftcard_data ['To Email'] = woocommerce_clean ( $_POST ['send_to_email'] );
			
			if (isset ( $_POST ['message'] ) && $_POST ['message'])
				$giftcard_data ['Message'] = woocommerce_clean ( $_POST ['message'] );
			
			$giftcard_data = apply_filters ( 'add_giftcard_extra_info', $giftcard_data, $_POST );
			$woocommerce->session->giftcard_product  = $giftcard_data;
			$woocommerce->cart->cart_contents [$cart_item_key] ["variation"] = $giftcard_data;
			return $woocommerce;
		}
	}
	public function generateGiftcard($order_id) {
	$order = wc_get_order($order_id);
				
			/* @var $order WC_Order */
			if (sizeof ( $order->get_items () ) > 0) {
					
				foreach ( $order->get_items () as $item ) {
					$_product     = apply_filters( 'magenest_giftcard_order_item_product', $order->get_product_from_item( $item ), $item );
		
					/* @var $_product WC_Product */
		
					$giftcard_balance = $_product->get_price();
					$is_giftcard = get_post_meta ( $_product->id, '_giftcard', true );
					if($is_giftcard=='yes') {
						$to_name ='';
						$to_email ='';
						$message ='';
					//$item_meta    = new WC_Order_Item_Meta( $item['item_meta'], $_product );
					$item_meta = $item;
					
					$qty = $item['item_meta']['_qty'][0];
					
					if (isset($item_meta['To Name'])) {
						$to_name = $item_meta['To Name'];
					}
		
					if (isset($item_meta['Message'])) {
						$message = $item_meta['Message'];
					}
		
					if (isset($item_meta['To Email']) ) {
						$to_email = $item_meta['To Email'];
					}
                    
					//calculate gift card expired date
					
					$expired_at  = '';
					
					$expired_at_product_scope = get_post_meta($_product->id , '_giftcard-expiry-date', true);
					
					if ($expired_at_product_scope) {
						$expired_at = $expired_at;
					} else {
						if (get_option('magenest_giftcard_timespan')) {
							$giftcard = new Magenest_Giftcard();
							$expired_at_website_scope  = $giftcard->calculateExpiryDate();
							if ($expired_at_website_scope)
							$expired_at  =$expired_at_website_scope;
						}
					}
					
						/* save gift card and send notification email */
					$gift_card_data = array (
							'product_id' => $_product->id,
							'magenest_giftcard_order_id'=> $order->id,
							'product_name' => $_product->get_title (),
							'user_id' => $order->get_user_id (),
							'balance' => $giftcard_balance,
							'init_balance' => $giftcard_balance,
							'send_from_firstname' => '',
							'send_from_last_name' => '',
							'send_to_name' => $to_name,
							'send_to_email' => $to_email,
							'scheduled_send_time' => '',
							'is_sent' => 0,
							'send_via' => '',
							'extra_info' => '',
							'code' => '',
							'message' => $message,
							'status' => 0,
							'expired_at' => $expired_at 
					)
					;
					$gifcard = new Magenest_Giftcard();
					
					for($i = 0; $i < $qty; $i ++) {
						$gifcard->generateGiftcard ( $code = '', $gift_card_data );
					}
				}
			}
		}
	}
 }
 return new Magenest_Giftcard_Buygiftcard();
 